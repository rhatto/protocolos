# Anti-assédio e anti-discriminação

{{ warning.trigger.pt }}

{{ info.context.pt }}

## Prevenção e combate

Cartilhas e manuais anti-assédio e anti-discriminação:

* [Prevenção aos assédios moral e sexual - Capes](https://www.gov.br/capes/pt-br/centrais-de-conteudo/documentos/16102023_Cartilha_de_Preveno_aos_assdios_moral_e_sexual.pdf).
* [Assédio moral no trabalho: perguntas e respostas - MPT](https://mpt.mp.br/pgt/publicacoes/cartilhas/assedio-moral-no-trabalho-perguntas-e-respostas/@@display-file/arquivo_pdf).
* [Assédio moral e sexual - Previna-se - CNMP](https://www.mpf.mp.br/sc/arquivos/cartilha-assedio)
  ([cópia](https://www.cnmp.mp.br/portal/publicacoes/245-cartilhas-e-manuais/9982-assedio-moral-e-sexual-previna-se)).
* [Manual sobre a prevenção e o enfrentamento ao assédio moral e sexual e à discriminação](https://mpt.mp.br/pgt/publicacoes/manuais/manual-sobre-a-prevencao-e-o-enfrentamento-ao-assedio-moral-e-sexual-e-a-discriminacao/@@display-file/arquivo_pdf).
* [Assédio Moral, Sexual e Discriminação Saiba mais sobre essas distorções de conduta no ambiente de trabalho](https://biblioteca.mpf.mp.br/server/api/core/bitstreams/a7da884c-fefb-4641-a301-a22abeb93824/content).

## Metadados

* {{ metadata.version.pt }}: `0.0.1 (2024-09-14)`.
* {{ metadata.identifier.pt }}: `pid:pessoal:anti_assedio`.
