# Pisando no Quadrado: um Código de Conduta Padrão

{{ warning.trigger.pt }}

{{ info.context.pt }}

{{ warning.needs_work.pt }}

Este é um checklist de procedimentos e código de conduta.
Você tem uma ideia de como se portar em cada um desses items?

## Checklist

* Comunicação implícita e explícita:
    * O que pode ser assumido de antemão.
    * O que não pode ser assumido de antemão.
* Separação entre aspectos da vida:
    * Profissional, político e pessoal.
    * Amizades e afinidades.
    * Público e privado.
* Lidando com conhecimento:
    * Criando um canal e um encorajamento explícito para contatos, sugestões e críticas.
    * Fomentando a formação e a inclusão de mais pessoas no ativismo técnico e na difusão do conhecimento.
    * Tendo uma melhor comunicação com grupos e pessoas próximas.
    * Definir melhor e de forma pública o funcionamento e a participação.
* Relacionamento:
    * [Alteridade](http://www.revolucoes.org.br/v1/conferencia/alteridade).
    * Generosidade.
    * Alimentação.
    * Liberdade de associação e de não-associação, tanto para propósitos gerais quanto específicos.
    * Responsabilidade e zelo.
    * Buscar facilitar as relações e situações, ao invés de dificultar a fluidez dos relacionamentos.
    * Comportamentos evitados: trolling, bullying, spamming, etc.
    * Gentileza gera gentileza.
    * Proceder: humildade, respeito, responsa.
    * Assiduidade, pontualidade e atrasos.
    * Política de assinatura de chaves.
    * Participação em debates e discussões.
    * Favores e reciprocidade.
    * Fidelidade (compromisso) e lealdade (espontâneo).
    * Sobre a confiança (contextual e total) e a alteridade.
    * Pronomes de tratamento e linguagem inclusiva.
    * Gêneros (neutro por padrão).
    * Discussão, facilitação, opiniões (e mudança de opiniões), respeito a rodadas e falas:
        * Discussão de temas em geral.
        * Discussão periódica da relação.
    * Comunicação mediada (virtual):
        * [Netiqueta](https://tools.ietf.org/html/rfc1855).
        * Lei de [Postel](https://en.wikipedia.org/wiki/Jon_Postel): "Be
          liberal in what you accept, and conservative in what you send".
        * Como lidar com ausência de resposta.
        * Como a ausência de resposta é interpretada.
        * Assuntos públicos, semi-públicos e privados: como lidar com mensagens
          privadas evitando concentração de informação e canais laterais que
          possam prejudicar organizações horizontais.
        * Emoticons, cordialidade e polidez.
        * Quando assinar comunicação, quando não assinar, modo anônimo.
    * Comunicação ao vivo:
        * Cumprimentos (saudação visual, aperto de mão, beijo, saudação).
* Sistematizando formas de lidar com situações diversas:
    * [Defence mechanisms](https://en.wikipedia.org/wiki/Defence_mechanisms).
    * Círculo Restaurativo.
    * Comunicação não-violenta.
    * Linguagem inclusiva.
    * Situações críticas: como não reproduzir instituições burguesas (tribunal,
      prisão, manicômio) ou arcaicas (ostracismo, linchamento, etc)?
    * Facilitação e resolução de conflitos.
        * [Conflito e Consenso](https://docs.indymedia.org/Local/CmiBrasilConflitoConsenso).
        * [Conflict resolution](https://simple.wikipedia.org/wiki/Conflict_resolution).
* Alertas de conteúdo (também conhecido como "Alertas de gatilho): quando e como usá-los:
    * [Trigger warnings: What do they do? - BBC News](https://www.bbc.com/news/blogs-ouch-26295437)
    * [What Are Trigger Warnings: Sharing Traumatic or Disturbing Content - GoodRx](https://www.goodrx.com/health-topic/mental-health/trigger-warnings-traumatic-disturbing-content)
    * O mais correto parece ser "Alerta de conteúdo" ao invés "de gatilho":
      parece mais respeitoso e evita a menção a gatilhos, pois a mera menção já
      pode ativar gatilhos.
    * Uma maneira de abordar a ambiguidade dos alertas de gatilho talvez seja formulá-los
      de modo que a pessoa possa ter a escolha de como e quando lidar com suas
      questões, ao invés de ser pega de surpresa.
    * Em mensagens escritas, vale dar o alerta antes do conteúdo. Exemplo: `Alerta de conteúdo: [informações do conteúdo]. Clicar em "Leia mais para acessar". XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX`.

## Metadados

* {{ metadata.version.pt }}: `0.0.4 (2024-10-17)`.
* {{ metadata.identifier.pt }}: `pid:pessoal:conduta:quadrada`.
