# Conduta Estrita em Debates

{{ warning.trigger.pt }}

{{ info.context.pt }}

## Introdução

No modo de conduta estrita, o comportamento é otimizado para evitar
desconfortos.

## Recebendo um convite

* Não participar de eventos sem ter recebido convites explícitos.
* Ao receber um convite, checar:
    * Se, dependendo dos seus privilégios atuais, o mais correto é sugerir à
      organização do evento para que priorize outras pessoas/grupos sociais etc,
      ficando como opção secundária/última opção etc.
    * Quais são os códigos de conduta explícitos e implícitos do evento, para
      em seguida estudá-los de modo a cumpri-los estritamente.

## Preparando sua fala

* Para não desperdiçar o tempo alheio, prepare de antemão a sua intervenção.
* Prepare, se possível, materiais auxiliares (audiovisuais, slides, panfletos etc).

## Fala inicial padrão

Este pode ser um exemplo de pontos para uma fala inicial padrão:

* Introdução:
    * Pedir licença para falar e desculpas de antemão pelas incompletudes,
      limitações e equívocos.
    * Agradecimento pela atenção e oportunidade.
    * Alerta sobre possíveis gatilhos, e do quanto (não-)prioritário é
      o assunto.
    * Afirmar que ainda está aprendendo tudo isso, e que gostaria de
      compartilhar o que aprendeu até o momento e sobre o que ainda quer
      aprender.
    * Reconhecimento/enunciação dos meus privilégios, de quanto sou parte dos
      problemas, e quanto acredito (ou posso ser) parte das soluções.
* Disclaimers opcionais:
    * Esse convite é mais generosidade comigo do que a qualidade do
      que tenho a dizer.
    * A pandemização pode ter me deixado com dificuldades de socialização.
    * Me esforço para uma exposição breve e sucinta, falando apenas o
      necessário.
    * Por gentileza, se alguém se sentir com mais aptidão do que eu, ou achar
      que outra pessoa deveria estar aqui, posso ceder meu lugar na mesa [e
      eventualmente ficar de backup].

## Finalização

* Agradecer mais uma vez.
* Pedir desculpas antecipadamente por todos os erros, problemas etc.
* Sugestões, dúvidas, retornos (feedback) etc são todos
  bem-vindos para a melhoria deste trabalho e da minha postura.

## Recebendo feedbacks

Após contribuições/colocações/retornos de terceiros:

* Anotar pontos para consideração.
* Agradecer pelas ponderações, indicando que já estão sendo consideradas.
* Perguntar se a pessoa autoriza ser citada.
* Perguntar se a pessoa permite que a colocação seja incorporada
  no trabalho, com a devida citação.

## Após o evento

* Refletir sobre como foi sua participação, especialmente relativamente
  às ponderações, sugestões etc recebidas.

## Metadados

* {{ metadata.version.pt }}: `0.0.2 (2024-08-15)`.
* {{ metadata.identifier.pt }}: `pid:pessoal:conduta:debate_estrito`.
