# Códigos de Conduta Pessoal

{{ warning.trigger.pt }}

{{ info.context.pt }}

## Introdução

Esta seção trata de códigos de conduta pessoal:

* Tanto de Códigos de Conduta pré-estabelecidos em eventos, grupos etc.
* Quanto modelos de Códigos de Conduta versionados que podem ser adotados por
  cada pessoa, independentemente de acordos com outras pessoas e grupos, e ser
  usado em eventos e outras situações onde não haja um Código de Conduta
  explícito/definido/conhecido.

!!! warning "Alerta de comportamento"

    Não é porque um local/evento não possua um código de conduta explícito
    que significa que não haja código de conduta.
    Na maioria dos casos, não há código explícito, mas pode haver vários
    protocolos tácitos de conduta.
    O mais seguro é adotar sempre uma conduta inicial mais estrita até
    entender quais são as condutas de bem estar coletivo aplicáveis no
    local, asssim como entender quais são as violências existentes
    para que se possa agir no sentido de não reproduzí-las.

    Ou seja, como regra geral: havendo código de conduta implícito ou explícito
    no evento, adotar as regras que forem mais estritas, caso a caso: se as
    regras deste código forem mais estritas para uma dada situação, continue
    usando-as, caso contrário adote as regras do evento enquanto este durar.

## Códigos de Conduta Pré-estabelecidos

Esta é uma lista incompleta de códigos de conduta pré-estabelecidos para
participação em organizações e eventos:

* [Código de Conduta da CryptoRave](https://we.riseup.net/cryptorave/politica-anti-assedio).
* [Código de Conduta para eventos na MariaLab](https://www.marialab.org/codigo-de-conduta/).
* [Código de Conduta Antiassédio Sexual Intervozes](https://app.rios.org.br/index.php/s/PyLXN85zywLkYBt).
* [Código de Conduta do Projeto Tor](https://community.torproject.org/policies/code_of_conduct/).
* [Code of Conduct Resources - The Citizen Lab](https://citizenlab.ca/2017/07/code-conduct-resources/)
* [General Resolution: code of conduct - Debian](https://www.debian.org/vote/2014/vote_002), [Anti-harassment team - Debian](https://wiki.debian.org/AntiHarassment).
* [Código de Conduta do Mediawiki](https://www.mediawiki.org/wiki/Code_of_Conduct).
* [Contributor Covenant: A Code of Conduct for Open Source Projects](http://contributor-covenant.org/).
* [Note Well - IETF](https://www.ietf.org/about/note-well/).
* [Universal Declaration of Human Rights | United Nations](http://www.un.org/en/universal-declaration-human-rights/index.html).
* [Chatham House Rule](https://en.wikipedia.org/wiki/Chatham_House_Rule).

## Metadados

* {{ metadata.version.pt }}: `0.0.2 (2024-09-14)`.
* {{ metadata.identifier.pt }}: `pid:pessoal:conduta`.
