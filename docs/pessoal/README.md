# Protocolos pessoais

{{ info.context.pt }}

{{ warning.needs_work.pt }}

Isso varia de pessoa para pessoa, mas em resumo a ideia aqui é a seguinte:
experimente formas de se organizar e não se prenda a esquemas prontos e
fechados; descarte os modelos que não funcionam, melhore o que estiver
funcionando e adote novos até encontrar algo que sirva!

Não se prenda a regras e esquemas quando eles não funcionem, o que vale
inclusive para esta regra!
