# Saúde pessoal

{{ info.context.pt }}

{{ warning.needs_work.pt }}

Seção com protocolos e modelos para saúde pessoal.

## Metadados

* {{ metadata.version.pt }}: `0.0.1 (2024-09-14)`.
* {{ metadata.identifier.pt }}: `pid:pessoal:saude`.
