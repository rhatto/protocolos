# Ficha médica

{{ info.context.pt }}

{{ warning.needs_work.pt }}

## Básico

    Ficha médica / Medical record
    Nome / Name                : José Bedeu
    Tipo sanguíneo / Blood type: A+
    Endereço / address         : Rua dos Parnasianos, 35
                                 São Longuinho - RJ - 30353-129 - Brasil
    Telefone / phone           : +55 12 0000 0000

## Metadados

* {{ metadata.version.pt }}: `0.0.1 (2024-09-14)`.
* {{ metadata.identifier.pt }}: `pid:pessoal:saude:ficha`.
