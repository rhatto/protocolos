# Organização

{{ info.context.pt }}

{{ warning.needs_work.pt }}

Se a [modelagem de ameaças](https://plano.autodefesa.org)
é a mãe da segurança, organização é a mãe de tudo!

Uma fala sobre organização (processos, metodologias, etc) para coletivos
técnicos radicais. Apenas aspectos organizacionais entre pessoas será abordado,
como tomada de decisões, responsabilização, realização, etc, deixando aspectos
técnicos para outras falas.

## Introdução

    Se você está panguando, o problema é seu.
    Se você quer deixar de panguar, o problema é nosso.

    --- Atarefados Anônimos

* Houve uma época em que acreditava que a computação ajudaria as pessoas a se
  organizarem.

* Mas hoje fico impressionado no nível de desorganização de grupos e pessoas.

* Em parte, porque a capacidade de se organizar foi transferida para as
  máquinas corporativas que escolhem qual e que informação as interagirão.

* Ao mesmo tempo, a desorganização é a forma mais sutil de opressão, já que
  dificilmente as pessoas enxergam a desorganização como causada por fatores
  externos: "estou desorganizado/a" é uma fase comum que atribui a culpa às
  pessoas.

* Mas sim, a organização é uma tarefa nossa. Avante!

## Princípios

Alguns princípios para organização pragmática básica:

* Ambiente de trabalho limpo (as coisas estão guardadas).
* Saber o que está disponível (lembramos da existência das coisas).
* Facilidade de achar aquilo que se procura (encontramos as coisas).
* Facilidade de acessar aquilo que se procura (as coisas estão bem guardadas).
* Facilidade de guardar aquilo que foi usado (as coisas estão bem dispostas).
* As coisas estão sempre prontas para o uso (elas foram limpas, consertadas e bem acondicionadas).
* O que não desarrumamos não precisamos arrumar.
* Abordagem da limpeza e organização contínua versus deixar acumular: cada uma
  tem seus prós e contras, e há possibilidade de alternância entre esses modos.

## Formas de organizar as atividades

Um exemplo:

* Dividir as atividades em projetos/grupos/organizações:
  * Cada uma delas possui uma pasta no meu computador pessoal.
  * Cada nível de uma estrutura de pasta possui itens de uma mesma categoria.
* Cada um desses projetos possui seu próprio sistema de gestão de tarefas (de
  um simples arquivo TODO até um sistema de tickets).
* Possuo um calendário com agendador de tarefas integrados:
  * Procuro deixá-lo o mais vazio possível em termos de compromissos e tarefas.
  * Cada projeto pode ter seu arquivo de calendário / categoria próprios.
  * A mesma paridade pode ser feita com pastas de email.
  * O calendário possui um lembrete básico para que eu dê uma olhada nas pendências
    de todos os projetos.
  * Deixo no calendário apenas as tarefas mais importantes e as menos importantes seguem
    nas listas de tarefas de cada projeto.
  * Projetos com poucas tarefas e eventos ou que façam parte de uma mesma categoria podem
    ser agrupados nas pastas de email e de calendário.
* Possuo coletores auxiliares de lembretes e ideias, em geral cadernos de notas ou papéis
  na carteira, mas estes devem ser "sincronizados" assim que possível, isto é, seu conteúdo
  transcrito para os projetos correspondentes.
* Tenho mania de anotação. Lembrei de algo, tento anotar na hora e no local certo, o que ocorreu
  inclusive com esta nota ;)

Alguns projetos podem ser considerados universais, isto é, serem adotados por
pessoas indiferentes de suas atividades principais. São eles:

* [Templates](https://git.fluxo.info/templates.git): pode ser entendido como um
  metaprojeto, agregando receitas de como se começar um novo projeto.
* Logística: é um projeto que gerencia o estoque.

## Pragmática

Trabalho reprodutivo, atividades básicas, baseline, constância:

* Homeostase.
* Logística.
* Inventário.
* Auditoria.
* Rotina.
* Regulação.
* Regularidade.
* Prática.
* Pragmatismo.
* Prosa.

Técnica, simplexidade: garante que o baseline tenha carga mínima; linha de base
operacional.

## Formas de organizar o tempo

Percepções:

* Percebo que existe uma inércia mental, digamos um período até que entremos no
  contexto de uma dada atividade.

* Analogamente, existe um período para aquisição de concentração.

Assim:

* Tento organizar as atividades em slots com período suficiente para entrar no
  contexto e adquirir concentração.

* Tento minimizar as saídas a fornecedores.

## A mecânica da agenda

* Coletores, agendas e listas de tarefas são buffers de armazenamento de desejos ou intenções
  de realização. São escalonadores de atividades.
* Arquivos são listas de atividades que nunca ou já ocorreram mas que não compõe a lista atual
  de intenções de realização.
* ChangeLogs são locais de descrição de tarefas que ocorreram.

A vida de um evento:

* Eventos singulares: ocorrem uma vez e são automaticamente são arquivados.
* Eventos recorrente ou multi-etapas: após a iteração atual devem ter sua
  próxima ocorrência agendada.

Exemplos:

* Eventos singulares: encontrar alguém numa data e local específicos.
* Eventos recorrentes: checagem de inventário, auditoria em sistemas.
* Eventos multi-etapas: tirar um documento.

## Coleções de tarefas e listas contextuais

Vamos começar fazendo prática e depois a teoria!

* Coletores e gestores pessoais
  * Caderno
  * Post-it: que bagunça!
  * Agenda impressa e digital
  * Email e a caixa de saída

* Coletores e gestores coletivos
  * Lista de discussão: as tarefas se perdem!
  * Sistemas de tickets

* Relação entre caderno, email, sistema

* Acumulação: guardar o trabalho organizativo e material feito hoje para usá-lo no futuro.

## Topologia

* Desenvolvimento e Operação
* Upstream e downstream: mover a solução para upstream
* Ter mais informes do que questões da debater: divisões em Grupos de Trabalho
  (GTs) ou comitês: desacoplamento.

## Urgência e emergência

* Urgências e emergências devem estar previstas na medida do possível.

## Fluxos

### A espiral de anéis

Os dois momentos: homeostase e pesquisa:

    (técnica)      (arte)
    (automático)   (manual)
    (produção)     (desenvolvimento)
    homeostase ->- pesquisa
       \             /  \
        `------<----´    `-----> arquivamento
        (incorporação)    `
                            `--> descarte

A produção pode consistir tanto de automatismo pessoais (corporificados)
quanto na automação de sistemas para realização de tarefas constantes.

### Método espirálico

         (tática)               (estratégia)                     (logística)
    |-- brainstorm --|--------- planejamento ----------------|-- realização --|


    atividades imediatas:
    ----------------------------->---------------------------------------------

    atividades mediadas:

                      .-->- descarte      --------<-------------
                     /                  .´                      `.
                    .--->- arquivamento -->--.       .----<-------.
                   /                          `.    /              \
     coletores ZTD -->-- agenda contextual (loop principal) ->- execução
           \       \                            .                  /
            \       `->- TODOs contextuais ->--´                  /
             \                                                   /
              ------------------<-------------------------------´

## Distribuição de eventos

* Quebrar tarefas grandes em tarefas menores.
* Empirismo para determinar quantidade de tarefas para cada dia.
* Recomendações ZTD, GTD, Pomodoro, etc.
* A rotina não pode ser tornar uma camisa de força para a pessoa; a ideia é
  justamente o contrário: que ela otimize o tempo o bem viver.

### Documentação

Cada processo pode ser composto por documentação pública ou privada:

    documentação --->---- separação de especificidades -->--- documentação pública
    privada               e dados sensíveis

## Níveis

* Pessoal: usando técnicas como:
    * [Zen To Done (ZTD)][].
    * [Pomodoro][].
    * [Bullet Journal (BuJo)][].
    * [Ultimate Study Method (USM)][].
* [Coletivo](/coletivo)
* Às vezes ajudamos mais os grupos se já estamos organizados/as, mas em muitas
  situações é a atividade coletiva que nos organiza.

[Zen To Done (ZTD)]: https://zenhabits.net/zen-to-done-ztd-the-ultimate-simple-productivity-system/
[Pomodoro]: https://francescocirillo.com/pages/pomodoro-technique
[Bullet Journal (BuJo)]: https://bulletjournal.com/
[Ultimate Study Method (USM)]: http://www.ultimatestudymethod.com/

## Problemas

* Quais são os problemas a serem resolvidos?
* Problemas que queremos/iremos resolver e aqueles que não resolveremos.
* Fazer o que queremos X fazer o que é necessário.

## Prazos

* Temporalidade.
* Otimismo pode prejudicar a estimativa de prazos.
* Metas de pequeno, médio e longo prazo.
* Lembrar das coisas.

## Zeladoria rotativa

Será que funciona?

## Os frutos da organização

* A quem cabe fruir o valor da organização?
* Se tornar eficiente onde?
* Ativistas como trabalhadores/as nos próprios movimentos.

## Comunicação

    Cohn's Law:
            The more time you spend in reporting on what you are doing, the less
            time you have to do anything.  Stability is achieved when you spend
            all your time reporting on the nothing you are doing.

Dicas básicas:

* Comunicação direta, porém não ríspida.
* Diferenças de contexto.
* Lembrar que possivelmente os canais de comunicação serão também canais
  históricos, ou seja, ajudarão pessoas no futuro a entender os grupos. Da
  mesma forma, existe a preocupação com segurança.

## Rotinas

É importante montar rotinas a partir da constatação de que é necessário reduzir
as transições de contexto, evitando que a rotina semanal esteja emaranhada de
projetos e tarefas, quando num mesmo dia ou semana é preciso dar conta de uma
série de projetos -- trabalho, família, casa, atividades sociais, estudo etc.

O excesso de projetos e tarefas misturadas numa mesma semana implica em
mudanças de contextos frequentes, o que é custoso, ineficiente e prejudicial.

Os exemplos de rotina a seguir podem ajudar, lembrando que é um privilégio
conseguir definir a própria rotina, especialmente de maneira tão flexível.

### Exemplo de rotina com dois sprints semanais

Este exemplo é orientado em sprints semanais.

* Por exemplo, três dias de trabalho intenso (de 11 a 12 horas diárias),
  seguidos por 1 de descanso (segunda a quarta), e finais de semana temáticos
  (sexta a domingo).
* Outra possibilidade é usar a sexta-feira para organização/logística/etc,
  deixando sábado e domingo para atividades temáticas.
* Ou então deixar o sprint de trabalho de terça a quinta, com a segunda-feira
  para descanso, sexta para afazeres diversos, sábado e domingo temáticos.
* Ou um esquema ainda mais simples, consistindo no seguinte
* Semana de 7 dias separadas por dois períodos ativos (sprints) (trampo de
  terça a quinta; temático de sábado e domingo), com dois dias de transição,
  para descanso e organização/logística (segunda e sexta).

Cômputo de horas:

* 3 dias de trabalho com 12 horas cada: 36 horas.
* 3 dias de trabalho com 11 horas cada: 33 horas.
* Incluindo a sexta-feira em modo flexível: 41 a 44 horas trabalhadas.

Sprints de trabalho:

* Alimentação: recomendação de fazer a última refeição até às 19:00.
* Sono: recomendação: das 21:00 ou 22:00 às 5:00 ou 6:00; usar tampão de
  ouvido, tela mosquiteira e boa ventilação conforme necessário; tomar um chá
  um pouco antes de dormir, ouvir músicas calmas, desligar a comunicação etc.
* Depertar: higiene, arrumação básica, meditação, café da manhã, exercícios
  básicos.
* Comunicação: recomendação: uma vez ao dia, no final da tarde, logo após o
  expediente.
* Durante o expediente, considerar a técnica pomodoro intercalada com pequenos
  afazeres, pequenos relaxamentos/meditações, breves alongamentos e exercícios
  oculares; tirar cochilos se der sono ou confusão mental
* Distrações: a serem descarregadas em buffers e listas temáticas
  para triagem, organização e eventual encaminhamento posterior nos
  sprints temáticos.
* Nos sprints, adotar metodologias/customizações do tipo Zen To Done / GTD,
  especialmente tentando resolver a tarefa mais importante logo no início do
  dia.

Sprints temáticos (finais de semana):

* Finais de semana temáticos podem incluir o estudo, viagem, atividades
  sociais, casa, família etc, mas escolhendo um único tema
* Os findes temáticos precisam ser definidos dinamicamente, pois a
  vontade e a disponibilidade das pessoas é dada com pouca antecedência;
  sua vontade, pique e desejos também precisam ser considerados.

Dias de transição/buffer:

* Atividades metabólicas domésticas, familiares e sociais a serem incluídas nos
  findes temáticos ou nos dias de
  descanso/organização/manutenção/logística/backups (lavar roupa, cozinhar,
  limpeza básica etc).
* Tentar ir na feira apenas a cada 2 semanas, o que requer calibrar lista de
  compras para que isso funcione, especialmente banana, mamão e verduras
* Os dias de transição operam como buffers e descompressão, permitindo a
  mudança de contexto.
* Nesses dias, recomenda-se buscar a calma, relaxamento e a reflexão, além
  de dormir cedo para acordar cedo no começo do próximo sprint (sugestão:
  iniciar um sprint às 5 ou 6 da manhã).

Terceiros:

* Terceiros envolvidos (trabalho, familia, amigos etc) precisam ser informados
  deste modo de operação e eventualmente do tema da semana.
* Combinar, por exemplo, para preferencialmente me agruparem pedidos
  e solicitações; e para que estas sejam atendidas em dias específicos;
  excetuando emergências e urgências inesperadas.

Férias:

* Esta rotina precisa ser intercalada por uma quebra de rotina --
  férias do trabalho temáticas -- passeios, retiros de estudo,
  carnaval, reformas e manutenção da casa etc -- e que incluam um tempo
  inicial e final também de buffer/descompressão/descanso/etc.
* A recomendação é de uma a duas semanas por trimestre, e um mês
  em casos especiais.

Considerações:

* Os sprints também não são super fixos, já que é impossível conseguir
  combinar com o resto do mundo para respeitar integralmente os slots
  de tempo; assim, pode acontecer de trabalho e reuniões ocorrerem
  às segundas e sextas-feiras.
* Sobretudo, reconhecer que não somos lineares e perfeitamente encaixáveis em
  esquemas como este, então é importante respeitar a si mais do que o esquema,
  deixando-se também levar pelos fluxos e horários irregulares -- o mais
  importante é tentar ficar por períodos mais longos fazendo uma mesma atividade
  e em boas condições.
* Cansaço é cumulativo, descanso não. Sono idem. Por isso é importante
  descansar sempre, TODOS os dias em que for possível.
