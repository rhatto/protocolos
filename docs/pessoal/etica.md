# Ética Pessoal

{{ info.context.pt }}

{{ warning.needs_work.pt }}

## Motivação

Código é lei? Código pode também ser ética. Se existem
[protocolos](https://protocolos.fluxo.info) e [códigos de
conduta](https://www.debian.org/vote/2014/vote_002) atuando no nível de grupos,
é importante também haver procedimentos de auxílio para relações em nível
individual.

Dito [atribuído a Oscar Wilde](https://www.pensador.com/frase/NjMzNTE2/) (citação necessária):

> Chamamos de ética o conjunto de coisas que as pessoas fazem quando todos
> estão olhando. O conjunto de coisas que as pessoas fazem quando ninguém está
> olhando chamamos de caráter.

_Codex moralia_ básico:

* Pisar no quadrado não é só pra quem quer, como principalmente para quem pode.
* Pisar no quadrado por ter condições, mas sem moralizar quem não tem.

## Pressupostos

* Como cantou Daminhão Experiência, "O mundo foi bem feito / todo mundo tem
  defeito".
* Também como nos ditames:
    * "Ninguém é melhor do que ninguém".
    * "Nem melhor, nem pior, apenas diferente".
* Saber quando pisar dentro e fora do quadrado.
* Noções básicas de postura social.
* Não desmerecer ninguém, especialmente quando se ressalta qualidades de outrem.

## Cuidados

Não é a intenção deste breve guia:

* Se transformar numa rocha inalterável e inflexível de como as relações
  humanas devem ser.
* Criar um protocolo que dificulte relações e deixe as pessoas apartadas.

Este guia é mais uma referências de aspectos a serem pensados em relações com pessoas.

## Objetivos

Grosso modo:

* Calma.
* Alegria.
* Agilidade.
* Sangue-bonismo.
* Companheirismo.
* Solidariedade, fraternidade, alteridade, autruísmo.

## Exemplo

Subconjunto de baixa complexidade necessária:

0. Saúde é o que interessa!

1. Não importa o que aconteça, duas coisas precisam ser constantes na vida:
   estudar e se exercitar (circuito do aprendizado e da experiência).

2. De resto, não leve nada a sério mas sempre se engaje! Que venha a farra, a
   alegria e a solidariedade/generosidade!

## Códigos de Conduta

Vide seção [Códigos de Conduta](conduta/README.md).

## A Teia das Implicações

* Ética em Morin: "ecologia da ação".

* A noção de [karma][] como algo que passa de relação em relação.

* A partir disso, conceituar as relações de parasitismo, como
  a exploração de outros seres, por exemplo no caso da expropriação
  do trabalho alheio (vulgo roubo).

* Ladrão que rouba ladrão [apenas paradoxalmente teria "cem anos de perdão"][].
  O que acontece é que o segundo ladrão acaba herdando o roubo original. Em
  relação ao primeiro ladrão, houve expopriação mas, se o que foi roubado
  não volta para quem foram originalmente expoliados, não haverá justiça de fato.

* Por esta interpretação, mesmo quando não operamos diretamente o roubo, mas
  nos beneficiamos dele, ainda assim nos implicamos. Exemplos de situações:
    * Ao comprarmos uma "mercadoria" ou "serviço", mesmo que o produto não
      tenha sido produzido em condições ditas como "análogas à escravidão",
      ainda assim pode haver expropriação de mais-valia, ou seja, roubo do
      tempo e da vitalidade alheia.

* Estar dentro de uma sociedade baseada no roubo, na exploração etc torna
  muito difícil, senão impossível, que alguém consiga estar totalmente livre
  de participar, voluntária ou involuntariamente, direta ou indiretamente,
  de relações de exploração.

* Mas isso não é justificativa para não se importar com isso. Ao contrário,
  é um chamado à tentar reduzir ao máximo a participação, direta ou indireta,
  em redes de relação parasitárias e de mau viver.

* Mundo Implicado[@denise2019]: todos os seres estão implicados num
  emaranhamento não-desenmaranhável.

[karma]: https://pt.wikipedia.org/wiki/Karma
[apenas paradoxalmente teria "cem anos de perdão"]: https://arquivo.fluxo.info/conteudo/rattus.esgoto.org/httrack/rattus.esgoto.org/corrupcao.html

## Referências

\bibliography
