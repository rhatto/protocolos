# Básico da Vida

{{ info.context.pt }}

{{ warning.needs_work.pt }}

## Necessidades básicas

* Baseline vital.
* A vida não é quadrada e isto é um modelo.
* O dia é curto demais para que a lista seja completada.
* Este é um roteiro para estar bem e fazer o bem todos os dias.
* Evitando cansaço, ansiedade e mal uso de recursos.
* Para ser [ético](/etica/pessoal).

### Exemplo de rotina diária ideal

Especificidades na lista de tarefas/calendário.

* Descanso     (8:00).
* Higiene      (0:30).
* Alimentação  (1:00).
* Exercícios   (1:00).
* Organização  (0:30).
* Subsistência (8:30).
* Existência   (5:00).

### Checklist de necessidades

0. Ambientais (0:00):
    * Ar (CNTP).
    * Chão.
    * Iluminação (conforme ocasião).
1. Circadianas (8:00):
    * Sono.
    * Descanso.
2. Fisiológicas (1:00):
    * Água.
    * Alimentação, mastigando bastante.
    * Purgas!
    * Estética e higiene:
        * Banho.
        * Bucal.
        * Roupa limpa.
        * Limpeza do rosto.
        * Pelos aparados.
        * Unhas lixadas.
        * Bronze.
3. Comportamentais (1:00):
    * Meditação.
    * Exercícios físicos:
        * Postura.
        * Alongamento.
        * Exercícios oculares.
        * Ciclismo.
        * Musculação.
4. Estratégicas (0:30):
    * Organização!
    * Informações táticas (tempo, trânsito, emergências).
5. Subsistência:
    * Trabalho (6:00).
    * Estudo (2:00).
    * Casa (0:30).
6. Existência:
    * Social.
    * Família.
    * Cultura.
    * Atividades apaixonantes.
    * Auto-análise.

## Prioridades básicas

### Metodologia

* O que é mais importante fazer agora?
* Em ordem de realização e escala de tempo padrões.
* Resolver prioridades em ordem de importância!
* É mais importante atacar um pouco de cada prioridade do que focar
  demasiadamente em apenas algumas delas.
* É preciso estar bem para poder fazer o bem.
* Apesar da ordem acima, as pessoas sempre vem antes. Se algum prezado/a
  estiver precisando, a ordem das prioridades muda temporariamente.
* Prioridades de mais importância devem ser realizadas em escala de tempo menor
  (segundos, minutos, horas),
  enquanto que prioridades mais baixas devem ser realizas em escala de tempo
  maior (dias, semanas, meses, anos).
* Prorizar não somente o que for umportante mas que só eu possa ou esteja
  disponível para fazer.

### Priorização

Exemplo de priorização:

0. Preparar-se para a morte. O que é mais importante fazer antes que ela
   chegue, e o que é importante deixar no mundo e para outros seres?

## Referências

* Balance: trouble to change context or change it too often.
* Dose de estoicismo, ascetismo e frugalidade.
* [Erich Fromm's eight basic needs](https://en.wikipedia.org/wiki/Erich_Fromm#Psychological_theory).
* [Maslow's hierarchy of needs - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/Maslow%27s_hierarchy_of_needs).
* [Seis chaves para ser feliz, segundo a Universidade de Harvard](http://brasil.elpais.com/brasil/2015/06/16/ciencia/1434480172_001091.html)
* [A lição de Epicteto: aceitar os fatos é um passo essencial para a serenidade](http://www.diariodocentrodomundo.com.br/aceitar-os-fatos-e-um-passo-essencial-para-a-felicidade/).
* [Zen to Done](http://lucasteixeira.com/ztd/).
* [Comunidade Portuguesa de Ginástica Ocular](http://ginasticaocular.net/).
* [Como Exercitar seus Olhos: 9 Passos (com Imagens)](http://pt.wikihow.com/Exercitar-seus-Olhos).
