# Sobre

## Licença

* [Licença do projeto](LICENSE).

## Repositórios

O conteúdo total deste projeto está disponível em repositório:

* Espelho #1: [https://git.fluxo.info/templates](https://git.fluxo.info/templates).
* Espelho #2: [https://0xacab.org/rhatto/protocolos.git](https://0xacab.org/rhatto/protocolos.git).

## Metadados

* {{ metadata.version.pt }}: `ver:2024-09-14`.
* {{ metadata.identifier.pt }}: `pid:sobre`.
