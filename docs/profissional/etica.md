# Ética profissional

{{ info.context.pt }}

{{ warning.needs_work.pt }}

* A maior diferença de profissionais para não profissionais: ética e atitude.
* Entregar "redondo" para a próxima pessoa/empresa que for prosseguir com o
  projeto/tarefa.

## Exemplo de Ética e procedimentos profissionais

Uma boa relação profissional requer uma definição de ética e procedimentos.
Aqui detalho minha atual política de comportamento profissional.

Futuras modificações neste documento são possíveis, portanto solicite uma
versão mais atualizada caso necessário.

### Ciclo de desenvolvimento

Em geral, meu ciclo de trabalho é o seguinte:

1. Reunião com o cliente, onde as necessidades são elencadas e possíveis
   soluções são vislumbradas (brainstorming).
2. Elaboração e envio de proposta de trabalho, incluindo orçamento e prazos.
3. Etapa de negociação da proposta e, caso aprovada, elaboração e assinatura de
   contrato.
4. Início do desenvolvimento da solução com acompanhamento do cliente.
5. Entrega da solução, da documentação e início de período do suporte.

### Prazos

1. Apenas inicio o trabalho após assinatura do contrato.
2. Não trabalho com prazo curto, urgências ou emergências, especialmente se o
   prazo incluir a apresentação do trabalho a terceiros/as.
3. Contato via email é mais eficaz. Contato via telefone não é garantido,
   principalmente em emergências, já que um retorno imediato nem sempre
   possível. No caso de necessidade de suporte numa ocasião especial, é preciso
   combinar de antemão.
4. Propostas de trabalho tem prazo de validade.
5. Não realizo atividades que não constem no contrato ou que saiam do escopo do
   mesmo.

### Segurança e privacidade

1. Não divulgo e nem disponibilizo informações privadas do cliente a
   terceiros/as.
2. Trabalho tendo em mente a segurança das informações privadas do cliente.

### Acesso à informação

1. Disponibilizo ao cliente o código fonte das aplicações desenvolvidas no
   contrato.

### Divulgação do trabalho

1. A menos que o cliente peça o contrário, os trabalhos desenvolvidos são
   adicionados ao meu portfolio.
