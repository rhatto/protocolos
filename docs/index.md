# Protocolos e Procederes Sociotécnicos

## Sobre

**Modelos, listas, templates! Prontos para usar e se organizar!**

Este é um repositório de protocolos de gestão pessoal e coletiva.

Ele contém procedimentos, comportamentos e regras simples de livre adoção que,
quando combinados, podem contribuir para o surgimento[^emergencia] de padrões
complexos de organização coletiva.

São pequenas máquinas que aumentam a fluidez e a organização de grupos sociais.

{{ info.context.pt }}

## Traduções

Estes protocolos são mantidos prioritariamente no idioma Português, porém
versões noutros idiomas podem estar disponíveis, como por exemplo em [Inglês](en).

[^emergencia]: Ou _emergência_, no sentido de emergir.

## Metadados

* {{ metadata.version.pt }}: `ver:2024-09-14`.
* {{ metadata.identifier.pt }}: `pid:index`.
