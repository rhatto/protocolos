# Principles and process for a network of collectives

{{ info.context.en }}

{{ warning.needs_work.pt }}

Originally [Network intentions and process letter](https://rectech.sarava.org/publico/processo).

# About

The `$network` network is a forum of anti-capitalist, anti-fascist, anti-sexists,
anti-homophobic and anti-racists tech collectives that rejects any form of
social domination and prejudice.

The goal of the forum is to allow interchange, mutual aid and cooperation among
collective members and also to be a public interface for debates.

Then, `$network` is not a collective but a network.

# How it works

The network by default is in a dormant state until be activated, when for example
some collective(s) suggest an activity, meeting or cooperation.

# Membership process

Membership in the `$network` network is restrictet to collectives that comply with
afinity, trust and commitment to the network. New collectives can join the network
after positive decision making process and if they agree with this letter.

Once inside the network, collectives can determine which of its members has to
be subscribed or unsubscribed from the `$network` network communication platforms.

When joining on the `$network` network communication platforms, each individual has
to present her or himself and state that will act in a constructive way,
otherwise will be subjected to unsubscription.

# Decision making process

No person or group can take decisions that affect the network autonomy before
consulting the network and getting a positive consensus.

Decisions in the network are based on proposals that have to be sent to the
network's closed discussion list.

The minimum deadline for decisions is two weeks up to postponing and the
decisions are taken by consensus. If a collective doesn't manifests about a
proposal it will be considered that the collective agrees with the proposal.

# Privacy

The `$network` network is semi-public, i.e, part of it's communication and
organization is restricted to the member collectives while other part is public
and open to any group or person.

Informations that flows in private communication instances needs authorization
to be shared in public media (declassification).

It's a choice of each member to stay or not as a private member.

# Communication interfaces

The `$network` network has the following communication instances:

* Closed list with archives restricted to its members.
* Moderated announcement list with open subscription and open archives.
* Collaborative web platform.

Such interfaces are hosted by member or trusted collectives and it's access and
existence are crucial to the maintenance of the network memory.  Then, it's
imprescindibile to the network to have backups of these platform when needed.

# About this text

This process was based on the "Template for Network Intentions and process letter v1.0".
License for the document: GNU Free Documentation License:
http://www.gnu.org/copyleft/fdl.html
