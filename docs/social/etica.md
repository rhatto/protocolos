# Conjunto de Princípios Éticos

{{ info.context.pt }}

{{ warning.needs_work.pt }}

Baseado nos [princípios das mídias e grupos livres][].

[princípios das mídias e grupos livres]: https://encontro.fluxo.info/Principal/ConjuntoDePrincipiosEticos

0. Sobre a mobilidade dos princípios: Todos os princípios podem ser a qualquer
   momento modificados ou abandonados desde que não sejam mais a expressão
   imanente das relações que se constituem através das ações coletivas.

1. Sobre a autonomia: grupos e mídias livres renunciam e se recusam a recorrer
   a qualquer entidade política que não a si próprias para constituir sua
   legalidade e sua normatividade, por acreditar que a sua única fonte legítima
   é sua emergencia a partir dos laços de confiança e solidarieade entre
   participantes e de cada participante com os coletivos por eles constituídos.

2. Sobre a apropriação pública: As mídias e os grupos livres defendem e
   promovem a apropriação pública dos meios de produção (rejeita a sua
   apropriação privada) e, em específico dos meios de produção de bens
   simbólicos e culturais e aos produtos do trabalho intelectual e imaterial.

3. Sobre o licenciamento: As mídias e os grupos livres usam licenciamento
   livre, apoiam explicitamente os novos direitos autorais e consideram
   especialmente inaceitável a apropriação privada do trabalho intelectual /
   imaterial. aqui, bem que podia aparecer alguma vinculação das licenças com
   propostas de escambo, economia solidária, moeadas locais, permacultura,
   comércio justo... tipo: inventar uma licença que permitisse a circulação em
   sistemas de mercado não-capitalistas. Talvez, exigindo para isso o
   consentimento (tácito?) dos envolvidos

4. Sobre o acesso público: As mídias e os grupos livres criam plataformas de
   comunicação mediática e espaços simbólicos de acesso público em que se
   rejeita absolutamente a monopolização vertical da produção mediática; embora
   estabeleçam princípios éticos e políticos para o acesso aos suportes, não há
   controle sobre a produção de "conteúdo", permitindo que uma pluralidade de
   organizações possam se utilizar dos mesmos canais de comunicação.

5. Sobre a diversidade: As mídias e os grupos livres visam aumentar a
   diversidade dos pontos de vista e estimular o debate argumentativo, recusa
   apoiar práticas da política institucional e veda o proselitismo religioso,
   político-institucional e/ou propaganda comercial.

6. Sobre a gestão: As mídias e os grupos livres usam e desenvolvem
   sistematicamente mecanismos de gestão anti-hierárquicos e baseados na
   geração de consensos a partir da argumentação pública; ou seja, rejeitam (ou
   evitam ao máximo), como práticas de organização: a representação política e
   a votação plebiscitária. A divisão funcional é adotada com ponderação, sob
   avaliação coletiva e de maneira ocasional.

7. Sobre as invenções: As mídias e os grupos livres propiciam e estimulam a
   invenção estética, tecnológica e política, na medida em que tomam a
   organização social, a cultura e o corpo como realidades dignas de serem
   transformados e aperfeiçoados. Nesse sentido, as mídias e os grupos livres
   defendem a liberdade de conhecimento e de acesso a ele; para contribuir com
   a concretização destas liberdades, as mídias e os grupos livres incentivam o
   uso de softwares livres e a publicação em formatos livres (.ogg para áudio,
   .png para imagens, etc.) e em, caso isso não seja possível, em formatos
   proprietários mas que sejam públicos (.rtf e .pdf para textos, .mpg para
   vídeos, etc.). As mídias e os grupos livres não incentivam o uso de formatos
   proprietários (.doc para texto, .ppt para apresentação de slides, etc.).

8. Sobre as pesquisas e as metodologias: As mídias e os grupos livres promovem
   a pesquisa, o desenvolvimento e pratica de metodologias para a apropriação
   dos recursos de comunicação pelos públicos, através do que buscam
   transformar em práticas cotidianas dos cidadãos a produção de comunicação
   mediática e o seu uso público político.

9. Sobre a expansão e a organização em redes: A lógica de expansão das mídias e
   grupos livres segue a de formação dos rizomas (e não da árvores): formam
   novas organizações quando o contingente de participantes aumenta, adensam as
   interconexões (comunicação lateral) entre as organizações e evitam que
   indivíduos e grupos de influência se coloquem no lugar de intermediários
   políticos.

10. Sobre doações: Se recebem dinheiro, o fazem apenas como doação, ou seja:
    qualquer apoiador deve saber que seus recursos não serão empregados senão
    para os fins estritos de criação de espaços comunicativos livres, sendo
    vedadas as práticas de mercantismo cultural, social ou político. Tais
    doações são aceitas apenas se anônimas (isto é, não publicizadas). Dinheiro
    governamental ou empresarial não é aceito.

11. Sobre auto-sustentabilidade: As mídias e grupos livres estimulam a geraçãoo
    de mecanismos de autosustentabilidade (ou "autodependência") local e
    comunitária. Exemplos: venda de camisetas, comidas, rifas, organização de
    festas, mostra de videos, etc. Tratam-se de atividades criadas e
    organizadas para estimular a vivência em coletivo e a escapar das práticas
    capitalistas. É recomendável que, dentro dos grupos e entre eles, exista
    uma socialização dos recursos e que os individuos também adotem essa
    prática, compartilhando recursos pessoais com o coletivo, para criar
    ambientes de solidariedade comunitária, onde ninguém seja excluído por
    falta de recursos.

12. Sobre a gestão financeira: Para garantir essas condições de financiamento,
    toda a gestão financeira das mídias e grupos livres é publica: tanto as
    informações contábeis quanto a participação nas decisões são acessíveis às
    pessoas concernidas nas ações desta organização.

13. Sobre a privacidade: As mídias e os grupos livres defendem a
    inviolabilidade da intimidade e a privacidade do indivíduo, especialmente
    contra sua exploração capitalista, por meio de dispositivos de
    identificação de padrões comportamentais. No caso da implementação de
    sistemas emergentes de identificação de padrões de uso nas plataformas das
    mídias e grupos livres, todos os concernidos têm pleno acesso ao seu
    funcionamento e os podem alterar sempre que desejarem (através de processos
    de formação argumentativa de consensos, vide acima).

14. Sobre a espetacularização: As mídias e os grupos livres não se utilizam da
    espetacularização ou maravilhização. Quer dizer, realmente estamos
    precisados de conceituar e expressar verbalmente o que estamos tomando sob
    "espetacularização" e "maravilhização": quase a totalidade dos grupos de
    hoje trabalham demais com a espetacularização: as ações que escolhem são as
    mais espetacularizadas possíveis (tanto é que não vemos os grupos
    trabalharem com a mesma intensidade em coisas de base ou em estrutura),
    eles tem uma preocupação imensa em mostrar o que estão fazendo, em
    trabalhar com mídia, etc. Isso deve ser uma herança da dita mídia tática e
    de mais um monte de coisa e acho que deveriam ser analisados, tem que ter
    uma crítica. Porque às vezes os logotipos parecem surgir antes mesmo das
    ações.

15. Sobre uma sociedade livre: As mídias e os grupos livres comprometem-se com
    o projeto de construção de uma sociedade livre, igualitária e com respeito
    ao meio ambiente.

16. Sobre a garantia de expressão: As mídias e os grupos livres trabalham no
    sentido de garantir um espaço para que qualquer pessoa, grupo (de afinidade
    política, de ação direta, de artivismo) e movimento social - que estejam em
    sintonia com esses objetivos - possam publicar sua própria versão dos
    fatos.

17. Sobre a distinção entre produtor/a e consumidor/a: As mídias e os grupos
    livres trabalham no sentido de romper o papel de espectador(a) passivo/a e
    transformando a prática midiática ao romper com a mediação do/a jornalista
    profissional e com a interferência de editores/as no conteúdo das matérias.

18. Sobre a transformação da sociedade: As mídias e os grupos livres favorecem
    conteúdos informacionais sobre transformação social ou que retratem as
    realidades dos/as oprimidos/as ou as lutas dos novos movimentos.  Sobre a
    união: As mídias e os grupos livres trabalham no sentido de unir esforços
    para uma real democratização da sociedade, primando sempre por privilegiar
    a perspectiva dos/as oprimidos/as. Em função disso, esperamos uma atitude
    construtiva e tolerante entre os/as participantes do sítio; afinal,
    queremos juntar forças, não lutar entre nós.

19. Sobre a intolerância: As mídias e os grupos livres lutam contra o racismo,
    o sexismo e outros tipos de intolerância.

20. Sobre a remuneração pelo trabalho: As mídias e os grupos livres funcionam
    exclusivamente a partir de trabalho voluntário.

21. Sobre a capitalização sobre trabalho: As mídias e os grupos livres não
    devem permitir que seus voluntários/as capitalizem em cima do seu trabalho
    voluntário, seja adicionando tal trabalho em seu currículo ou seja por
    obter benesses através do uso do nome do grupo ou da mídia livre.

## Outros princípios éticos coletivos

* [Debian Social Contract, Version 1.0](https://www.debian.org/social_contract.1.0).
* Projeto Tor:
    * [Social Contract](https://community.torproject.org/policies/social_contract/).
    * [Statement of Values](https://community.torproject.org/policies/statement_of_values/).
