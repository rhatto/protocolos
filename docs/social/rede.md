# Princípios e processo da rede $rede

{{ info.context.pt }}

{{ warning.needs_work.en }}

Originalmente [Princípios e processo da rede `$rede`](https://rectech.sarava.org/publico/processo).

## Sobre

A rede `$rede` é um fórum de coletivos técnicos anticapitalistas, antifascistas,
antisexistas, antihomofóbicos e antiracistas e que rejeitam qualquer forma
de dominação e discriminação social.

O objetivo do fórum é possibilitar o intercâmbio, a ajuda mútua e a cooperação
entre os coletivos participantes e também uma interface pública de debates.

Assim, `$rede` não é um coletivo mas sim uma rede.

## Como funciona

A rede por padrão permanece em estado de dormência até ser ativada, quando por
exemplo algum(s) coletivo(s) sugere(m) uma atividade, encontro ou cooperação.

## Processo de participação

A participação na rede `$rede` é restrita a coletivos que atendam os critérios de
afinidade, confiabilidade e comprometimento com a rede. Novos coletivos podem
entrar na rede após processo de decisão favorável e caso eles concordem
com a presente carta.

Uma vez dentro da rede, os coletivos podem indicar quais dos seus membros devem
ser inscritos ou retirados das plataformas de comunicação da rede `$rede`.

Cada indivíduo deve, ao ingressar nas plataformas de comunicação da rede `$rede`,
se apresentar e afirmar que atuará de forma construtiva, caso contrário estará
passível de remoção.

## Processo de decisão

Nenhuma pessoa ou grupo pode tomar decisões que afetem a autonomia da rede sem
consultá-la anteriormente e obter um consenso positivo.

As decisões na rede são baseadas em propostas que devem ser enviadas para a
lista de discussão fechada da rede.

O prazo mínimo de decisão é de duas semanas passível de adiamento e as decisões
são atingidas por consenso. Caso um coletivo não se manifeste sobre uma proposta
ele será considerado como em concordância com a proposta enviada.

## Privacidade

A rede `$rede` é semipública, isto é, parte de sua comunicação e organização é
restrita aos coletivos participantes, enquanto que outra parte é pública e aberta
a qualquer grupo ou pessoa.

Informações que circulem em instâncias de comunicação privadas precisam de
autorização para serem disponibilizadas em meios públicos (desclassificação).

É uma escolha de cada participante se manter como membro privado.

## Interfaces de comunicação

A rede `$rede` possui as seguintes interfaces de comunicação:

* Lista de discussão fechada com arquivo restrito a participantes.
* Lista de anúncios moderada com inscrição e arquivos abertos.
* Plataforma web colaborativa.

Tais interfaces são hospedadas por coletivos participantes ou afins e seu
acesso e continuidade são cruciais para a manutenção da memória da rede.
Assim, é imprescindível que a rede disponha de backups dessas plataformas
sempre que necessário.

## Sobre este texto

Este processo foi baseado no "Template para Carta de princípios e processo de
redes v1.0". Licença deste documento: GNU Free Documentation License:
http://www.gnu.org/copyleft/fdl.html
