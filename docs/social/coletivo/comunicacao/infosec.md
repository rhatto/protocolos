# Política de segurança da informação

{{ info.context.pt }}

{{ warning.needs_work.pt }}

O presente processo estabelece uma série de definições e recomendações
relacionadas à segurança da informação circulada por instâncias mantidas pelo
Coletivo.

## Política de senhas e chaves

Recomenda-se que as pessoas participantes de instâncias de informação mantidas
pelo Coletivo assumam uma política de senhas como a seguinte:

1. Não utilizar a mesma senha para sistemas sensíveis.
2. Não utilizar senhas frágeis.

Recomenda-se ainda, que sejam utilizadas aplicações como as seguintes:

* [Monkeysphere](http://web.monkeysphere.info) para auxiliar na autenticação de sistemas remotos.
* [ssss](http://point-at-infinity.org/ssss), para o compartilhamento de senhas em sistemas sensíveis.
* Programas como o [apg](http://www.adel.nursat.kz/apg) para a geração de senhas fortes.
* Programas como o [KeePassXC](https://keepassxc.org/) ou o
  [keyringer](https://0xacab.org/rhatto/keyringer) para o armazenamento seguro
  de senhas.

## Emails suficientemente seguros

Define-se como uma conta de email suficientemente segura aquela que utiliza:

1. [http://en.wikipedia.org/wiki/STARTTLS STARTTLS] nas transmissões para
   outras contas de email suficientemente seguros.
2. É acessada apenas através de conexão SSL, seja HTTPS, IMAPS, POPS ou SMTPS.
3. Criptografia de disco para armazenamento de mensagens no servidor.

A participação em instâncias de comunicação mantidas pelo Coletivo e que não
são totalmente públicas requerem o uso de contas de email suficientemente
seguros.

## Criptografia

Recomenda-se que as pessoas participantes de instâncias de informação mantidas
pelo Coletivo:

1. Armazenem informações internas relacionadas ao mesmo apenas em volumes
   criptografados. Se não tiverem condições de assim procederem com suas máquinas
   pessoais, recomenda-se que armazenem tais informações apenas em instâncias de
   comunicação do Coletivo que possuam transmissão e armazenamento criptografado
   de dados.

2. Utilizem o sistema OpenPGP de criptografia assimétrica para proteção,
   integridade e verificação de procedência de dados sensíveis.

3. Utilizem canais de comunicação criptografados sempre que possível e que não
   utilizem canais não-criptografados para tratar remotamente de questões internas
   ao Coletivo.

## Lista de recomendações e práticas sugeridas

Por se tratar de uma questão complexa e sensível mas por contar com
documentação dispersa, listas adicionais de recomendações e práticas sugeridas
sobre segurança da informação podem ser anexadas ao presente processo.

Eventualmente, recomenda-se que este processo seja atualizado para contemplar
progressos neste campo.

## Criação de contas

A criação de contas em sistemas mantidos pelo Coletivo deve obedecer o seguinte procedimento:

1. Priorizar a escolha de senha pelo titular da conta sem que outra pessoa
   precise conhecê-la, desde que possível.
2. Enviar para o/a usuário:
   a. Pedido de mudança de senha logo que consiga se autenticar nos sistemas em
      questão, caso isso seja possível.
   b. Fornecer fingerprints de chaves de criptografia utilizadas para o acesso da conta.
   c. Se possível, as informações da conta utilizando criptografia e para uma
      conta de email suficientemente seguro do/a usuário.
   d. Uma cópia da lista de recomendações e boas práticas.

## Persistência de dados

Informações armazenados num determinado nível de acesso ou segurança (exemplo,
disco criptografado) devem, por padrão, permanecer nesse mesmo nível ou serem
transferidas para um nível mais seguro, exceto quando constitui informação
desclassificada e permitida para descer de nível.

Telefone e outros meios de comunicação privada que não possuam segurança
suficiente do conteúdo, da origem e do destinatário da informação devem ser
considerados, para todos os efeitos, como meios de comunicação públicos e
portanto não serem utilizados para veicular informações sensíveis.

## Dependências

A realização deste processo depende da realização dos seguintes processos:

* [ACL Lista de acesso à participação (ACL)](/organizacao/comunicacao/acl.html).
