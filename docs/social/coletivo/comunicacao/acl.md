# Lista de acesso à participação

{{ info.context.pt }}

{{ warning.needs_work.pt }}

O presente processo estabelece uma série de camdas de acesso e participação
sobre fluxos informacionais realizadas em instâncias de comunicação mantidas
pelo Coletivo com o objetivo de fortalecer relações de abertura e fechamento do
Coletivo que garantam o máximo de modos de que pessoas de fora possam colaboram
conosco (outsourcing) ao mesmo tempo em que garanta a proteção de informações
sensíveis.

Para isso, este processo se inspira no princípio de que quanto mais forem
públicas as informações, atividades e participações desempenhadas pelo
Coletivo, não só as chances de sustentabilidade aumentam como também as
relações com o campo social se fortalecem. Por outro lado, não se pode ignorar
a vigilância e o controle de massa que ameaçam a integridade e as atividades
dos grupos e movimentos sociais.

Uma forma de segmentar as atividades levando em conta a tensão entre a
tendência de tornar tudo público com o cuidado de manter a integridade das
atividades é dividi-las em grupos referentes à sua possibilidade de
publicização e participação:

1. Quais delas podem ser externalizadas pelo Coletivo, isto é, tornadas
   públicas com
    * a. Feedback de qualquer pessoa ou grupo.
    * b. Feedback apenas de pessoas conhecidas.
    * c. Feedback apenas de pessoas de dentro do Coletivo.
2. Quais delas não podem ser tornadas públicas mas que podem ser compartilhadas
   com pessoas e grupos próximos com
    * a. Feedback apenas de pessoas conhecidas.
    * b. Feedback apenas de pessoas de dentro do Coletivo.
3. Quais delas precisam ser mantidas em sigilo dentro do Coletivo.

Por feedback se entede por poder de leitura e escrita direta, sem necessidade
de mediação do Coletivo. Obviamente que em atividades públicas podem ter
contribuições de terceiros/as, mas tal contribuição pode ser direta (com
feedback ativado) ou indireta, isto é, mediada pelo Coletivo.

Assim, o Coletivo define a seguinte Lista de Camadas de Acesso à Informação ou
Lista de Controle de Acesso (LCA ou ACL):

1. Atividades públicas
    * a. Feedback de qualquer pessoa ou grupo.
    * b. Feedback apenas de pessoas conhecidas.
    * c. Feedback apenas de pessoas de dentro do Coletivo.
2. Atividades vizinhantes
    * a. Feedback apenas de pessoas conhecidas e confiáveis.
    * b. Feedback apenas de pessoas de dentro do Coletivo.
3. Atividades privadas: nossos processos internos.

Em outras palavras, o Coletivo adota um modelo de três camadas que funciona
como uma lista de controle de acesso (ACL) para diversas atividades que possam
ser compartimentalizadas:

* É uma lista de acesso definida por instância de comunicação, dizendo quem e
  como se dá a participação numa dada instância.
* Cada instância ocupa apenas um nível nessa lista. Como exemplo, uma dada
  instância pode ter ACL ''1.c'', ou seja, ser uma instância de realização de
  atividade pública mas apenas com feedback de pessoas do Coletivo.
* Cabe aos processos que definem cada instância de comunicação atribuir um
  nível de acesso.

Recomenda-se que as atividades sejam bem compartimentalizadas (no caso de
utilizarem mais de uma instância) para que se consiga maximizar a publicização
de atividades e proteger apenas os pontos sensíveis.
