# Lista da Vizinhança

{{ info.context.pt }}

{{ warning.needs_work.pt }}

Este processo estabelece as linhas gerais para o funcionamento da vizinhança do
`$coletivo`, definida como o espaço de intercâmbio para o Coletivo, pessoas e
grupos hospedados, amigos e/ou simpáticos.

Os espaços da vizinhança são todos aqueles que se fazem parte do nível 2. da
[Lista de acesso](/coletivo/comunicacao/acl.html), em especial uma lista de discussão por
email denominada de Lista da Vizinhança do $coletivo com nível ALC `2.a`.

## Critérios de participação

Será considerado satisfeito o critério ACL `2.a` para participação na
vizinhança as pessoas ou grupos que:

1. Já estiverem sendo hospedadas pelo Coletivo. Caso o grupo hospedado seja
   muito aberto e muito amplo, em princípio apenas as pessoas relacionadas
   diretamente com a hospedagem OU

2. Mediante processo formal para decidir se tal pessoa ou grupo é confiável o
   suficiente para participar de tal nível de acesso.

Apenas [emails suficientemente seguros](/organizacao/comunicacao/infosec.html) podem
ser adicionados à lista.

## Procedimento de participação

Pessoas que satisfazem os critérios de participação podem pedir inscrição ou
serem convidadas a participar da vizinhança. No ato da inscrição, a seguinte
mensagem de boas vindas com pedido de apresentação e recomendações de
participação deve ser enviada:

    Seja bem vindo/a à lista da vizinhança do $coletivo :)))

    Esta é uma lista composta por pessoas e grupos hospedados ou que são
    considerados/as confiáveis pelo Coletivo. Este é um espaço de intercâmbio, trocas
    e livre associação entre os/as participantes.

    Pedimos por gentileza para que você

      - Se apresente e aproveite para, caso queira, compartilhar sua história,
        objetivos e anseios.

      - Não repasse informações veiculadas nessa lista a terceiros/as sem pedir antes.

      - Evite enviar muitas mensagens, principalmente se relacionadas a divulgações,
        mas sinta-se à vontade para fazer isso quando julgar necessário.

## Responsabilização

Para que seja possível manter a vizinhança, é necessário que as instâncias de
comunicação por ela utilizadas estejam operantes. Assim, o Grupo de Trabalho
formado pelas pessoas responsáveis por esse processo deve:

* Configurar a descrição da lista para "Vizinhança ao Grupo $coletivo".
* Administrar e moderar a Lista da Vizinhança.
* Inscrever e desinscrever pessoas na Lista da Vizinhança, enviando mensagem de
  boas-vindas, termo de aceitação e pedido de apresentação.

## Dependências

A realização deste processo depende da realização dos seguintes processos:

* [Lista de acesso à participação (acl.html)](/organizacao/comunicacao/acl.html).
* [Política de segurança da informação](/organizacao/comunicacao/infosec.html).
