# Conjunto de Licenciamento Livre

{{ info.context.pt }}

{{ warning.needs_work.pt }}

Originalmente em [Encontro: Cultura Livre e
Capitalismo](https://encontro.fluxo.info/Principal/ConjuntoDeLicenciamentoLivre).

## Seções das licenças

Para contemplar inúmeros pontos de vista dos grupos e pessoas participantes
deste espaço, a proposta de licença envolve as seguintes partes:

1. Liberdades atribuídas ao/à licenciado/a
2. Obrigações do/a licenciado
3. Liberdades e obrigações atribuídas ao/à licenciante

## Liberdades atribuídas ao/à licenciado/a

Atribui ao detentor/a da informação as seguintes liberdades:

1. A liberdade de armazenar a informação.
2. A liberdade de manipular a informação.
3. A liberdade de distribuir a informação, modificada ou não.

## Obrigações do/a licenciado

### Restrição obrigatória: "viralidade"

Desde que esta licença acompanhe a informação.
Restrição ética ou mercantil

### As seguintes opções mutuamente exclusivas são possíveis para a montagem de uma licença:

1. Contanto que o detentor da informação pactue com os princípios das mídias e grupos livres.
2. Restrição comercial: Desde que para fins não-comerciais.
3. Restrição de lucro: Desde que para fins não-lucrativos.

### Restrição de autoria e fonte

As seguintes opções não-exclusivas são possíveis para a montagem de uma licença:

1. Desde que o autor seja citado.
2. Desde que a fonte seja citada.

### Restrição de distribuição

Essa restrição obriga que a pessoa que utilizar a informação a:

1. Caso ocorra uma modificação, distribuir a informação modificada.
2. Ou, eventualmente, distribuir a informação para qualquer um dos usos que ela estiver sujeita.

Opcionalmente, uma das seguintes restrições de notificação podem ser utilizadas:

1. Caso o conteúdo seja distribuído, a fonte deve ser notificada antecipadamente.
2. Ou, caso o conteúdo seja distribuído, a fonte deve ser notificada
   antecipadamente caso existam modificações.

## Liberdades e obrigações atribuídas ao/à licenciante

As seguintes opções não-exclusivas são possíveis para a montagem de uma licença
e apenas fazem sentido se a informação preserva autoria ou fonte:

1. O/a autor/a pode a qualquer momento revogar o licenciamento da informação
   para uma determinada pessoa ou entidade.
2. O/a autor/a apenas pode licenciar sua informação sob a licença se ele
   declarar que não relicenciará a informação.

## Licença do Conjunto de Licenciamento Livre

Atribui ao detentor/a do texto do Conjunto de Licenciamento Livre as seguintes liberdades:

1. A liberdade de armazenar o texto do Conjunto.
2. A liberdade de manipular o texto do Conjunto, inclusive para criar novas licenças.
3. A liberdade de distribuir o texto do Conjunto, modificada ou não.

### Obrigações do/a licenciado

1. Desde que esta licença acompanhe a informação.
2. Restrição de lucro: Este Conjunto de Licenciamento Livre e esta licença
   podem ser utilizados apenas para criar licenças e licenciar conteúdos com
   finalidades não lucrativas.

### Licença da licença

O texto desta licença está licenciado por ele mesmo.

## Exemplo

Estabelece o seguinte texto como a licença padrão de distribuição de conteúdo
produzido pelo Coletivo:

    1. Licença de Manipulação de Informações do Grupo $coletivo

    Licença baseada no Conjunto de Licenciamento Livre[1] do Encontro: Cultura
    Livre e Capitalismo[2].

    2. Liberdades atribuídas ao/à licenciado/a

    Atribui ao detentor/a da informação as seguintes liberdades:

       1. A liberdade de armazenar a informação.
       2. A liberdade de manipular a informação.
       3. A liberdade de distribuir a informação, modificada ou não.

    Desde que as condições listada na seção Obrigações do/a licenciado/a a seguir
    sejam respeitadas.

    3. Obrigações do/a licenciado/a

    3.1 Viralidade

    Desde que esta licença acompanhe a informação.

    3.2 Restrição mercantil

        * Desde que para fins não-comerciais.

    3.3 Restrição de citação

        * Desde que a fonte seja citada.

    3.4 Restrição de distribuição

        * Caso o conteúdo seja distribuído por você, o Grupo $coletivo deve ser
          notificado antecipadamente.
        * Caso ocorra uma modificação, distribuir a
          informação modificada e notificar antecipadamente o Grupo $coletivo.

    4. Liberdades e obrigações atribuídas ao/à licenciante

        * O Grupo $coletivo pode a qualquer momento revogar o licenciamento da
          informação para uma determinada pessoa ou entidade.

    [1] https://protocolos.fluxo.info/organizacao/comunicacao/license/
    [2] http://encontro.fluxo.info

Não é obrigatória nem compulsória a utilização desta licença, mas conteúdo
veiculado pelo ou em nome do Coletivo a utiliza por padrão caso não haja menção
formal em contrário.

### Responsabilização

A(s) pessoa(s) responsável(is) pelo processo ficam encarregadas de manter o
texto da licença em um local público acessível via web para que possa ser
referenciado por outros textos do Coletivo, além de adicionar a seguinte nota
antes do texto da licença:

`
Desde que não mencionado em contrário, todo o conteúdo produzido pelo Grupo
$coletivo é distribuído de acordo com a Licença de Manipulação de Informações do
Grupo $coletivo.
`
