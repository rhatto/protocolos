# Instâncias de comunicação de backup

{{ info.context.pt }}

{{ warning.needs_work.pt }}

Este processo estabelece procedimentos para comunicação de backup para

* Lista de email.
* Bate-papo.

## Responsabilização

O Grupo de Trabalho responsável por este processo deve manter uma lista de
email e uma sala de bate-papo de [ACL](/coletivo/comunicacao/acl.html)
nível 3 para participação -- isto é, canal de acesso apenas para pessoas
do Coletivo -- para serem utilizados quando alguma das instâncias padrões
estiverem com problemas.

Os critérios de funcionamento dessas instâncias de backup são os mesmos para
as instãncias usuais equivalentes.

Recomenda-se também que, se possível, as pessoas do Coletivo possuam emails
adicionais que satisfação a
[Politica Política de segurança da informação](/coletivo/comunicacao/infosec.html)
e que possam ser utilizados como backup.

## Dependências

A realização deste processo depende da realização dos seguintes processos:

* [Política de segurança da informação](/coletivo/comunicacao/infosec.html).
* [ACL Lista de acesso à participação (ACL)](/coletivo/comunicacao/acl.html).
