# Transparência e compartilhamento de informações e protocolos

{{ info.context.pt }}

{{ warning.needs_work.pt }}

O presente processo estabelece critérios de transparência e compartilhamento de
informações e protocolos desenvolvidos dentro e fora do Coletivo.

## Protocolos

No âmbito do presente processo, entende-se por "protocolos" o conteúdo textual
de templates, protocolos ou propostas de processo e não especificidades de
determinadas instâncias processuais ou mesmo informações de responsabilização e
realização das mesmas.

## Compartilhamento

Protocolos que podem ser publicizados por padrão em nível [ACL](/coletivo/comunicacao/acl.html)
''1.a'' ou superior são todos aqueles que

1. Não mencionam explicitamente o Coletivo, grupos ou pessoas E QUE
2. Não contenham informações sensíveis, privadas ou particulares.

Protocolos que podem ser publicizados por padrão em nível [ACL](/coletivo/comunicacao/acl.html)
''2.a'' ou superior são todos aqueles que

1. Afetem as atividades vizinhantes e dos grupos hospedados E QUE
2. Não mencionam explicitamente grupos ou pessoas E QUE
3. Não contenham informações sensíveis, privadas ou particulares.

Cada processo formal pode alterar seu estado de transparência protocolar.

## Instâncias de compartilhamento protocolar

Muitos dos protocolos e processos desenvolvidos dentro do Coletivo podem ser
úteis para outros grupos. Da mesma forma, desenvolvimentos similares que
ocorram fora do Coletivo podem servir de inspiração para processos internos.

Assim, o Coletivo permite por padrão que os protocolos que possam ser
compartilhados em nível [ACL](/coletivo/comunicacao/acl.html) ''1.a'' detalhados na seção
anterior sejam compartilhados ou integrados à linha de desenvolvimento nos
seguintes locais:

* Sítio "Protocolos do Coletivo", que deve existir em
  `http://protocolos.$dominio` e que pode conter também análises dos protocolos.
* Eventualmente em Resource Sharing Protocol (`http://rsp.$dominio`), caso aplicável.
* Em outros locais, mediante pedido formal ao Coletivo.

## Responsabilização

As pessoas responsabilizadas pelo presente processo ficam encarregadas de
manter o sítio "Protocolos do Coletivo".

## Dependências

A realização deste processo depende da realização dos seguintes processos:

* [Lista de acesso à participação (acl.html)](/organizacao/comunicacao/acl.html).
