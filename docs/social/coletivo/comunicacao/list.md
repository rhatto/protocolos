# Administração da Lista do Coletivo

{{ info.context.pt }}

{{ warning.needs_work.pt }}

Este processo estabelece as linhas gerais para a administração da Lista de
Discussão do Coletivo, estabelecida com o [Protocolo de Ação do $coletivo](/coletivo/coletivo/).

## Critérios de participação

1. Para participar da lista do Coletivo, uma pessoa precisa satisfazer o nível
   `3` de [ACL](/coletivo/comunicacao/acl.html), isto é, fazer parte do Coletivo.
2. Apenas [emails suficientemente seguros](/coletivo/comunicacao/infosec.html)
   podem ser adicionados à lista.

## Responsabilização

Para que seja possível manter tal lista, é necessário que as instâncias de
comunicação por ela utilizadas estejam operantes. Assim, o Grupo de Trabalho
formado pelas pessoas responsáveis por esse processo deve:

* Garantir, na medida do possível, a existência da Lista do Coletivo e
  mantendo a documentação correspondente.
* Administrar e moderar a Lista do Coletivo.
* Inscrever e desinscrever pessoas na Lista do Coletivo.

## Dependências

A realização deste processo depende da realização dos seguintes processos:

* [Organização do Coletivo](/coletivo/coletivo).
* [Lista de acesso à participação (acl.html)](/coletivo/comunicacao/acl.html).
* [Política de segurança da informação](/coletivo/comunicacao/infosec/).

## Administração da Lista de Mensagens de Sistema

Este processo estabelece as linhas gerais para a administração da Lista de
Mensagens de Sistema, utilizada para receber mensagens de sistema das diversas
camadas do Coletivo.

## Critérios de participação

1. Para participar da lista do Coletivo, uma pessoa precisa satisfazer o nível
   `3` de [ACL](/coletivo/comunicacao/acl.html), isto é, fazer parte do Coletivo.

2. Apenas [emails suficientemente seguros](/coletivo/comunicacao/infosec/) podem ser
   adicionados à lista.

## Responsabilização

Para que seja possível manter tal lista, é necessário que as instâncias de
comunicação por ela utilizadas estejam operantes. Assim, o Grupo de Trabalho
formado pelas pessoas responsáveis por esse processo deve:

* Garantir, na medida do possível, a existência da Lista de Mensagens de
  Sistema e manter a documentação correspondente.

* Administrar e moderar a Lista de Mensagens de Sistema.

* Inscrever e desinscrever pessoas e emails administrativos na Lista de
  Mensagens de Sistema.

Observação: emails administrativos (isto é, emails de sistema) devem ser
cadastrados com a recepção de mensagens desabilitadas, a não ser em casos
especiais em que isso se fizer necessário.

## Dependências

A realização deste processo depende da realização dos seguintes processos:

* [ACL Lista de acesso à participação (acl.html)](/organizacao/comunicacao/acl.html).
