# Armazenamento de documentos

{{ info.context.pt }}

{{ warning.needs_work.pt }}

O armazenamento de documentos consiste no depósito organizado e seguro de
documentos físicos (isto é, impressos) do Coletivo, consistindo nas seguintes
tarefas:

1. Manter os documentos do Coletivo (notas fiscas, contratos, acordos, etc)
   armazenados em local protegido (de umidade, desgaste, mofo, incêndio, roubo,
   etc) e observando a política de segurança da informação do Coletivo.

2. Fornecer os documentos (ou cópias dos mesmos) ao Coletivo conforme
   solicitação e zelando para que os mesmos retornem ao depósito ou se mantenham
   em condições compatíveis de armazenamento. Os documentos devem ser fornecidos
   num prazo de até '''uma semana''' (incluindo finais de semana e feriados) após
   a solicitação.

3. Manter cópias digitais (escaneadas ou fotografadas) dos documentos em
   instância de comunicação fechada do Coletivo.

## Responsabilização

Cada pessoa responsabilizada pelo armazenamento de documentos é denominada de
"fiel depositária dos documentos do Coletivo".
