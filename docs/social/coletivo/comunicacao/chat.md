# Bate-papo

{{ info.context.pt }}

{{ warning.needs_work.pt }}

Este processo estabelece as linhas gerais para a administração dos sistemas de
bate-papo utilizados pelo Coletivo. Tais sistemas permitem a comunicação
praticamente em tempo real. Se utilizadas com critérios de segurança e
privacidade, as salas de bate-papo podem desempenhar um ótimo papel para a
comunicação rápida no Coletivo.

## Canais

Os seguintes canais são definidos como instâncias de comunicação informais do Coletivo:

* `#$canal_aberto`: [ACL](/coletivo/comunicacao/acl.html) nível 1.a ou superior para
  participação, ou seja, um canal de acesso público onde não se deve discutir
  ou divulgar assuntos internos do Coletivo.

* `#$canal_fechado`: [ACL](/coletivo/comunicacao/acl.html) nível 3 para participação,
  isto é, canal de acesso apenas para pessoas do Coletivo, sem restrição de assuntos.

* temporários, para o caso de pessoas do Coletivo precisarem conversar com terceiros/as
  de modo privativo, com [ACL](/coletivo/comunicacao/acl.html) nível 2.a ou superior.

## Modos de configuração

Os seguintes modos de configuração são requeridos para canais privativos:

* Entrada restrita a membros do Coletivo.
* Sem exibição na listagem de canais do servidor (no caso do IRC, modo +s e/ou +p).
* Acesso via SSL ou outro métodos de criptografia assimétrica (IRC, SILC, etc).

## Responsabilização

Cabe ao Grupo de Trabalho formado pelas pessoas responsáveis pelo presente
processo manter o registro, a manutenção, a operação e a documentação
relacionada aos canais de bate-papo do Coletivo, levando em consideração:

* [Os critérios de segurança e privacidade](/coletivo/comunicacao/infosec.html).
* O nível de acesso de cada um dos canais, não permitindo pessoas que não
  tenham o devido acesso a participarem de determinados canais.
* Que muito de ausência dos/as operadores do canal pode levar à perda do seu registro.

## Dependências

A realização deste processo depende da realização dos seguintes processos:

* [Política de segurança da informação](/coletivo/comunicacao/infosec.html).
* [Lista de acesso à participação (ACL)](/coletivo/comunicacao/acl.html).
