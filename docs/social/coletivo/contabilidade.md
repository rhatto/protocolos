# Contabilidade

{{ info.context.pt }}

{{ warning.needs_work.pt }}

A [Contabilidade](/coletivo/contabilidade), por possibilitar a manipulação de
recursos do coletivo que podem ser usados para a aquisição, manutenção e
proteção de outros recursos, representa um ganho em autonomia.

A contabilidade consiste nas seguintes tarefas:

1. Efetuar os depósitos, saques e transferências em uma conta bancária (gestão
   financeira), observando os [critérios](/coletivo/contabilidade/criterios)
   sobre:
    * a. Doação (de e para o Coletivo).
    * b. Arrecadação.
    * c. Gastos.
2. Transparência
    * a. Manter um balanço atualizado em local de acesso restrito ao Coletivo.
    * b. Fornecer os dados contábeis de acordo com os [critérios de
       transparência](/coletivo/contabilidade/criterios) estabelecido pelo Coletivo.

## Situações emergenciais e não-emergenciais

O grupo de trabalho formado pelas pessoas responsáveis por este processo se compromete a:

1. Em casos não-emergenciais, atender requisições (depósitos, saques, etc e
   cujo uso estiver aprovado) em até '''2 semanas''', isto é, '''14 dias incluindo
   finais de semana e feriados'''.
2. Em casos de emergência, disponibilizar recursos (cujo estiver aprovado) em
   até '''48 horas''', mantendo o Coletivo informado nas situações em que tal
   prazo não puder ser atendido (por exemplo no caso férias, viagens, etc).

## Responsabilização

As pessoas responsáveis ficam encarregadas, além das tarefas contábeis
mencionadas anteriormente, de:

1. Fornecer ao menos conta bancária que possa ser utilizada para o
   armazenamento de dinheiro do Coletivo e manter tal informação atualizada em
   local de acesso restrito ao Coletivo.
2. Antes de deixarem de ser responsáveis pelo processo (i.e, antes de saírem
   dele), de transferirem os recursos financeiros do Coletivo que estejam de posse
   para os/as responsáveis remanescentes.
