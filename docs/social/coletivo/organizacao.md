# Protocolo de Ação Coletiva

{{ info.context.pt }}

{{ warning.needs_work.pt }}

Possíveis interpretações sobre o significado e o efeito deste protocolo se
encontram [aqui](/organizacao/coletiva/interpretacoes).

* Versão: 1.0.
* Licença: LIMICS[1].
* Protocolos apresentados neste projeto podem ser aplicados através do esquema
  de "processos formais" definidos neste documento, o que inclui até mesmo o
  presente protocolo.

## Processos e autonomia

Tudo o que ocorre no Coletivo é um processo. Os processos assumem diversas
manifestações, mas principalmente são fluxos e registros desses fluxos
(memória/informação).

Existem dois tipos de processos:

* Processos formais
    * Forma necessariamente definida de antemão via consenso do coletivo E
    * Lidam com a autonomia do coletivo. PORTANTO
    * Precisam ser acompanhados pela responsabilização mínima para o processo não
      falhar por falta de iniciativa

* Processos informais
    * Forma não necessariamente definida de antemão E
    * Não afetam a autonomia do coletivo. PORTANTO
    * Não precisam necessariamente estar atrelados à responsabilidade de alguém
      (isto é, a não-realização de um processo informal não afeta a autonomia do
      Coletivo)

Atividades sem informação disponibilizada no Coletivo não podem ser
consideradas como processos (formais ou informais) do Coletivo porque não
dispõem de igualdade de acesso à informação, requisito para a possibilidade de
participação (isonomia informacional).

A autonomia básica do Coletivo, isto é, a autonomia mínima que garante a sua
existência de acordo com este protocolo, é a posse de canais (instâncias) de
comunicação privados e seguros que permitam a existência dos registros de
processos coletivos (formais ou informais). Sem esses canais, a autonomia
básica do Coletivo é seriamente abalada, assim como a aplicação deste
protocolo. Toda autonomia adicional do Coletivo (isto é, que não for a
autonomia básica) deve ser definida através de processos formais.

Um/a integrante do Coletivo atua dentro dele quando utiliza os recursos e o
nome do Coletivo. Por outro lado, um/a integrante do Coletivo atua fora dele
quando não utiliza os recursos ou o nome do Coletivo. Intregrantes do Coletivo
não realizam ações (dentro ou fora do Coletivo) que, conscientemente, possam
prejudicar a autonomia do Coletivo.

## Processos Formais

Os processos formais possuem as etapas e os andamentos de acordo com o
fluxograma a seguir:

         .------------------->-----------------.
        /  .----------<--------------<-------.  \
       |  '                                   \  \
       |  |               .------>-----.       \  \
       |  |              |              \       \  \
      Proposta -----> Discussão ->--.    \       \  \
         |  ^            |           \    \       \  \
         |  |            |            \    \       \  \
         |   `----<-----'             |     \       \  \
         |                            |      |       \  \
          `------>------ Decisão --<--'      |        \  \
                           | |               |         \  \
                           | |               |          | |
       Atribuição de --<---' '---> Arquivamento --->---'  ;
     Responsabilidades ----->-------'   ^    \           /
          ^  |              ___________/      `---<-----'
          |   \           .'
          |    `--> Realização -->--.
          |           |  |           \
          |           |  |           /
           `----<-----'   `-----<---'

* Proposta: etapa na qual a idéia de um procedimento formal é lançado ao
  Coletivo. A idéia -- ou descrição -- do processo pode vir do Arquivo de
  propostas, de uma Discussão anterior, de um procedimento informal que se
  julga importante formalizar ou mesmo de uma pessoa ou grupo de pessoas de
  dentro ou de fora do Coletivo. Recomenda-se que ela seja bem explicada e
  contenha: sugestão de prazo de decisão, ciclo de vida do processo, critérios
  e prazo para atribuição de responsabilidade assim como recomendações para
  situações emergenciais (quando aplicável).

* Discussão:
    * Não é uma etapa estritamente necessária, mas não deixa de ter importância.
    * Alterações em propostas fazem com que o procedimento formal em questão
      volte para a etapa de Proposta. Propostas que não seguirem para a etapa de
      Decisão ou que não forem alteradas até o prazo proposto devem ser
      arquivadas.
    * Propostas que vem de fora do Coletivo ou que tenham como participantes
      grupos ou pessoas de fora do coletivo e que forem discutidas e alteradas
      devem ser enviadas também para o grupo ou à pessoa de fora do Coletivo
      responsável pela sua introdução, apesar destas pessoas não participarem
      da discussão interna do Coletivo. Se tal pessoa ou grupo concordar com a
      proposta alterada, então o processo formal em questão retorna à etapa de
      Discussão com a nova proposta. Caso contrário, isto é, a pessoa ou grupo
      de fora do Coletivo não concordar com a proposta alterada, então o
      processo formal em questão é arquivado (exceto se as partes externas
      apresentarem uma nova alteração à proposta ou mais argumentos à
      discussão).

* Decisão:
    * Via consenso e a participação ativa depende do acompanhamento das
      informações do Coletivo requeridas pela proposta em questão.
    * Se não há consenso sobre a aprovação de uma proposta, a mesma permanece
      bloqueada, podendo ter seu prazo estendido.
    * São considerados dois caminhos possíveis para a tomada de decisão caso
      haja silêncio. Cada uma delas tem vantagens e desvantagens e pode depender
      do contexto do Coletivo. Recomenda-se que a opção escolhida seja pactuada
      explicitamente por todas e todos participantes logo na adoção deste Protocolo:
        * Opção 1: "quem cala não consente", isto é, aprovação somente com
          consenso explícito não-silencioso; neste caso, é considerado por
          padrão que ninguém topa participar ou concorda com a proposta.
        * Opção 2: manter-se em silêncio é considerado como concordância com a
          proposta em questão.
    * Prazo: recomenda-se que os mesmos sejam estipulados relativamente ao
      tempo que as pessoas ativas no coletivo tomarem conhecimento, discutir,
      propor alterações, pedirem eventuais adiamentos, etc, sendo passíveis de
      prorrogação ou antecipação através de um pedido explícito por alguma
      pessoa do Coletivo. No entanto, se não há pedido para alteração de prazo,
      a data inicial da proposta deve ser respeitada.
    * Aprovações de propostas que vem de fora do Coletivo ou que tenham como
      participantes grupos ou pessoas de fora do coletivo são comunicadas às
      pessoas/grupos de fora do Coletivo apenas após a atribuição de
      responsabilidades.

* Atribuição de Responsabilidades:
    * Minimização de pontos de falha.
    * Responsabilização voluntária, mas que exige envio de termo de
      comprometimento/responsabilização afirmando que:
        * Tem conhecimento sobre o procedimento em questão.
        * Irá realizá-lo dentro do prazo estipulado, que manterá o Coletivo
          informado sobre a sua realização.
        * Caso não possa mais arcar com a responsabilidade, avisará o Coletivo
          com antecedência suficiente para que o mesmo possa, dependendo do
          caso, manter a realização do processo, atribuir novas
          responsabilidades a ele ou então simplesmente encerrá-lo e
          arquivá-lo.
    * O não-cumprimento de uma responsabilidade compromete a atribuição de
      outras responsabilidades. Além disso, a atribuição de uma
      responsabilidade é voluntária e deve ser feita por escrito para fins de
      documentação e para evitar mal-entendidos e problemas de comunicação.
    * Processos formais que forem aprovados mas que, findo o prazo para a
      responsabilização, não tiverem responsabilização suficiente atribuída,
      devem seguir para o arquivamento, sendo que o desarquivamento de
      propostas anteriormente aprovadas não pode seguir diretamente para a
      atribuição de responsabilidade, mas sim seguir para a etapa de
      proposição.
    * No caso de propostas que vem de fora do Coletivo ou que tenham como
      participantes grupos ou pessoas de fora do coletivo são comunicadas às
      pessoas/grupos de fora do Coletivo sobre seu estado de
      Aprovação/Realização apenas após a atribuição de responsabilidade, isto
      é, ao final desta etapa.

* Realização:
    * Apenas processos formais cuja responsabilização foi atribuída podem partir
      para a etapa de realização. Processos que forem realizados e que não
      tiverem prosseguimento definido são arquivados.
    * Processos formais que, não tendo sido realizados no prazo comprometido pelo
      grupo das pessoas que se responsabilizaram por ele, devem retornar à etapa
      de Atribuição de Responsabilidades. De modo análogo, processos em
      realização mas cujos/as responsáveis não puderem mais realizá-los devem
      retornar à etapa de Atribuição de Responsabilidades caso o número de
      pessoas responsáveis remanescentes não for suficiente para a sua
      realização.

* Arquivamento:
    * Propostas que:
        * Foram aprovadas mas não foram adotadas responsavelmente OU
        * Foram realizadas e encerradas OU
        * Estavam em realização mas não tem mais o número de pessoas
          responsáveis suficiente, por exemplo: quando ninguém ou apenas um
          número insuficiente de pessoas estiverem cuidando de um dado recurso.
    * No caso de um processo que estava sendo realizado e precisar ser
      arquivado por falta de pessoas responsáveis por ele, as últimas pessoas
      responsáveis por ele devem realizar o procedimento de encerramento e
      arquivamento.
    * No caso de propostas que vem de fora do Coletivo ou que tenham como
      participantes grupos ou pessoas de fora do coletivo são comunicadas às
      pessoas/grupos de fora do Coletivo sobre seu estado Recusa/Arquivamento
      apenas nesta etapa.

## Dependências entre processos

1. Processos formais que explícita ou implicitamente dependam de outros
   processos formais podem ter vínculo de dependência estabelecido.
2. Processos formais em realização cujas dependências se encontrarem arquivadas
   são passíveis de arquivamento.

## Referências

* [1] Licença de Manipulação de Informações do Grupo Saravá - http://sarava.fluxo.info/Main/Licenca
