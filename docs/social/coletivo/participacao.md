# Participação de pessoas no Coletivo

{{ info.context.pt }}

{{ warning.needs_work.pt }}

O presente processo trata da Entrada e saída de pessoas no Coletivo,
estabelecendo assim a participação de pessoas no Coletivo no nível 3 de
[ACL](/coletivo/comunicacao/acl).

## Participação de pessoas

A participação de cada pessoa no nível 3 de ACL deve ser dar através de um processo formal, onde:

1. Nas etapas de proposta e discussão da participação deve ocorrer a
   aproximação da pessoa com o Coletivo.
2. A etapa de realização do processo está dividida nas seguintes fases:
  a. Entrada tipo "trainee".
  b. Participação efetiva.
  c. Saída/afastamento, quando o processo de participação da pessoa no nível
     ACL 3 é arquivado, podendo ser desarquivado no futuro.

## Aproximação com o Coletivo

Nesta etapa, a pessoa e o Coletivo procuram se aproximar e se conhecer e ambos
avaliam a vontade de prosseguir com a participação.

## Entrada de treinamento

Caso haja prosseguimento do processo, a pessoa passa por um período de
experiência e treinamento, onde:

1. Toma conhecimento do funcionamento e das atividades do Coletivo.
2. É auxiliada por alguma pessoa do Coletivo que se voluntaria de guia.
3. Aprende a utilizar os instrumentos técnológicos básicos do Coletivo.

Mas que:

1. Não tem acesso a chaves, senhas ou contas em camadas do Coletivo.

Antes de entrar no período de participação, a pessoa precisa concordar
explicitamente que respeitará:

1. Os processos do Coletivo.
2. O sigilo e a privacidade do Coletivo, mesmo no caso de deixar de participar
   do mesmo.

A fase de treinamento se encerra quando a pessoa sentir que já pode participar
efetivamente do Coletivo.

## Participação efetiva

Após passar pelo período de treinamento, a pessoa se integrará efetivamente no
Coletivo, podendo assumir responsabilidades e ter acesso a todas as camadas e
interfaces do Coletivo.

Para que continue com tal participação, um mínimo de comprometimento é
necessário

1. Ter ciência do que aconteceu nos últimos tempos dentro do Coletivo.
2. Participar de alguma forma nas discussões do Coletivo.
3. Caso não possa arcar com 1 ou 2, deve ao menos responder nos processos de
   [roll call](/organizacao/misc/rollcall).

## Critérios de segurança

Levando em conta que o Coletivo abriga informações de muita gente, é importante
que exista um nível de segurança mais alto do que a média):

1. Ter a pasta pessoal e área de troca (swap) criptografadas.
2. Utilizar senhas seguras.
3. Utilizar criptografia GPG.
4. Verificar fingerprints em servidores, etc.
5. Demais [recomendações](/organizacao/comunicacao/infosec).

## Privacidade

É uma escolha de cada participante se manter como membro privado ou mencionar
que faz parte do Coletivo, seja publicamente ou em círculos restritos.

## Congelamento e término de participação

Nos casos de problemas pessoais, o Coletivo pode, mediante processo formal,
congelar temporariamente a participação de alguma pessoa no Coletivo.

No caso de congelamento ou término de participação, a pessoa perde os acessos
às camadas e interface de ACL 3 do Coletivo.

## Responsabilização

O Grupo de Trabalho formado pelas pessoas responsáveis pelo presente processo
fica encarregado de zelar pela aplicabilidade do presente processo e de
certificar que os acessos às interfaces e camadas das pessoas que se desligarem
ou se afastarem do Coletivo sejam removidos.

## Retroatividade

O presente processo é retroativo, isto é, mesmo quem já participa do Coletivo
antes da vigência do processo precisa passar por ele, por uma questão de
isonomia.
