# Protocolo de Ação do Coletivo

{{ info.context.pt }}

{{ warning.needs_work.pt }}

!!! info "Versão revisada e comentada"

    Uma versão revisada e comentada deste documento encontra-se
    [aqui](https://saraventos.fluxo.info/protocolo.html), como
    parte da coletânea [Saraventos](https://saraventos.fluxo.info).

* Versão: 1.0.
* Licença: LIMICS[1].

O Coletivo `$coletivo` adota a versão 0.1 do "Protocolo de Ação Coletiva"[2],
de modo que se tome por "Coletivo `$coletivo`" toda ocorrência da palavra
"Coletivo" no referido texto.

## Instâncias de Comunicação

Conforme a atual configuração tecnológica do Coletivo $coletivo, as principais
instâncias de comunicação utilizadas são:

* Wiki/sistema de tickets fechado: `https://admin.$dominio`.
* Lista de discussão: `$lista_de_discussao`.
* Demais meios de comunicação que satisfaçam requisitos de privacidade e segurança.

## Processos e tickets

A manifestação que os registros de processos assumem no Coletivo `$coletivo`
são chamados de tickets ou requisições. Todos os processos devem ser tickets.
Processos informais não precisam necessariamente ser tickets antes da sua
realização, mas para que posteriormente possam ser considerados como processos
precisam virar tickets (isto é, precisam de um mínimo de documentação).

## Formalidade e informalidade de instâncias

Com relação à formalidade e informalidade das instãncias de comunicação, temos que:

* Todas as instâncias de comunicação são informais ''a priori'' (isto é, se
  nada mais for dito a respeito da forma de cada uma delas).
* As instâncias formalizadoras (isto é, as instâncias onde deve ser informada
  da proposição e aprovação de um processo para que o mesmo tenha validade
  formal) são o sistema de tickets e a lista de discussão.

## Recomendação sobre jardinagem de discussões

Levando em consideração que

* É interessante manter um fluxo de emails baixo com mensagens pequenas para
  que a lista de discussão possa ser bem usada especialmente em urgências.
* Wiki e sistema de tickets são muito úteis para lidar com grandes quantidades
  de informação, apesar de não serem muito bons para a obtenção de feedback
  rápido.
* No caso de pendências e tarefas, o sistema de tickets é mais confortável para
  o acompanhamento de atividades.

Recomenda-se que

* Se possível, as discussões sejam originadas nos meios que lhes forem mais
  propícios (alguma emergências se iniciam melhor com o envio de um email, onde
  obtém melhor resposta).
* Caso um meio se torne inadequado para a manutenção da discussão, que a mesma
  seja transferida para um meio mais adequado mas que tal transferência seja
  acompanhada pelo referenciamento mútuo em ambas as instâncias (na instância
  onde ela deixar de ocorrer envia-se uma mensagem indicando para onde a
  discussão está sendo encaminhada e nesta última se adiciona uma indicação
  sobre onde a discussão veio.
* Discussões que adquiram vulto sejam transformadas em processos informais (com
  a criação de um ticket com respectivo link, se aplicável, para o local onde a
  discussão está sendo realizada).

## Procedimento para Processos Formais

Além de obedecer ao fluxograma de processos formais detalhado no texto
"Protocolo de Ação Coletiva", a proposição e a decisão de propostas formais
devem ser realizadas da seguinte forma para serem válidas:

* Para enviar uma proposta formal, primeiramente crie um ticket.
* Em seguida, envie um email para a lista de discussão informando da proposta e
  incluindo, pelo menos, o link do ticket.
* A discussão e alteração da proposta pode ser feita apenas pelo ticket, pela
  lista de discussão ou mesmo em instâncias informais, mas recomenda-se que se
  utilize o ticket como agregador do maior número possível de informações
  discutidas a respeito da proposta.
* Discussões realizadas em instâncias informais não tem valor formal se não
  forem documentadas e apresentadas como tal na lista de discussão, no ticket
  ou eventual página wiki, observando a recomendação sobre jardinagem de
  discussões.
* Ao passar o prazo de decisão da proposta, é necessário  enviar um email de
  comunicação à lista de discussão para que a decisão seja formalizada.

Conforme o processo formal em questão for passando pelas etapas formais, o
estado do ticket deve ser alterado pelas pessoas que se interessarem por
fazê-lo. Se necessário, a proposta também pode ter ao menos uma página no wiki
fechado, sendo possível usar o wiki para acompanhar diferentes versões de uma
proposta, por exemplo.

## Referências

* [1] [Licença de Manipulação de Informações do Coletivo $coletivo](/organizacao/comunicacao/license).
* [2] [Protocolo de Ação Coletiva](/organizacao/coletiva.
