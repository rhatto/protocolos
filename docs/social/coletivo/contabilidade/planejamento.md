# Planejamento financeiro

{{ info.context.pt }}

{{ warning.needs_work.pt }}

## Plano

O Coletivo adota o seguinte plano financeiro baseado numa reserva mínima e no seu excedente:

1. A reserva mínima é a quantidade de dinheiro necessária para arcar com a soma (total: `R$x`) de:
  a. Gasto `a`.
  b. Gasto `b`.
  c. Gasto `c`.

2. Se o Coletivo possuir recursos em caixa cuja soma é maior do que a reserva
   mínima, a diferença entre o total de dinheiro e a reserva mínima é considerado
   como excedente.

3. A arrecadação deve ser constante de modo que o Coletivo tenha sempre em
   caixa uma reserva mínima ou condições de recuperar sua reserva mínima.

## Utilização do dinheiro

* A reserva mínima deve ser utilizada apenas pelo Coletivo.
* O excedente arrecadado pelo Coletivo pode ser disponibilizado para outros
  grupos, coletivos e pessoas.
* A cada ano o valor da reserva mínima deverá ser revisado de acordo com o IGP-M.
