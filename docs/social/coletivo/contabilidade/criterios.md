# Critérios financeiros

{{ info.context.pt }}

{{ warning.needs_work.pt }}

Este processo estabelece os critérios financeiros sobre

1. Doação (de e para o Coletivo).
2. Arrecadação.
3. Gastos.
4. Transparência.
5. Remuneração.
6. Ajuda de custos.
7. Lucro e contas bancárias.

## Doação

Com relação à doações é adotado o seguinte critério (oriundo dos
[http://encontro.fluxo.org/Principal/ConjuntoDePrincipiosEticos Princípios das
mídias e grupos livres]):

    10. Sobre doações: Se recebem dinheiro, o fazem apenas como doação, ou seja:
        qualquer apoiador deve saber que seus recursos não serão empregados senão para
        os fins estritos de criação de espaços comunicativos livres, sendo vedadas as
        práticas de mercantismo cultural, social ou político. Tais doações são aceitas
        apenas se anônimas (isto é, não publicizadas). Dinheiro governamental ou
        empresarial não é aceito.

Ou seja, o Coletivo apenas recebe doações que satisfaçam o critério acima. No
caso de doações que o Coletivo pode efetuar, as mesmas devem ser de acordo com
o critério de gastos.

## Arrecadação

Com relação à arrecadação de recursos, é adotado o seguinte critério (oriundo
dos [http://encontro.fluxo.org/Principal/ConjuntoDePrincipiosEticos Princípios
das mídias e grupos livres]):

    11. Sobre auto-sustentabilidade: As mídias e grupos livres estimulam a geração de
        mecanismos de autosustentabilidade (ou "autodependência") local e comunitária.
        Exemplos: venda de camisetas, comidas, rifas, organização de festas, mostra de
        vídeos, etc. Tratam-se de atividades criadas e organizadas para estimular a
        vivência em coletivo e a escapar das práticas capitalistas. É recomendável que,
        dentro dos grupos e entre eles, exista uma socialização dos recursos e que os
        individuos também adotem essa prática, compartilhando recursos pessoais com o
        coletivo, para criar ambientes de solidariedade comunitária, onde ninguém seja
        excluído por falta de recursos.

## Gastos

Os gastos (reembolso, doações, aquisições, etc) são efetuados através de
procedimento formal.

## Transparência

Com relação à transparência da contabilidade, é adotado o seguinte critério
(oriundo dos [http://encontro.fluxo.org/Principal/ConjuntoDePrincipiosEticos
Princípios das mídias e grupos livres]):

    12. Sobre a gestão financeira: Para garantir essas condições de financiamento,
        toda a gestão financeira das mídias e grupos livres é publica: tanto as
        informações contábeis quanto a participação nas decisões são acessíveis às
        pessoas concernidas nas ações desta organização.

Portanto, o Coletivo adota o critério de fornecer/publicar:

1. Seus critérios financeiros.
2. [wiki:Contabilidade/Planejamento Seu planejamento financeiro].
3. [wiki:Contabilidade/Balanco Seu balanço financeiro].

às pessoas afetadas pela organização do Coletivo (por exemplo, grupos
hospedados e parcerios) levando em conta as restrições de privacidade e
segurança, isto é, a publicação/disponibilização do balanço é restrita aos
grupos e pessoas próximas.

## Remuneração

Sobre à remuneração pelo trabalho realizado dentro do Coletivo, é adotado o
seguinte critério (oriundo dos
[http://encontro.fluxo.org/Principal/ConjuntoDePrincipiosEticos Princípios das
mídias e grupos livres]):

    21. Sobre a remuneração pelo trabalho: As mídias e os grupos livres funcionam
        exclusivamente a partir de trabalho voluntário.

Em outras palavras, o Coletivo não remunera pelo trabalho nele e por ele
realizado. A remuneração, contudo, não pode ser confundida com a ajuda de
custos.

## Ajuda de custos

A ajuda de custos é um recurso utilizado para criar um ambiente de igualdade de
participação no Coletivo, por exemplo nos casos de custeio de ida à reuniões
para pessoas que no momento não estejam em condições de fazê-lo, etc. Assim,
integrantes do Coletivo que não possam participar de alguma atividade do
Coletivo por não disporem de recursos materiais para fazê-lo podem solicitar
ajuda de custos para o Coletivo.

## Lucro e contas bancárias

O Coletivo não possui fins lucrativos. Por isso, o Coletivo não se utiliza de
operações financeiras com o intuito de auferir lucro, mas pode se utilizar de
meios lícitos que lhe garantam a atualização monetária.

Assim, quanto ao uso de transações e contas bancárias, o Coletivo reconhece
que, apesar da segurança do dinheiro poder ser garantida de outras formas, a
utilização de contas bancárias pode ser útil para

* Facilitar a arrecadação financeira por dispor da rede bancária.
* Permitir a atualização financeira.

O Coletivo reconhece as contradições de utilizar o sistema financeiro e por
isso se compromete a utilizar somente o recurso da caderneta de poupança, que
possui as seguintes características:

* Baixo risco.
* Rendimentos baixos, uma vez que ela é basicamente apenas uma atualização
  monetária do dinheiro e por isso é praticamente o fundo de investimentos
  menos prejudicial à sociedade.

## Licença

Como estes critérios se utilizam de trechos oriundos dos
[http://encontro.fluxo.org/Principal/ConjuntoDePrincipiosEticos Princípios das
mídias e grupos livres]), segue a seguinte licença de manipulação dos mesmos
(disponível também [http://encontro.fluxo.org/Principal/Licenca aqui]):

    1.  Licença de Manipulação do Conteúdo Deste Sítio

    Copyright (c) Encontro: Cultura Livre e Capitalismo: desde que não mencionado
    em contrário, este conteúdo é distribuído de acordo com a licença a seguir.

    2.  Liberdades atribuídas ao/à licenciado/a

    Atribui ao detentor/a da informação as seguintes liberdades:

       1. A liberdade de armazenar a informação.
       2. A liberdade de manipular a informação.
       3. A liberdade de distribuir a informação, modificada ou não.

    Desde que as condições listada na seção Obrigações do/a licenciado/a a seguir
    sejam respeitadas.

    3.  Obrigações do/a licenciado

    3.1  Viralidade

        * Desde que esta licença acompanhe a informação.

    3.2  Restrição mercantil

        * Desde que para fins não-lucrativos.

    3.3  Restrição de autoria e fonte

        * Desde que a fonte seja citada.
