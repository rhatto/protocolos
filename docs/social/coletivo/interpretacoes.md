# Interpretações do Protocolo de Ação Coletiva

{{ info.context.pt }}

{{ warning.needs_work.pt }}

!!! info "Versão revisada e comentada"

    Uma versão revisada e comentada deste documento encontra-se
    [aqui](https://saraventos.fluxo.info/interpretacoes.html), como
    parte da coletânea [Saraventos](https://saraventos.fluxo.info).

## Uma interpretação possível

* Versão: 0.2.
* Licença: LIMICS[1].

Este documento contém uma interpretação possível do Protocolo de Ação
Coletiva[2], que aqui é considerado como um processo possível de existência de
um coletivo autônomo tendo como propriedades emergentes a robustez, a
resiliência, a resistência a falhas e a adaptabilidade ao redor da otimização
de atividades. Este texto considera que a adoção de tal protocolo possa servir
a alguns grupos de afinidade desde que ele atenda aos propósitos do grupo (um
processo deve ser útil ao grupo, não o contrário).

## Característica geral

Para grupos pequenos e coesos, a existência apenas de processos formais pode
não representar um grande problema. Mas, se o grupo for grande, e/ou dispersivo
e/ou com o foco de atuação abrangente, etc, as discussões e as tomadas de
decisão podem se tornar muito cansativas e demoradas, além de nem sempre serem
produtivas. Fora isso, o temor de não contemplar a todas as pessoas do coletivo
pode fazer com que sejam aprovadas propostas que na prática não serão
realizadas, mesmo se com a aprovação houver a responsabilidade do coletivo para
cumpri-las.

Muitos grupos possuem apenas processos formais, de modo que qualquer
procedimento a ser realizado nesse grupo requer a pessagem pelo processo de
tomada de decisão. Em muitas ocasiões, os processos formais acabam servindo,
mesmo que as decisões sejam tomadas por consenso, para legitimar uma
concentração de poder e um engessamento da estrutura dos grupos e contribuindo
para criar grupos avessos a qualquer forma de organização definida. Surgem
então os grupos organizados de maneira puramente informal, o que funciona muito
bem para favorecer a espontaneidade e a auto-organização mas que também podem
apresentar sérios problemas pela própria falta de formalidade.

A características geral do Protocolo de Ação Coletiva consiste na articulação
de uma forma possível de organização coletiva que responda tanto à problemática
da tirania das organizações sem estrutura[3] quanto ao engessamento ocasionado
por modos de organização estruturados de forma excessivamente rígida.

O Protocolo de Ação Coletiva também pode ser entendido como uma forma de
organização social do trabalho que possui um grande vínculo com a autonomia do
grupo.

## Configuração Organizacional

Chamaremos de Coletivo aos fluxos informacionais, energéticos e materiais de um
dado grupo de afinidade -- isto é, um grupo de pessoas que nutrem afeto umas
pelas outras em relação a determinadas ou indeterminadas atividades -- que
operam ao redor de uma dada configuração de autonomia definida pelo próprio
grupo de afinidade. Por outro lado, é esta autonomia que define o Coletivo
enquanto um grupo de afinidade por esta delinear a sua atuação:

    Coletivo <-------------> Autonomia
       ^                          ^
        \                        /
         `------> Fluxos <------'

Tudo o que ocorre no Coletivo é um processo. Os processos assumem diversas
manifestações, mas principalmente são fluxos e registros[4]. Todo o processo é
um registro e um fluxo. O registro armazena o estado do fluxo, estando
acessível para qualquer pessoa do Coletivo. Já o fluxo é a produção do processo
e aquilo que efetua a escrita no registro[5].

Em resumo: tudo no Coletivo é processo/fluxo/registro, sendo essa uma
conceituação importante para auxiliar no entendimento do que se realiza, do que
se produz no Coletivo. A mera existência de fluxos não garante ao Coletivo sua
existência, mudança ou permanência, já que fluxos podem ser disjuntivos,
dispersivos, explosivos.

A autonomia do Coletivo é sua capacidade de determinar os fluxos que o afetam.
A autonomia não opera apenas nas relações internas do Coletivo pelo fato deste
não constituir um sistema fechado/isolado: o Coletivo pode ser entendido como
um sistema aberto dentro de um sistema aberto mais abrangente que é o próprio
mundo. Manter sua autonomia, isto é, sua autodeterminação de mudança e
permanência (do mundo e de Si), é estipular uma relação de abertura e
fechamento em relação ao mundo.

De modo análogo, as pessoas que fazem parte do Coletivo também tem sua própria
autonomia de ação. Enquanto integrantes do Coletivo, contudo, as pessoas
concordam que sua autonomia pessoal não pode ser posta em detrimento da
autonomia do Coletivo. Enquanto pessoas atuando fora do coletivo, exercem sua
autonomia individual. Quando atuando como partes do Coletivo, sua autonomia
enquanto indivíduos é limitada pela autonomia do Coletivo.

Um/a integrante do Coletivo atua dentro dele quando utiliza os recursos e o
nome do Coletivo. Por outro lado, um/a integrante do Coletivo atua fora dele
quando não utiliza os recursos ou o nome do Coletivo. Intregrantes do Coletivo
não realizam ações (dentro ou fora do Coletivo) que, conscientemente, possam
prejudicar a autonomia do Coletivo.

A autonomia, por ser uma propriedade de autodeterminação, pressupõe, no caso de
uma associação de pessoas, um processo de tomada de decisões que respeite
igualmente a autonomia das pessoas enquanto integrantes do Coletivo. Em outras
palavras, é necessário que exista um processo formal para a operação da
autonomia do Coletivo.

Além disso, mesmo atuando dentro do Coletivo, a autonomia pessoal não é
suprimida, apenas diminuída: há uma grande margem entre a autonomia da pessoa e
a do Coletivo, o que permite que exista, dentro do Coletivo, fluxos que não
interfiram na autonomia do Coletivo. Em outras palavras, é possível que exista,
dentro do Coletivo, processos sem forma necessariamente definida e que não
operem a autonomia do Coletivo, isto é, processos informais.

## Os Processos Formais

Como dito anteriormente, o Coletivo possui dois tipos essenciais de
processos/fluxos no que concerne ao uso da autonomia: os formais e os
informais. Se nos informais não há interferência na autonomia do Coletivo, para
os formais é preciso não apenas de um processo de tomada de decisão como uma
garantia que as decisões tomadas sejam realizadas, do contrário a autonomia não
pode ser exercida.

No Coletivo, tal garantia é obtida pela responsabilização, que é o ato de
chamar para Si a incumbência por um dado procedimento assim como responder pela
sua realização total, parcial ou mesmo pela sua não-realização. É através da
responsabilização que o Coletivo tem garantias mínimas de que o processo será
realizado.

Em resumo, os processos formais tem as seguintes características:

* Forma necessariamente definida de antemão pelo coletivo E
* Lidam com a autonomia do coletivo. PORTANTO
* Precisam ser acompanhados pela responsabilização mínima para o processo não
  falhar por falta de iniciativa

Uma forma de saber saber se um processo deve ser formal ou informal é perguntar
se o mesmo afeta (mexe com, precida da) ou não a automonia do Coletivo.

## Fluxograma de tomada de decisões e responsabilização formal

Existem inúmeras maneiras de se estabelecer um procedimento formal. No caso do
Protocolo de Ação Coletiva, os processos formais do Coletivo são realizados de
acordo com o seguinte diagrama:

        .------------------->-----------------.
       /  .----------<--------------<-------.  \
      |  '                                   \  \
      |  |               .------>-----.       \  \
      |  |              |              \       \  \
     Proposta -----> Discussão ->--.    \       \  \
        |  ^            |           \    \       \  \
        |  |            |            \    \       \  \
        |   `----<-----'             |     \       \  \
        |                            |      |       \  \
         `------>------ Decisão --<--'      |        \  \
                          | |               |         \  \
                          | |               |          | |
      Atribuição de --<---' '---> Arquivamento --->---'  ;
    Responsabilidades ----->-------'   ^    \           /
         ^  |              ___________/      `---<-----'
         |   \           .'
         |    `--> Realização -->--.
         |           |  |           \
         |           |  |           /
          `----<-----'   `-----<---'

Todas as etapas de um processo formal apresentam ao menos uma via de entrada e
outra de saída. Não há começo e nem fim, apenas um processo fluído de
realização autônoma. Cada etapa é discutida nas sessões a seguir.

## Etapa de Proposta

Esta etapa não é a primeira nem a última, mas foi escolhida como tal nesta
explicação apenas para facilitar o entendimento dos processos formais. É na
etapa de proposição que a idéia de um procedimento formal é lançado ao
Coletivo. A idéia -- ou descrição -- do processo pode vir do Arquivo de
propostas, de uma Discussão anterior, de um procedimento informal que se julga
importante formalizar ou mesmo de uma pessoa ou grupo de pessoas de dentro ou
de fora do Coletivo.

Recomenda-se que a proposta de processo formal seja bem explicada, contenha uma
sugestão de prazo de decisão e que sugira o ciclo de vida do processo, isto é,
como, quando e por quanto tempo ele deve ser realizado (se aplicável), assim
como os critérios e prazo para atribuição de responsabilidade, o seu término
(se aplicável) e recomendações para situações emergenciais (se aplicável).

Uma vez lançadas, propostas podem ser discutidas, aprovadas ou arquivadas, como
veremos a seguir.

## Etapa de Discussão

Após a introdução de uma proposta, a discussão não é estritamente necessária,
isto é, a Proposta pode seguir diretamente para a Decisão. No entanto, a
discussão não deixa de ter importância: ela não só ajuda a arquivar as
propostas que não foram adotadas num dado momento como efetuam as alterações
desejadas nas propostas antes da tomada de decisão. A discussão é o momento
para o refinamento das propostas que as pessoas do Coletivo acharem
interessantes.

Alterações em propostas fazem com que o procedimento formal em questão volte
para a etapa de Proposta. Propostas que não seguirem para a etapa de Decisão ou
que não forem alteradas até o prazo proposto devem ser arquivadas.

Propostas que vem de fora do Coletivo e que forem discutidas e alteradas devem
ser enviadas também para o grupo ou à pessoa de fora do Coletivo responsável
pela sua introdução, apesar destas pessoas não participarem da discussão
interna do Coletivo. Se tal pessoa ou grupo concordar com a proposta alterada,
então o processo formal em questão retorna à etapa de Discussão com a nova
proposta. Caso contrário, isto é, a pessoa ou grupo de fora do Coletivo não
concordar com a proposta alterada, então o processo formal em questão é
arquivado e a pessoa ou grupo devem ser informados do arquivamento (exceto se
as partes externas apresentarem uma nova alteração à proposta ou mais
argumentos à discussão).

## Etapa de Decisão

Como dito anteriormente, a autonomia, por ser uma propriedade de
autodeterminação, pressupõe, no caso de uma associação de pessoas, um processo
de tomada de decisões que respeite igualmente a autonomia das pessoas enquanto
integrantes do Coletivo. No Protocolo de Ação Coletiva, a etapa de Decisão é o
momento no qual o Coletivo decide se determinada proposta é pertinente ou não,
isto é, se o Coletivo julga, apóia e concorda com o seu teor ou o contrário.

É importante ressaltar que há uma distinção entre julgar uma proposta
pertinente ao Coletivo, se responsabilizar por ela e finalmente realizá-la. Na
etapa de Decisão, apenas o teor da proposta é avaliado. Se o Coletivo aprova
uma proposta, isso significa que a sua eventual execução será feita em nome do
Coletivo, mesmo que seu nome não seja levado a público (isto é, para fora do
Coletivo).

O Protocolo de Ação Coletiva assume o consenso como a forma de tomada de
decisões. A participação em consenso depende do acesso às informações do
coletivo; para participar ativamente de uma decisão, é preciso, portanto, estar
acompanhando as informações referentes à proposta em questão. Se não há
consenso sobre a aprovação de uma proposta, a mesma permanece bloqueada,
podendo ter seu prazo estendido.

Se manter em silêncio é considerado como concordância com a proposta em
questão. O consenso velado, isto é, quando ninguém ou apenas poucas pessoas se
posicionam em relação a uma proposta de processo formal, não é um problema
desde que haja pessoas que se responsabilizem pelo andamento do processo
aprovado; sem pessoas se responsabilizando, o processo é
arquivado/interrompido/congelado na próxima etapa, de tal modo que o silêncio
das pessoas não interfere no processo de tomada de decisão.

Quanto aos prazos, recomenda-se que os mesmos sejam estipulados relativamente
ao tempo que as pessoas ativas no coletivo levam para tomar conhecimento,
discutir, propor alterações, pedirem eventuais adiamentos, etc, sendo passíveis
de prorrogação ou antecipação através de um pedido explícito por alguma pessoa
do Coletivo. No entanto, se não há pedido para alteração de prazo, a data
inicial da proposta deve ser respeitada. Se não sofrerem objeção, pedidos de
adiantamento ou adiamento de prazos são automaticamente aceitos. Na medida do
possíve, prazos razoáveis são recomendados em detrimento de prazos
emergenciais. É importante manter uma boa temporalidade para a etapa de decisão
por possibilitar a participação de quem quiser e permitir a apreciação
cuidadosa das propostas.

Aprovações de propostas que vem de fora do Coletivo ou que tenham como
participantes grupos ou pessoas de fora do coletivo são comunicadas às
pessoas/grupos de fora do Coletivo apenas após a atribuição de
responsabilidades, uma vez que o Coletivo terá responsabilidade sobre a
realização de uma proposta a partir do momento que informar o ator/a externo da
decisão da proposta.

## Etapa de Atribuição de Responsabilidades

Se a aprovação uma proposta requer a aceitação de todo o Coletivo, a atribuição
de responsabilidades, por outro lado, tem um caráter distinto: não é preciso
que todo o Coletivo assuma responsabilidades pela execução de uma proposta, mas
apenas um determinado número de pessoas do Coletivo é necessário à execução da
proposta em questão. A etapa de atribuição de responsabilidades é o momento em
que um grupo de trabalho será formado com a responsabilidade pela realização da
proposta aprovada.

A atribuição de responsabilidade é uma etapa sensível porque constitui o
momento onde a garantia de realização do procedimento será dada. Não adianta
apenas tratar da responsabilização sem tratar da irresponsabilização: essa
etapa cuida não só do comprometimento de pessoas do Coletivo com relação a um
dado processo formal mas como também do caso emergencial quando pessoas do
Coletivo tiverem comportamento irresponsável perante um processo formal. Em
outras palavras, a etapa de Atribuição de Responsabilidades também deve ser a
etapa de planejamento de falhas.

A primeira medida para evitar a não-realização do processo formal é a
minimização dos possíveis pontos de falha, principalmente dos pontos de falha
singular. Um ponto de falha singular é todo aquele que por si só pode acarretar
na não realização do processo formal. Um ponto de falha é um ponto de falha
singular se for o único apoio existente para a realização do processo formal:
se for removido, o processo não se realiza. Já pontos de falha não-singulares
são os apoios que, mesmo se removidos, não comprometem a realização do
processo.

É evidente que se deseja evitar os pontos de falha singulares. Assim, para que
seja possivel sair da etapa de responsabilizacao para a de realizacão, é
importante que se estipule o número mínimo de pessoas necessário para que o
processo seja considerado realizável. Isso depende da tarefa em questão e sua
especificação é importante para evitar pontos de falha. Assim, por
responsabilização suficiente se entende o número mínimo de pessoas se
responsabilizando pelo processo formal para o mesmo ser considerado realizável.

Outra forma de evitar a falha se encontra no detalhamento a respeito da
responsabilização: a responsabilização não é apenas o comprometimento pelo
andamento de um dado processo, mas também a responsabilidade de informar com
antecedência quando não puder mais realizá-lo, assim como auxiliar no processo
de transição da responsabilidade para outras pessoas, se for o caso.

O não-cumprimento de uma responsabilidade compromete a atribuição de outras
responsabilidades. Além disso, a atribuição de uma responsabilidade é
voluntária e deve ser registrada para fins de documentação e para evitar
mal-entendidos e problemas de comunicação.

A pessoa que se responsabilizar por algum processo formal deve declarar no
registro do processo formal que:

* Tem conhecimento sobre o procedimento em questão.
* Irá realizá-lo dentro do prazo estipulado e que manterá o Coletivo informado
  sobre a sua realização.
* Caso não possa mais arcar com a responsabilidade, avisará o Coletivo com
  antecedência suficiente para que o mesmo possa, dependendo do caso, manter a
  realização do processo, atribuir novas responsabilidades a ele ou então
  simplesmente encerrá-lo e arquivá-lo.

Processos formais que forem aprovados mas que, findo o prazo para a
responsabilização, não tiverem responsabilização suficiente atribuída, devem
seguir para o arquivamento, sendo que o desarquivamento de propostas
anteriormente aprovadas não pode seguir diretamente para a atribuição de
responsabilidade, mas sim seguir para a etapa de proposição.

Chamaremos aqui de de grupo de trabalho o conjunto/grupo de pessoas envolvidas
num mesmo processo (formal ou informal). Lembramos, também, que não é um
requisito para se estar num grupo de trabalho formal a responsabilização pelo
processo formal em questão: pode-se estar num grupo de trabalho (formal ou
informal) de modo informal, isto é, sem ter responsabilidade sobre isso
(bastando para isso participar das discussões ou acompanhar as respectivas
informações).

A permanência de pessoas no coletivo que não se voluntariam para se
responsabilizar por algo não é problema, muito pelo contrário: permite que
pessoas afins permaneçam no coletivo mesmo se não puderem ajudar em processos
formais, já que há uma diferença entre não-responsabilidade e
irresponsabilidade. Assim, as pessoas podem permanecer no Coletivo e serem
ativas de várias maneiras sem precisarem necessariamente se responsabilizar por
algo.

No caso de propostas que vem de fora do Coletivo ou que tenham como
participantes grupos ou pessoas de fora do coletivo são comunicadas às
pessoas/grupos de fora do Coletivo sobre seu estado de !Aprovação/Realização
apenas após a atribuição de responsabilidade, isto é, ao final desta etapa.

## Etapa de Realização

Apenas processos formais cuja responsabilização foi atribuída podem partir para
a etapa de realização. Processos que forem realizados e que não tiverem
prosseguimento definido são arquivados.

Processos formais que, não tendo sido realizados no prazo comprometido pelo
grupo das pessoas que se responsabilizado por ele, devem retornar à etapa de
Atribuição de Responsabilidades. De modo análogo, processos em realização mas
cujos/as responsáveis não puderem mais realizá-los devem retornar à etapa de
Atribuição de Responsabilidades caso o número de pessoas responsáveis
remanescentes não for suficiente para a sua realização.

## Etapa de Arquivamento

O arquivo, ou banco de propostas, é o local onde ficam armazenadas todas as
propostas que:

* Não foram aprovadas OU
* Foram aprovadas mas não foram adotadas responsavelmente OU
* Foram realizadas e encerradas OU
* Estavam em realização mas não tem mais o número de pessoas responsáveis
  suficiente, por exemplo: quando ninguém ou apenas um número insuficiente de
  pessoas estiverem cuidando de um dado recurso.

Um processo formal é dito em arquivamento quando uma das condições acima for
verdadeira para ele. No caso de um processo que estava sendo realizado e
precisar ser arquivado por falta de pessoas responsáveis por ele, as últimas
pessoas responsáveis por ele devem realizar o procedimento de encerramento e
arquivamento.

No caso de propostas que vem de fora do Coletivo ou que tenham como
participantes grupos ou pessoas de fora do coletivo são comunicadas às
pessoas/grupos de fora do Coletivo sobre seu estado Recusa/Arquivamento apenas
nesta etapa.

## Observações sobre Processos Formais

Em resumo, o processo formal necessita que haja proposta, decisão e
responsabilização antes da realização de uma atividade.

É importante ressaltar que o objetivo destes princípios é favorecer o trabalho
coletivo e otimizar a busca dos compromissos que o grupo de afinidade pode
adquirir coletivamente. Pelo fluxograma de procedimentos formais, vemos que
qualquer processo formal que não estiver na responsabilidade de alguém (ou de
um grupo de trabalho) será automaticamente arquivado/congelado.

Esta é provavelmente a propriedade emergente mais importante desse sistema: a
etapa de responsabilização atua como um filtro de propostas aprovadas de tal
forma que apenas o que passar por ela pode ser realizado. A etapa de
responsabilização introduz uma limitação necessária no processo, o que se
compatibiliza com a limitação real de atuação do grupo. O processo como um todo
atua no sentido de encontrar com eficiência as atividades que o Coletivo pode e
quer realizar.

Um caso degenerado seria um grupo que adote este processo mas que, composto por
pessoas verborrágicas porém preguiçosas, decidisse fazer tudo mas não se
responsabilizar por nada, de modo que o processo formal opera como um detector
de índole e proporciona ao grupo encontrar o que realmente quer fazer: apenas
discutir.

Indo pela situação oposta, outro grupo que adote este processo mas que, por não
discutir nada também não decide nada e, consequentemente, não precisa se
responsabilizar por nada e também não realiza nada. Neste caso o processo
também auxiliou o grupo a realizar as atividades a que estiver inclinado.

Mesmo em grupos que estão entre esses pólos de atuação, o processo formal
favorece a triagem das propostas que podem serem postas em prática. Muitas
vezes as propostas partem de pessoas (ou grupos dentro do Coletivo) que já
estão predispostas a realizá-las. Noutras, porém, a proposta surge de pessoas
que não estão inclinadas a realizá-la mas julgam a realização, por parte do
Coletivo, muito pertinente. Em ambos os casos, se a tomada de decisão avalia se
a proposta é pertinente e aparentemente viável, a etapa de responsabilização
dirá se o coletivo possui meios de arcar com sua realização.

Sob o aspecto do registro, cada processo formal pode ser entendido como uma
instância de uma máquina de estado finito. Seu fluxograma apresenta
características interessantes: o barramento de arquivamento é full-duplex e que
existe uma configuração rotacional ao redor da discussão e da realização. Já o
arquivamento possui basicamente convergências e divergências.

O processo formal é uma maquinação, um processo de desenvolvimento de um
software social. Ele não impõe regras sobre o teor das propostas (que inclusive
podem propor a mudança ou abolição do fluxograma), mas apenas regras mínimas
sobre o próprio andamento do coletivo. No entanto, os próprios princípios de
organização coletiva compõem um processo formal cuja responsabilidade é de todo
o coletivo. Consequentemente, ele mesmo passa pelos procedimentos de
aprovação/execução/responsabilização previstos nele mesmo (bootstrapping e
recursividade).

## Os Processos Informais

Os processos informais do Coletivo são os fluxos que não interferem na sua
autonomia. Eles apresentam propriedades notáveis para a emergência de padrões,
para a experimentação, para a auto-organização e para o combate à apatia e ao
gerenciamento centralizado.

Nos coletivos onde se reconhece apenas a existência de processos formais, está
subentendido que toda a ação coletiva deve passar pela instância de tomada de
decisões. Nesse tipo de coletivo, nada se realiza explicitamente sem uma
decisão central, mantendo-se assim uma cultura de gerenciamento onde a
iniciativa pessoal ou de um pequeno grupo apenas tem espaço no processo de
tomada de decisão oficial. Mesmo através do conhecimento empírico é possível
mostrar que tal modelo muitas vezes afasta a participação e a iniciativa
pessoal, além de gerar ruído desnecessário na instância formal de decisão por
ter de decidir a respeito de todos os assuntos e ações pertinentes ao grupo.

Por exemplo: muitas vezes constitui um esforço desnecessário para submeter à
decisão do Coletivo qual é a melhor data para se realizar uma reunião onde nada
será decidido pelo Coletivo. Pode ser até mais complicado logisticamente de se
discutir esse tipo de coisa na instância formal de decisão quando um processo
informal pode endereçar isso facilmente.

Por isso que, em fluxos que sejam possíveis de serem realizados sem interferir
na autonomia do Coletivo que o Protocolo de Ação Coletiva incentiva que estes
sejam processos informais. Como processos formais, pela própria definição, não
tem forma definida de antemão pelo processo de decisão do Coletivo, eles não
precisam ser propostos e nem decididos, mas podem ser simplesmente realizados
desde se constituam como processos, isto é, sejam fluxos e registros (conforme
nossa definição inicial de um processo). Sendo também registros, os processos
informais em planejamento ou realização tem sua informação disponibilizada para
todas as pessoas do Coletivo, sendo então sua existência um convite à
participação. Por exemplo, procedimentos informais podem começar com um
informe, convite ou como uma proposta (mas não confundir com uma proposta
formal).

Em resumo, os processos informais tem as seguintes características:

* Forma não necessariamente definida de antemão E
* Não afetam a autonomia do coletivo. PORTANTO
* Não precisam necessariamente estar atrelados à responsabilidade de alguém
  (isto é, a não-realização de um processo informal não afeta a autonomia do
  Coletivo).

Trocando em miúdos, os processos informais não necessariamente tem um protocolo
previamente definido, ou seja, negociado de antemão: sua negociação pode ser
dar também durante sua realização. Por outro lado, devido à não necessidade de
responsabilização, os processos informais não tem garantias formais de
execução.

Cabe ressaltar que processos informais, mesmo não tendo forma previamente
definida, ainda são processos. Atividades sem informação disponibilizada no
Coletivo não pode ser considerada como processos (formais ou informais) do
Coletivo porque não dispõem de igualdade de acesso à informação, requisito para
a possibilidade de participação (isonomia informacional). A falta de forma
previamente definida não deve ser confundida com falta de forma (aformalidade):
nos processos informais, a forma está contida no próprio processo informal (in
forma) e não em princípios de organização externos (como no caso dos processos
formais, onde sua forma geral é definida de antemão e externamente ao
processo).

Certamente há uma vasta gama (talvez a maior parte) das atividades realizadas
que fogem dessa conceituação de processo, isto é, podem até serem processos
físicos, mentais, etc, mas pela falta delas figurarem no fluxo de informação do
Coletivo elas não podem ser consideradas como processos protocolares no sentido
dado pelo Protocolo de Ação Coletiva, mas apenas processos aformais. Para
ganhar em termos de organização, o Protocolo de Organização Coletiva restringe
a denomiação de processo apenas às atividades que integram o fluxo
informacional do Coletivo (processos do Coletivo são aqueles que foram
coletivizados).

## Formalização e informalização

Processos formais e informais são diferenciações que nos ajudam a agir conforme
a necessidade. Se é preciso voar, experimentar, atuar favorecendo-se com as
relações sociais e culturais que são naturais sem interferir na autonomia do
Coletivo, os processos informais permitem que se proceda sem regras dadas de
antemão. Caso precisemos agir com coesão, com firmeza e favorecendo uma
coletividade forte (isto é, usando a autonomia do Coletivo), o Protocolo de
Ação Coletiva encoraja o uso de processos formais.

Os processos formais possuem um andamento mais rígido quando é preciso um
acordo comum para pessoas agirem conjuntamente sem que precisem abrir mão das
suas diversidades, das suas diferenças, das suas particularidades, das suas
culturas. Da mesma forma, as pessoas do Coletivo podem precisar dos processos
informais se acreditarem ser importante uma ação coletiva que utilize
exatamente dessas diversidades e diferenças.

Modos formais e informais permanecem distintos em princípio mas, com o tempo,
também é possível formalizar um processo informal (via consenso e atribuindo
responsabilidade ao mesmo) ou, por outro lado, transformar um processo formal
em informal (via consenso e retirando responsabilidade, mas retirando
igualmente a dependência do coletivo a esse procedimento, isto é, a autonomia
do coletivo deve se tornar independente do processo que estiver se
informalizando).

Se o processo formal de tomada de decisões pode ser entendido como um protocolo
que mantém a coesão do coletivo ao redor de sua autonomia (espaço comum,
central), os procedimentos informais podem ser entendidos como protocolos
distribuídos, maleáveis, que se reordenam durante sua existência com uma
flexibilidade maior do que os processos formais. Alguns processos informais
podem até ter o caráter experimentalista de descobrir próximas ações que, de
tão importantes que podem se tornar, se transformem em processos formais.

## Fluxo informacional

Estes princípios apenas funcionam se há acesso disponível às informações por
todas as pessoas que fazem parte do Coletivo (acesso não significa obrigação
por acompanhar todas as informações que trafegam pelo Coletivo, mas sim
possibilidade de acompanhamento arbitrário das mesmas). Sendo assim, é
importante que existam instâncias de tráfego informacional (instâncias
processuais) que facilitem o acompanhamento dos processos coletivos (tanto
formais quanto informais) e que satisfaçam requisitos de privacidade e
segurança que garantam a autonomia do Coletivo.

Convém distinguir três possíveis instâncias informacionais:

* Instâncias informais, que são aquelas utilizadas para o fluxo informacional
  de processos informais.
* Instâncias formalizadoras, que são as instâncias utilizadas para formalizar
  procedimentos.
* Instâncias maleáveis, que podem ser utilizadas para agregar informações de
  processos formais e informais.

Notar que essas distinções não são estritamente excludentes: uma instância pode
ser utilizada simultaneamente para comunicação informal, para formalizações e
para agregar informações diversas. Por exemplo, um modo mais simples de lidar
com as instâncias seja considerar todas as instâncias de comunicação como
informais ''a priori'' (isto é, se nada mais for dito a respeito da forma de
cada uma delas) e estabelecer alguns critérios pelos quais determinadas
instâncias assumem o papel de formalizadoras e/ou maleáveis.

Alem disso, requisições e comunicações ao coletivo (internas ou externas) devem
ser feitas ao coletivo e não individualmente (isto é, fora da base pessoal),
tanto para não sobrecarregar pessoas quanto para:

* Manter a informação disponível ao coletivo.
* Incentivar uma interação coletiva.

## Autonomia do Coletivo

O Protocolo de Ação Coletiva define uma autonomia básica para o Coletivo. A
autonomia básica do Coletivo, isto é, a autonomia mínima que garante a sua
existência de acordo com estes princípios, é a posse de canais de comunicação
privados e seguros que permitam a existência dos registros de processos
coletivos (formais ou informais). Sem esses canais, a autonomia básica do
Coletivo é seriamente abalada, assim como a aplicação destes princípios. Toda
autonomia adicional do Coletivo (isto é, que não for a autonomia básica) deve
ser definida através de processos formais.

Todo o processo formal em realização requer uma autonomia (um poder) em geral
além da autonomia básica (por exemplo, um recurso necessário para a realização
do processo). Por isso, ao realizar um processo formal, o grupo de trabalho em
questão é responsável por manter a autonomia requerida. Portanto, os aumentos e
diminuições da autonomia do Coletivo apenas ocorrem através de processos
formais.

O Protocolo de Ação Coletiva reconhece, também, que mesmo um coletivo autônomo
também possui relações de dependência e trocas com o ambiente externo e que
esse é um fator importante de interferência na autonomia do Coletivo. O
Coletivo não produz tudo o que necessita para Si: certos fluxos são
externalizados (outsourcing, terceirização) de e para o ambiente.

Tal visão de autonomia, inclusive, é compatível e satisfaz os seguintes
Princípios das mídias e grupos livres do Encontro: Cultura Livre e
Capitalismo[6], uma vez que a autonomia do Coletivo emana de si enquanto
organização dinâmica:

> 0. Sobre a mobilidade dos princípios: Todos os princípios podem ser a
>    qualquer momento modificados ou abandonados desde que não sejam mais a
>    expressão imanente das relações que se constituem através das ações
>    coletivas.
>
> 1. Sobre a autonomia: grupos e mídias livres renunciam e se recusam a
>    recorrer a qualquer entidade política que não a si próprias para
>    constituir sua legalidade e sua normatividade, por acreditar que a sua
>    única fonte legítima é sua emergencia a partir dos laços de confiança e
>    solidarieade entre participantes e de cada participante com os coletivos
>    por eles constituídos.
>
> 6. Sobre a gestão: As mídias e os grupos livres usam e desenvolvem
>    sistematicamente mecanismos de gestão anti-hierárquicos e baseados na
>    geração de consensos a partir da argumentação pública; ou seja, rejeitam
>    (ou evitam ao máximo), como práticas de organização: a representação
>    política e a votação plebiscitária. A divisão funcional é adotada com
>    ponderação, sob avaliação coletiva e de maneira ocasional.

## Atividade Coletiva Complexa

O Protocolo de Ação Coletiva serve para facilitar as atividades do Coletivo,
combatem a apatia e o espetáculo (no sentido das pessoas se portarem como
espectadoras, pessoas gerenciadas que permanecem em estado de espera, letárgico
e apático), o ruído existente nas instâncias de tomada de decisão, a
dificuldade de acompanhamento dos processos no coletivo e a preenchem a
necessidade por processos realmente coletivos em andamento (e não apenas as
iniciativas pessoais).

Os processos formais encorajam um trabalho coletivo firme enquanto que os
informais encorajam comportamentos pessoais protagonistas que realizem
discussões e também encontros descompromissados. O Processo de Ação Coletiva
também ressalta a importância de se utilizar os recursos de comunicação
coletiva de modo a minimizar o ruído pois isso facilita o acompanhamento dos
processos.

Quanto ao seu funcionamento, uma analogia interessante pode ser feita com um
sistema operacional multi-usuário/a, uma vez que o Protocolo de Ação Coletiva
utiliza o conceito de processo e permite que muitos processos existam
paralelamente, os quais podem serem até organizados em árvores, de acordo com
suas semelhanças e/ou dependências.

## Memética da auto-organização

O Protocolo de Ação Coletiva é um fruto da cultura e do choque cultural do
grupo de afinidade no qual ele se originou. Quando esses princípios de
organização adentram o plano da cultura das pessoas do Coletivo, isto é, são
praticados com naturalidade e desenvoltura, então temos uma mudança profunda no
modo de agir coletivamente. Tal dinâmica pode se suceder indefinidamente
conforme os princípios antigos se tornam obsoletos.

Como exemplo, deixamos um modelo comportamental possível dentro dos Protocolo
de Ação Coletiva. Nesse esquema mental, qualquer pessoa do Coletivo pode
participar nas suas três instâncias informacionais:

* Nas reuniões informais, participando de discussões, bate-papo
  descompromissado, elaboração de propostas de decisão e ação.
* Na instância maleável, com a elaboração de relatos, propostas e documentação diversa.
* Na instância formalizadora, participando das tomadas de decisão.

Ou seja:

* Instância informal: reuniões presenciais ou remotas de caráter mais
  descomprometido e facilitador da troca de idéia.
* Instância formalizadora: utilização prioritariamente para os ritos de
  formalização de processos ou em casos emergenciais.
* Meio de campo: instância maleável, diminuidora de ruído, podendo ser usada ao
  máximo para economizar a largura de banda da instância formalizadora.

Esse esquema mental, aliado à distinção entre processos formais e informais,
sugere um modelo pessoal de entendimento e participação no processo coletivo,
onde a pessoa pode determinar a melhor maneira de se comunicar e submeter
propostas, idéias e relatos sem que suas mensagens façam parte de um ruído
(isto é, excesso de mensagens enviadas aos canais de comunicação) ou caiam num
processo de formalização muito burocrático sem necessidade.

Com relação às reuniões informais, não há problema de autonomia se as pessoas
combinarem previamente, avisarem ao Coletivo e depois acrescentarem relatos nos
canais de comunicação coletivos, já que numa reunião informal nada pode ser
decidido pelo Coletivo. Inclusive, as reuniões informais, se feitas dessa
forma, evitam o problema de gastarmos semanas tentando encaixar na agenda de
todo mundo uma reunião onde no fim das contas aparecem poucas pessoas. Desse
modo, quando alguém quiser ou sentir que uma reunião é necessária, basta
combinar com outras pessoas interessadas, comunicar ao Coletivo lista e pronto
:)

Tal modelo de comportamento, desde que respeite a presente carta de princípios,
nem precisa ser aprovado pelo coletivo, pois é um modelo de entendimento e
relacionamento pessoal de como as coisas podem fluir e como processos
interessantes podem emergir, lembrando que emergência pode ser entendida como
pequenas regras (ou modelos, esquemas) de comportamento que cada pessoa mantém
e aplica.

Por fim, a questão da apatia versus o protagonismo. Tal modelo de
relacionamento proposto só funciona de modo saudável se todas as pessoas forem
protagonistas, deixando sua apatia e sua preguiça de lado. Caso contrário, ela
levará a um gerenciamento centralizado nas poucas pessoas que forem ativas.
Sentiu que uma troca de idéias deve ser feita? Vá, faça, se possível informe a
lista com antecedência e depois adicione o conteúdo nos canais de comunicação
coletivos.

Quem não tem iniciativa está destinado/a a ser gerenciado/a e governado/a.

## Limitações

Mesmo que estes princípios sejam úteis e desejáveis para o Coletivo, convém
reconhecermos suas limitações. Tais princípios assumem que o Coletivo é um
grupo de afinidade e por isso ele pode enfrentar problemas e necessitar
modificações se for adotado por grupos onde não haja afinidade, principalmente
porque sua capacidade de resolução de disputas e conflitos é limitada. O
circuito de um processo formal também pode ser usado para criar loops e os
princípios não prevêem em si regras para lidar com o ruído. Talvez eles também
precisem de adaptações para funcionarem como desejado em grupos muito grandes,
principalmente porque processos que demandem um fluxo de informação muito
intenso (por exemplo, propostas grandes) tendem a demandar mais tempo para
discussão, decisão, etc.

O Protocolo de Ação Coletiva também não dá (e talvez nem devesse mesmo)
fundamento para lidar com desconexões (saída de pessoas do grupo de afinidade)
ou rachaduras no grupo. O grande obstáculo para lidar com rachaduras de forma
transparente é a questão do nome e da aparência pública do Coletivo, o que
talvez só possa ser resolvido num coletivo com espaço de nomes múltiplo. Como,
no entanto, o Protocolo de Ação Coletiva não foi criado para lidar exatamente
com desconexões e rachaduras, esses fluxos disjuntivos foram deixados de lado,
ao menos temporariamente.

Algo mais deve ser lembrado: estes princípios não definem os objetivos do
Coletivo e nem garantem que atividades sejam realizadas. Esta declaração de
princípios apenas diz como a energia pode ser gasta no Coletivo, isto é, dado
um potencial, um desejo de agir, como a energia pode fluir dentro do Coletivo.
O propósito, a vontade e o potencial de agir são sempre externos aos princípios
de organização coletiva e ao Protocolo de Ação Coletiva.

## Em direção a muitos protocolos de rede

A organização parece uma necessidade, algo crucial[7], ao passo que parece
inesgotável a quantidade de formas de organização possíveis. Em outras
palavras, se a organização é importante, os modos de se obtê-la podem não ser
tão óbvios.

A questão mais geral de como um grupo de pessoas pode se organizar melhor para
atender seus objetivos e lidar com os problemas que surgem pela própria
organização pode nos levar inclusive a uma análise um pouco mais profunda da
natureza não apenas do Protocolo de Ação Coletiva apresentado como de todo um
conjunto de protocolos que satisfaçam uma dada coletividade.

Historicamente, dentre as formas de organização igualitárias e de democracia
direta, destacam-se as federações, inventadas pelos movimentos sociais nos
últimos séculos e redes abertas, formas recentes criadas nos últimos anos com o
adventos das modernas técnicas de comunicação e com novos desafios para
organização encontrados pelos movimentos.

Muitas vezes federações e redes abertas são colocadas em polos opostos dadas
suas diferenças muitas vezes irreconciliáveis. Se é difícil dar um nome à
relação entre essas duas formas de organização, por outro lado elas não parecem
ser exatamente opostas, mas dialógicas. Algo que talvez possa generalizar tal
relação seja a noção de protocolo. Protocolos não apenas lidam com o fluxo de
informação, matéria e energia entre nós/grupos de uma rede, mas também podem
lidar com o processo de conexão e desconexão.

O modelo federativo auxilia muito no processo de decisão e responsabilização,
enquanto que o modelo das redes abertas impulsiona a emergência de padrões
complexos. Enquanto é mais difícil observar emergências em federações do que em
redes abertas, o oposto ocorre quando se tenta tomar uma decisão. Uma rede
aberta dificilmente realiza uma decisão que não faça parte do seu protocolo.

Portanto, federações tendem a ser uma melhor opção nos momentos em que
seus/suas participantes precisam decidir sobre o uso e o acesso a um bem ou
recurso rival, isto é, quando há uma necessidade de dirigir um bem
rival/excludente a um dado uso. Por outro lado, redes abertas existem
usualmente em locais onde pessoas e grupos lidam basicamente com bens
não-rivais (principalmente informação).

Consideremos, ao invés dos conceitos de federação e redes abertas, conceitos
como formalidade, informalidade e protocol. Um procotolo seria um conjunto de
regras (definidas ou indefinidas) usadas para dar topologia (forma) a uma rede,
lidando com suas conexões, desconexões, fluxo informacional, tomada de decisão,
etc.

A formalidade e a informalidade concernem ao conjunto de regras predefinidas
num dado protocolo: no caso de uma rede aberta, regras usualmente não existem a
priori e emergem as poucos num modo informal conforme o protocolo é atualizado
dinamicamente, enquanto que federações tipicamente começam com um conjunto de
regras acordadas de antemão.

Como regras acordadas antes da instanciação de uma regra representam um
entendimento comum das possibilidades e autonomia de casa uma das partes
envolvidas no acordo, elas são mais propícias para lidarem com ben/recursos
rivais/exclusionários. Mas, por precederam a atual experiência da rede, elas
podem carecer de características necessárias para lidam com padrões emergentes
de organização, especialmente à manipulação de bens não-rivais, algo muito bem
obtido com processos informais.

Muitos grupos são complexos o suficiente para lidaram tanto com bens e recursos
rivais quanto não-rivais mas também com questões importantes como segurança,
privacidade e confiança. Por isso, provavelmente muitos grupos precisarão de
protocolos híbridos que lidem ao mesmo tempo com a formalidade e a
informalidade.

Ter um processo não significa ter um monte de regras desnecessárias, mas sim um
pequeno conjunto de regras (um protocolo) para lidar com um processo de tomada
de decisão acerca de bens e recursos rivais, com a segurança e a privacidade,
podendo deixar os bens não-rivais se formarem de acordo com a experiência.

Para criar protocolos sociais, portanto, as seguintes perguntas podem ser de
grande valia:

1. Quais são os bens e recursos rivais a serem compartilhados pelo grupo?
2. Quais são os requisitos de segurança, privacidade e confiança no grupo?
3. Quais são os requisitos de administração da informação (canais de
   comunicação e documentação)?

Respostas práticas a essas questões permitem o esboço de um protocolo (ou
conjunto de protocolos):

1. Para a questão 1, o protocolo pode se dirigir ao processo de tomada de
   decisões.
2. Para a questão 2, o protocolo pode estabelecer um esquema de controle de
   acesso à informação.
3. Para a questão 3, o protocolo pode sugerir um fluxo informacional padrão que
   auxilie na comunicação, na documentação (memória) e eventualmente até nos
   critérios de distribuição de informação para entidades externas (licenciamento
   de conteúdo).

Protocolos híbridos, além de serem compatíveis simultaneamente com processos
formais e informais, são também propícios para se fazer um bom uso de recursos
limitados (trabalho, energia, matéria, etc. Bons protocolos híbridos fazem um
balanço entre formalidade (um conjunto excessivo de regras tende a ser
burocrático) e informalidade e auxiliam aos grupos acumularem mais excedente
com menos trabalho.

Além disso, pode ser conveniente balizar a criação de protocolos levando em
conta o princípio do não-preconcebimento, que afirma que ''nada que foi
explicitamente informado ou acordado deve ser considerado ou assumido''. Por
exemplo se alguém não informou que está realizando uma dada atividade, então
não há condições suficientes para se considerar que essa pessoa a está
realizando.

Ou seja, a iniciativa só deve ser considerada se houver disponibilidade de
informação. Até pode acontecer que alguém que não informou que esteja
trabalhando na verdade esteja, mas pela inexistência dessa informação não
podemos nos arriscar a considerá-lo.

## A urgência e a emergência da organização

Um dado "nível" de organização/acumulação permite inclusive tornar situações
antes emergenciais em procedimentos bem estabelecidos. Por isso, um grupo que
se dedica à sua organização terá um ganho futuro de lidar melhor com situações
que hoje são urgentes. Se o grupo apenas se dedicar a apagar o fogo, a resolver
emergências/urgências, não terá tempo para mudar e melhorar sua organização.
Certamente o mundo apresenta uma série de situações emergenciais. A urgência
permeia a existência.

Não é possível não se omitir em todas as lutas, em todas as frentes, em todas
as tarefas. Há uma rivalidade de atuação porque os grupos não são ilimitados. A
energia das sociedades humanas não escala indefinidamente, ao menos atualmente,
apesar de ser possível para um grupo ao menos apoiar -- nem que seja uma
declaração de apoio -- múltiplas causas, mas escolhas precisam ser feitas.

Por isso, é importante para um grupo dividir bem sua dedicação, seu tempo e seu
esforço não apenas para todas as emergências, mas também para a sua
organização. Às vezes é preciso dar um basta à resolução de emergências e
dedicar um pouco do tempo à organização. Ao se organizar mais, a resolução de
algumas emergências podem se tornar tarefas mais fácil.

## Referências

* [1] Licença de Manipulação de Informações do Grupo Saravá: http://wiki.sarava.org/Main/Licenca
* [2] Protocolo de Ação Coletiva, http://protocolos.sarava.org/trac/wiki/Organizacao
* [3] A tirania das organizações sem estrutura: https://web.archive.org/web/20111216165058/http://www.midiaindependente.org/pt/blue/2001/07/3257.shtml
* [4] O registro também pode ser entendido tanto como memória coletiva como superfície de inscrição do corpo coletivo. O registro é a memória da autogestão coletiva.
* [5] Existiriam paralelos ou mesmo identidades com os conceitos de máquinas desejantes, fluxo de produção, superfície de inscrição e corpo sem órgãos? Estaria então o desarranjo também inserido nesse princípio de funcionamento? Deixamos estas questões em aberto.
* [6] Os princípios das mídias e grupos livres - http://encontro.sarava.org/Principal/ConjuntoDePrincipiosEticos.
* [7] Veja, por exemplo, o texto A Organização II - Errico Malatesta - 11 de julho de 1897, http://nucleos-fasp.blogspot.com/2008/08/organizao-ii-errico-malatesta-11-de.html
