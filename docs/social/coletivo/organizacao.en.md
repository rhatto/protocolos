# Collective Action Protocol

{{ info.context.en }}

{{ warning.needs_work.en }}

The protocol was written to make possible a generic adoption by groups but at
the same time it attempt to solve specific problems that aren't necessarily
shared by a given group. Then, it's not intended to be "The Collective Protocol"
but instead a suggestion about how to shape a collective protocol.

Suggestion is to read it as how could a collective protocol look like instead a
suggestion of adoption, especially because usually the needs for a process are
different as here we're trying to solve a different set of problems.

The Collective Action Protocol seems to be both a stateless (at it's informal
part) and stateful (at it's formal part) protocol. It has some inspiration
from:

* Games, although it's unknown how a parallel with game theory could be traced
  (could this protocol be considered a game where the objective is a win-win
  outcome attempting to maximize the collective effort?).
* Free and open source software development processes (although this protocol
  makes the 'benevolent dictator' obsolete as behaviour, purpose and telealogy
  turns to be mean properties emerging internally from the processes and
  peoples' wishes and mutual relationship). It can be thought as a social
  software (culture).
* Ways labor division can be organized to better fill collective needs and
  autonomy while encouraging people to work together.

## Characteristics

For synthetic purposes, most of the discussion was split from the protocol
text. Perhaps this compression let a lot of helpful information to be missing,
so here comes a list of the main properties this protocol tries to achieve:

* Resilience, robustness and failover.
* Ensure some collective autonomy is guaranteed (through formal process having
  responsibility attached) while not blocking the self-organizing efforts using
  to kinds of processes (formal and informal). This means in some way to
  overcome the [The Tyranny of
  Structurelessness](http://en.wikipedia.org/wiki/Tyranny_of_Structurelessness)
  while not failing back to over-structuration, bureaucracy, etc and at the
  same time tries to deal with apathy and missparticipation.
* For formal processes, splits decision-making (i.e, the step where the
  collective evals if a given proposal is pertinent an deserves the collective
  moral support) from the resposibility assignment (step where people actually
  volunteer to do the tasks) in a way that just formal processes that are
  under responsibility can be counted as processes that will very probably
  happen, so if in the proposal/discussion/decision steps just the content of
  the proposal is evaluated, the responsibility assignment is a filter where just
  processes that finds people to achieve them are able to pass.
* Letting a process be also a registry eases the integration with a ticket system.

## Limitations

Some limitations of the protocol are:

* Not sure whether it's scalable. Maybe it was set for usage inside a small
  affinity group so it might not fit to big groups where people has not
  affinity between themselves.
* It does not deal with connections/disconnections, i.e, the protocol do not
  define how someone joins or leaves the group.
* As all protocols depends on it's usage, it is not a guarantee by itself that
  things are going to happen in the collective. The protocol just states how
  the energy is spent in the collective process but have no influence in the
  desire to act or not to act.
* It does not state what are the communication channels (both for formal and
  informal communications) and how they should be used. In fact, as this really
  varies a lot from collective to collective so it was chosen to leave that
  specific communication structure out of the Collective Action Protocol so it
  would be easier to share and have another protocol taking care of
  informational channels.

## Formality and informality

In this protocol, what is a formal and what is an informal process really
depends in how the collective wants to extend it's autonomy, so it's not a
thing defined withing the protocol. Examples of formal and informal processes
can be:

* Informal processes: meetings, dinners, researching, write texts, code, talk
  with people, wikifarming and everything else that the collective thinks that
  does not changes the collective autonomy.
* Formal processes: the main activities the group is committed to do and what is
  crucial for them to happen and to keep the collective autonomy.

## Collective Action Protocol

* Version: 0.1.
* Raw english translation version (related to protocol version): 0.1.
* License: Sarava Content Distribution License (no translation for now :/)

### Processes and autonomy

Everything that happens in the Collective is a process. Processes has different
manifestations but are mainly fluxes and the registry of these fluxes
(memory/information).

There are two kinds of processes:

* Formal processes
    * Shape/form predefined by collective consensus AND
    * Needs/use the collective autonomy THUS
    * Needs to be followed by a minimum responsabilization so the process do
      not fail

* Informal processes
  * Shape/form not needed to be predefined AND
  * Do not need/use the collective autonomy THUS
  * Do not need to be under the resposability of someone, i.e, an informal
    process that do not happen doesn't affect the collective autonomy

Activities without information in the collective cannot be considered by
processes (formal or informal) as these activities do not have information
equality/isonomy in the collective. To have information about a process is a
requisite for participation and then an activity without information inside the
collective cannot be considered as process.

The basic autonomy of the Collective, i.e, the minimum autonomy that allows
it's existence according to this protocol is the existence of secure and
private communication channels that allows the existence of collective
processes (formal or informal). Without these channels, the basic collective
autonomy is seriously damaged as well as the application of this protocol. All
additional autonomy in the collective (i.e, autonomy that is not contained in
the basic autonomy) should be defined with formal processes.

A collective member act inside the collective when use it's resources or the
collective name. By the other hand, a collective member act outside of the
collective when do not use such resources or the collective name. Collective
members do not make actions (inside or outside the collective) that
consciously can cause damage to the collective autonomy.

### Formal processes

Each formal process is an instance of the following state diagram:

       .------------------->-----------------.
      /  .----------<--------------<-------.  \
     |  '                                   \  \
     |  |               .------>-----.       \  \
     |  |              |              \       \  \
    Proposal -----> Discussion ->-.    \       \  \
       |  ^            |           \    \       \  \
       |  |            |            \    \       \  \
       |   `----<-----'             |     \       \  \
       |                            |      |       \  \
        `------>----- Decision --<--'      |        \  \
                         | |               |         \  \
                         | |               |          | |
    Responsibility --<---' '------> Archiving --->---'  ;
      Assignment --------->---------' ^    \           /
        ^  |              ___________/      `---<-----'
        |   \           .'
        |    `--> Achievement ->--.
        |           |  |           \
        |           |  |           /
         `----<-----'   `-----<---'

* Proposal: step where an idea of a formal process is presented to the
  collective. The idea -- or description -- can come for the Archive, from a
  previous Discussion, from an informal process that needs to be formalized or
  from a person or group from inside or outside the collective. General
  recommendation is to give a good explanation of the idea containing: deadline
  suggestion, process life cycle, responsibility assignment criteria and
  deadline and recommendations for emergencies (when applicable).

* Discussion:
    * It's not a mandatory step, but has importance anyway.
    * Changes to proposals make the formal process go back to the Proposal
      state. Proposals that do not follow to the Decision step or do not get
      changes until it's deadline should be archived.
    * Changed proposals that come from outside the collective or that have
      external groups or persons participation should be sent back also to the
      external group/person, despite these persons/groups do not participate in
      the internal discussion at the collective. If these external
      people/groups agree with the changed proposal, then the formal process
      proceeds with the discussion using the changed proposal. If that doesn't
      happen, i.e, these external people/groups don't agree with the changed
      proposal, then the formal process is archived (except if the external
      parties provide another changed proposal or more arguments to the
      discussion).

* Decision:
    * Through consensus and the active participation depends in following the
      information required by the proposal.
    * If there's no consensus about the decision of a proposal it's
      automatically blocked with the possibility to extend it's deadline.
    * Silence regarding a proposal is considered as an agreement.
    * Deadline: general reccomendation is to set deadlines relative to the
      average time needed by active people in the collective to take into
      account, discuss, make changes and ask for eventual postponing. Processes
      are eligible to postponing or advance it's deadline with an explicitly
      request from someone from the collective. If there's no such request, the
      initial deadline is assumed.
    * Approvals from proposals coming from outside the collective or that have
      external people/groups involved are communicated about the approval just
      after the responsibility assignment.

* Responsibility assignment
    * Concerns the minimization of points of failure.
    * Responsibilization is a volunteer action but requires the submission of a
      commitment/responsibilization term affirming that the person:
        * Has knowledge about the procedure in question.
        * Is gonna achieve it under the estimated deadline and is gonna keep
          the collective informed about the task.
        * Will inform the collective in a reasonable timeframe if cannot
          continue to be involved with the process so the collective can keep
          the process going, assign new responsibilities or finish it and send
          to the archive.
    * Non-accomplishment with a process compromises the ability of someone to
      be responsible for other tasks.
    * Formal processes that are approved but, after the responsibility
      assignment deadline, have insufficient responsibilization assigned,
      should be sent to the archive. Processes in the archive were previously
      approved cannot be sent back directly to the responsibilty assignment and
      should instead go to the proposal step.
    * In case of proposals coming from outside the collective or that have
      external people/groups involved, these people/groups should be informed
      of the approval just after the responsibility assignment, i.e, at the end
      of this step.

* Achievement
    * Just formal processes with sufficient responsibility assignment can go to
      the achievement step. Processes that were already achieved goes to the
      archive.
    * Formal processes that were not achieved in the deadline should come back
      to the Responsibility assignment step. In a similar way, processes whose
      assigned people cannot doing it should come back to the Responsibility
      assignment step if the number of assigned people is smaller than the
      required.
