# Checklist Básico

{{ info.context.pt }}

{{ warning.needs_work.pt }}

O que um grupo precisa em geral para funcionar:

* Plataforma de comunicação.
* Sistema de acompanhamento de tarefas.
* [Protocolos](https://protocolos.fluxo.info) de operação!

Fluxo de trabalho:

* Reuniões periódicas de trabalho coletivo: pesquisa, desenvolvimento, implementação, manutenção e auditoria.

Opcional:

* Site e canais de contato públicos.
* Licença de distribuição de conteúdo.
* Termos de serviço e política de privacidade.
