# Termo de responsabilização

{{ info.context.pt }}

{{ warning.needs_work.pt }}

    Me compromento a me responsabilizar pela realização da tarefa/processo X
    de acordo com os termos indicados na descrição da tarefa/processo e
    mantendo o coletivo informado do seu andamento.

    Caso não possa mais manter minha responsabilização, me comprometo a
    informar o coletivo com antecedência.

    No caso do encerramento da minha responsabilização acarretar numa
    responsabilidade mínima menor do que aquela especificada pela tarefa/processo,
    me comprometo também e a passá-la para frente ou, na ausência de pessoas
    que componham a responsabilidade mínima, me comprometo a arquivar a
    tarefa/processo, levando em conta os procedimentos específicos para
    o caso.
