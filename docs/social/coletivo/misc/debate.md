# Debate

{{ info.context.pt }}

{{ warning.needs_work.pt }}

Realização de um debate no `$local` ou em outro lugar conveniente (mas não pago
e nem corporativo), eventualmente em parceria com o `$grupo`, de um debate
sobre `$tema`. O evento seria oficialmente organizado pelo Coletivo e talvez
pelo `$grupo` (caso topem) e portanto se trata de um processo formal.

Tal evento é politicamente interessante ao Coletivo ao marcar presença no
`$local`, além de preencher uma lacuna pela falta de debates sobre o assunto.

## Tarefas

As tarefas envolvidas são:

* Marcar uma boa data.
* Reservar o `$local` ou local apropriado, juntamente equipamento necessário.
* Convidar palestrantes que entendam do assunto e possuam visão crítica.
* Fazer, imprimir e afixar cartazes em locais chaves.
* Divulgação pela internet e eventualmente pelo Portal do Coletivo.
* Realizar o debate, fazendo a mediação se necessário.

## Observações

* O debate não terá o apoio de organizações corporativas ou estatais e não
  contará com financiamento.
* As pessoas do Coletivo que estiverem presentes na mesa não falarão em nome do
  Coletivo.

## Sobre este texto

O texto deste processo foi redigido utilizando o [Template para
Debate](/organizacao/misc/debate). No caso de alterações que não dizem respeito
apenas ao Grupo e que possam enriquecer tal template, favor submetê-las também
upstream, isto é, ao texto do template.
