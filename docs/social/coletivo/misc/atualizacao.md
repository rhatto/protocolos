# Atualização de processo

{{ info.context.pt }}

{{ warning.needs_work.pt }}

O presente processo efetua a atualização do seguinte processo:

* `$processo`

O novo texto para o processo é o seguinte:

    $novo_texto

## Condições de atualização

* O processo só pode ser atualizado se as pessoas responsabilizadas pelo mesmo
  concordarem explicitamente com isso. Caso elas não concordem este processo
  deve ser arquivado.
* No ticket do processo deve haver uma menção à atualização efetuada e com
  referência ao processo que o alterou.

## Responsabilização

As pessoas responsáveis pelo presente processo devem efetivar a atualização do
processo de acordo com as condições mencionadas anteriormente.

## Sobre este texto

O texto deste processo foi redigido utilizando o [Template para Atualização de
Processo](/organizacao/misc/atualizacao). No caso de alterações que não dizem
respeito apenas ao Grupo e que possam enriquecer tal template, favor
submetê-las também upstream, isto é, ao texto do template.
