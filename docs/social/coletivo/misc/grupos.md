# Participação no $grupo

{{ info.context.pt }}

{{ warning.needs_work.pt }}

Este processo estabelece a participação do Coletivo no $grupo, doravante
mencionado neste texto tanto como $grupo ou simplesmente como '''Grupo'''.

## Descrição do Grupo

A descrever.

## Informações de contato e comunicação

A descrever.

## Critérios de privacidade e segurança do Grupo

A descrever.

## Descrição das tarefas relacionadas

Considerando que:

1. Não se pode assumir que todas as pessoas do Coletivo estejam participando
   ativamente do Grupo e que
2. O Coletivo precisa discutir, propor e emitir posições acerca de questões relativas ao Grupo.

As tarefas para a participação no Grupo consistem em:

1. Levar do Grupo para o Coletivo as questões a serem discutidas, decididas
   e/ou que sejam de interesse do Coletivo ou de pessoas do Coletivo, documentando
   na medida do possível os processos relacionados nas instâncias e seções
   correpondentes do Coletivo usadas para tais fins.

2. Levar do Coletivo para o Grupo as propostas, discussões, sugestões e
   posicionamentos que partirem do Coletivo e cujo envio estiver aprovado,
   documentando na medida do possível os processos relacionados nas instâncias e
   seções correpondentes do Grupo usadas para tais fins.

3. Providenciar ao Coletivo ou à pessoas do Coletivo informações disponíveis no
   Grupo mediante requisição da parte interessada.

## Responsabilização

Em todos os casos, as pessoas responsabilizadas:

1. Se comunicarão do Grupo para o Coletivo (e vice-versa) observando os
   critérios de privacidade segurança do Coletivo e do Grupo.

2. Informarão explicitamente, caso estejam repassando informações que o
   Coletivo envia para o Grupo, que tais informações representam uma comunicação
   oficial e formal do Coletivo. Nos demais casos, recomenda-se que as pessoas
   responsáveis informem explicitamente que estão se comunicando informalmente
   e/ou que a comunicação não representa a posição do Coletivo.

3. Realizarão, na medida do possível, a tradução de comunicação de e para o
   português, nos casos em que a mesma precise ser realizada noutro idioma.

Cabe ainda observar que:

1. As pessoas responsáveis por tal comunicação entre o Coletivo e o Grupo são
   denominadas de ''[http://en.wikipedia.org/wiki/Liaison liaisons]'' entre o
   Coletivo e o Grupo, podendo atuar também como
   ''[http://en.wikipedia.org/wiki/Proxy proxies]'' para pessoas do Coletivo, isto
   é, enviar informações para o Grupo oriundas de pessoas do Coletivo que não
   queiram se identificar.

2. Cabe aos/às ''liaisons'' dividirem entre si se organizarem para dividir as
   tarefas relativas ao acompanhamento e repasse de informações do Coletivo para o
   Grupo e vice-versa.

3. Oas/as '''liasons''' respeitarão e observarão os critérios, regras e
   sugestões de conduta, comunicação e etiqueta tanto do Grupo quando do Coletivo.

## Compartilhamento de informações

Caso o Grupo mantenha comunicações em outros idiomas que não o português, é
possível ainda compartilhar informações sobre o Grupo que não sejam
específicas/internas do Coletivo entre outros grupos lusófonos/brasileiros que
também estão no Grupo, desde que isso seja feito atendendo os critérios de
segurança e privacidade do Grupo e do Coletivo.

Além disso, tal repasse de informações:

1. Não é de responsabilidade dos/as ''liaisons''.
2. Precisa de autorização do Coletivo.

## Sobre este texto

O texto deste processo foi redigido utilizando o
[wiki:PageTemplates/ParticipacaoEmGruposExternos Template para Participação em
Grupos Externos]. No caso de alterações que não dizem respeito apenas ao Grupo
e que possam enriquecer tal template, favor submetê-las também ''upstream'',
isto é, ao texto do template.
