# Template para Autorização de Uso de Conteúdo

{{ info.context.pt }}

{{ warning.needs_work.pt }}

Por este processo, `$entidade` fica autorizada a utilizar os seguintes conteúdos do Coletivo:

1. `$conteudo`

Com as seguintes permissões:

1. Distribuição do conteúdo nos seguintes meios:
    * a. Digital.
    * b. Impresso.
    * c. Audiovisual.
2. Alteração do conteúdo mediante inclusão de nota descrevendo as mudanças efetuadas.
3. Vigente para versões anteriores e futuras do conteúdo.

Com as seguintes restrições:

1. Impedido o uso comercial ou fins lucrativos.
2. A preservação da licença original do conteúdo.
3. Uso restrito apenas para a publicação intitulada `$publicacao`.
4. A validade desta permissão é de `$validade`, podendo ou não ser renovada.

# Dependências

A realização deste processo depende da realização dos seguintes processos:

* [Licenciamento de informações](/organizacao/license).

# Sobre este texto

O texto deste processo foi redigido utilizando o [Template para Autorização de
Uso de Conteúdo/organizacao/misc/autorizacao]. No caso de alterações que não
dizem respeito apenas ao Grupo e que possam enriquecer tal template, favor
submetê-las também upstream, isto é, ao texto do template.
