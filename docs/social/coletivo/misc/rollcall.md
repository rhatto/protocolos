# Rodada de Chamadas

{{ info.context.pt }}

{{ warning.needs_work.pt }}

A Rodada de Chamadas, também conhecida como
[http://en.wikipedia.org/wiki/Roll_call roll call], é um procedimento utilizado
para saber quem ainda participa de um grupo. Em cada rodada, das pessoas
ausentes nas atividades do Coletivo, apenas aquelas que responderem permanecem
no grupo conforme o [Processo de participação no
Coletivo](/organizacao/coletiva/participacao).

## Chamado

As pessoas envolvidas no Coletivo que estiverem ausentes das atividades do
Coletivo tem o período entre o início e o término da realização do processo
para se pronunciarem a respeito da sua permanência no Coletivo, caso contrário
estarão passíveis de retirada do Coletivo.

Esta também pode ser considerada como uma oportunidade para as pessoas
apresentarem suas atividades e seus anseios. :)

## Sobre este texto

O texto deste processo foi redigido utilizando o [Template para Rodada de
Chamadas](/organizacao/misc/rollcall). No caso de alterações que não dizem
respeito apenas ao Grupo e que possam enriquecer tal template, favor
submetê-las também upstream, isto é, ao texto do template.
