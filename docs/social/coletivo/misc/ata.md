# Ata de reunião

{{ info.context.pt }}

{{ warning.needs_work.pt }}

# Reunião de ../../....

* Horário: ..:..
* Local: ... (Cidade).
* Caráter: ...
* Presentes: ...

# Ata

## Informes

...

## Próximas reuniões

...

## Outras questões

...
