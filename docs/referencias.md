# Referências

## Pedido de Comentários (RFC)

* Usando RFCs:
  [How to Stop Endless Discussions](https://candost.blog/how-to-stop-endless-discussions/).

## Metadados

* {{ metadata.version.pt }}: `ver:2024-09-14`.
* {{ metadata.identifier.pt }}: `pid:referencias`.
