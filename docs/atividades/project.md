# Projetos

{{ info.context.pt }}

{{ warning.needs_work.pt }}

## Sistemas de tarefas simples

Sistema de tickets, como por exemplo:

* No README.
* Arquivo TODO em formatos plaintext, Markdown, YAML ou híbridos.
* Um arquivo por tarefa em pastas `open` e `closed`.
* Ditz, bugs-everywhere, taskwarrior ou similar.
* Aplicação própria (Trac, etc)
* TODOs and FIXMEs ao longo do código em última instância.

## Projetos de Software

* Vagrantfile e manifests do puppet.
* Git ou VCS usado upstream.
* Branches de desenvolvimento e upstream.
* Workflow padrão (git-flow, git-hooks, etc).
* Suíte de testes.
* Adotar [Semantic Versioning](http://semver.org).

## Ferramentas

* [Planilha de Projeto](project.fods).
