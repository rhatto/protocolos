# Clube da Muamba $nome

{{ info.context.pt }}

{{ warning.needs_work.pt }}

O Clube da Muamba $nome pode ser composto por pessoas que fazem parte de uma
mesma vizinhança. Para saber a respeito da disponibilidade e viabilidade do
empréstimo de um dos recursos listados, entre em contato com o/a dono/a ou
zelador/a e eventualmente também com quem está atualmente com ele.

Por uma questão de logística, as muambas podem estar separadas por região, o
que não impede de uma pessoa de outra região pedir algo emprestado que esteja
em outra, desde que consiga resolver o problema do transporte :)

Lista de Muambas
----------------

| Equipamento            | Detalhes | Disponibilidade | Dono/a ou Zelador/a | Com quem está atualmente |
|------------------------|----------|-----------------|---------------------|--------------------------|
| Exemplo de equipamento | troca    | Total           | email@de.contato    | email@de.contato         |

Viajantes
---------

Esta seção existe para permitir que pessoas que viajarão de um local para outro
possam trazer muambas para outras pessoas e grupos (serviço voluntário de mula
:P). Recomenda-se dar preferência para trazer muambas a grupos ou objetos que
constarão no clube da muamba e deixar muambas para uso exclusivamente pessoal
com menor prioridade (mas que nem por isso sejam desencorajados os traslados de
muambas pessoais.

| Viajante | Local de origem | Local de destino | Data de partida | Data de chegada (ou retorno) | Observações |
|----------|-----------------|------------------|-----------------|------------------------------|-------------|
| -        | -               | -                | -               | -                            | -           |

Lista de Reparos
----------------

Nesta tabela as pessoas e/ou grupos podem colocar os bens que, por motivos de
força maior, poderiam mas não estão no Clube se estivessem em condições de uso.
Também aqueles que são inviáveis para o dono consertar mas de conserto
vantajoso para terceiros! :)

| Recurso | Detalhes | O que precisa(ou gostaria) | Possibilidade de vaquinha |
|---------|----------|----------------------------|---------------------------|
| -       | -        | -                          | -                         |

Lista de Livros
---------------

Aqui ficam listados os livros, revistas, etc que estão na roda, formando assim
nossa biblioteca distribuída e autogestionada :)

| Livro | Autor | Disponibilidade | Dono/a ou Zelador/a | Com quem está atualmente |
|-------|-------|-----------------|---------------------|--------------------------|
| -     | -     | -               | -                   | -                        |

Lista de Filmes
---------------

Esta é uma lista experimental onde ficam listados os filmes que estão na roda,
formando assim nossa videoteca distribuída e autogestionada. Adicione apenas
filmes em '''cópias físicas removíveis''' (DVDs, VHS, VCD), já que estamos
fomentando o empréstimo de bens físicos. Ou seja: se você tiver uma lista
imensa de filmes no seu disco, opte por utilizar um sistema de compartilhamento
P2P para aliviar o volume desta lista.

| Filme | Áudio/Legenda | Mídia | Disponibilidade | Dono/a ou Zelador/a | Com quem está atualmente |
|-------|---------------|-------|-----------------|---------------------|--------------------------|
| -     | -             | -     | -               | -                   | -                        |
 
Lista de Vontades
-----------------

Nesta tabela as pessoas e/ou grupos podem colocar os bens que gostariam que
estivessem no Clube.

| Recurso | Detalhes | Quem gostaria ou precisa | Possibilidade de vaquinha |
|---------|----------|--------------------------|---------------------------|
| -       | -        | -                        | -                         |

Lista de Reparos
----------------

Nesta tabela as pessoas e/ou grupos podem colocar os bens que, por motivos de
força maior, poderiam mas não estão no Clube se estivessem em condições de uso.
Também aqueles que são inviáveis para o dono consertar mas de conserto
vantajoso para terceiros! :)

| Recurso | Detalhes | O que precisa(ou gostaria) | Possibilidade de vaquinha |
|---------|----------|----------------------------|---------------------------|
| -       | -        | -                          | -                         |
