# Termo de empréstimo

{{ info.context.pt }}

{{ warning.needs_work.pt }}

## Plano Básico

Adoro emprestar minhas coisas! Empresto a muamba que você me pediu desde que:

* Você devolva conforme nosso combinado :)
* Caso haja alguma quebra ou perda da muamba, você se compromete a ressarcir o
  prejú.
* Se eu precisar dela antes do nosso combinado, você topa devolvê-la.

## Planos específicos

* Coisas frágeis (discos de vinil): como cuidar.
* Como proceder em casos de quebras:
*   Peças substituíveis.
*   Peças insubstituíveis.
* Entrega e devolução.
