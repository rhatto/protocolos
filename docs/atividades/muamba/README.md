# Clube da Muamba

{{ info.context.pt }}

{{ warning.needs_work.pt }}

Sabe aquela festa de rua que não acontece porque falta o gerador de energia,
aquele filme que não é feito porque não há câmera ou aquela rádio que não
transmite por falta de transmissor? E aquele panfleto que não é impresso porque
ninguém tem acesso a uma máquina de Xerox ou aquele passeio que não é feito por
falta de bicicletas?

É muito frustante não conseguir fazer algo por falta de recursos. Mais
frustrante ainda é saber que em geral muito desses recursos são possuídos por
alguém conhecido ou, alternativamente, poderiam sê-lo caso se soubesse da
necessidade do seu uso. Mesmo que alguém compre uma mesa de som para dar uma
festa, essa pessoa não a usará necessariamente todos os dias, e isso vale para
muitos dos nossos objetos pessoais: máquinas fotográficas, microfones, rádios,
etc, nem tudo isso é usado por nós o tempo todo, mas no entanto há um forte
apelo do mercado para que cada um de nós tenha um exemplar de cada um dos
milhares de produtos da indústria de massa.

O Clube da Muamba é uma iniciativa cultural de pensar novas formas de economia
que não se baseiam no valor de troca, mas no valor de uso não-rival de objetos
físicos. Para montar um clube da muamba, basta juntar seus/as amigas/os e
colocar na roda o que cada pessoa quer compartilhar.

## Como funciona?

Existem mil maneiras de fazer um Clube da Muamba, cabendo a cada grupo inventar
e/ou adaptar a sua e portanto aqui deixamos apenas sugestões de como fazer o
seu. O Clube da Muamba não é um grupo fixo ou organização institucional, mas
uma idéia para que existam muitos Clubes da Muamba e para que a prática de
empréstimos de equipamentos e recursos inativos se torne parte da cultura comum
das pessoas ao ponto de ser desempenhado com naturalidade.

Basta reunir um grupo de pessoas, amigos/as ou voluntários/as que esteja
disposto a compartilhar recursos (materiais ou não), sejam livros,
eletroeletrônicos, máquinário pesado, veículos de transporte, etc. Cada pessoa
ou subgrupo de pessoas do clube pode então tanto compartilhar objetos que já
possuem como juntar recursos para adquirir (conjuntamente ou não) outros bens
para serem disponibilizados coletivamente.

## Logística

Nossa idéia é que não haja gerência cuidando dos empréstimos ou do contato
entre as pessoas, mas sim que a logística se realize através de autogestão
distribuída: cada pessoa ou grupo de pessoas que for dono de um dado recurso
adiciona essa informação numa tabela, juntamente com informações adicionais
sobre o recurso e disponibilidade de empréstimo. Assim, a pessoa ou grupo de
pessoas que necessitar de um dado recurso disponível precisa apenas entrar em
contato com os/as donos do recurso.

## Vaquinha contínua

A vaquinha perpétua é uma forma de aquisição constante, permanente ou variável
de equipamentos: um grupo de pessoas mantém um fundo coletivo que é gasto em
bens diversos conforme a necessidade de incluí-los no Clube.

# Projetos similares

* [Vizinhocas](https://github.com/coolmeia/vizinhocas).
* [NeighborGoods](http://neighborgoods.net).

## Conservação de equipamentos

De preferência:

* Mantenha ferramentas e equipamentos sempre prontos para o uso!
* Para não perder equipamentos ou ter dificuldades em encontrá-los, mantenha-os
  sempre num mesmo local.

## Centro de Estocagem de Bugigangas

O que fazer com o monte de coisas acumuladas? Um Centro de Estocagem coletivo
numa vizinhança pode dar conta de aproveitar melhor as coisas que não temos um
uso imediato.
