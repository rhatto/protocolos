# Checklist Completa para Viagens

{{ info.context.pt }}

* Itens básicos da Checklist Mínima.
* Máquina fotográfica, capa, cabo, bateria sobressalente, carregador, memória
  sobressalente e adaptador de memória.
* Canivete.
* Saco de dormir.
* Roupas (1 semana).
  * Camisetas.
  * Calças.
  * Blusas.
  * Camisa social.
  * Roupas íntimas.
  * Meias.
  * Pisantes.
  * Cintos.
  * Verão:.
    * Bermudas.
    * Havaianas.
    * Calção de banho.
