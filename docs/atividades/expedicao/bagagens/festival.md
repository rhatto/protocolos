# Festivais

{{ info.context.pt }}

Kit folião completo: itens para expedições de carnaval e outros festivais:

* [ ] Comunicação:
    * [ ] Celular básico com:
        * [ ] SIM card.
        * [ ] Mapas offline.
        * [ ] Outras funcionalidades ativadas.
    * [ ] QR code e número de telefone impressos e colados no verso do telefone.
    * [ ] Roteiro de blocos.
* [ ] Doleira com:
    * [ ] Mini-carteira tinfoil com:
        * [ ] Cartão de débito pré-pago.
        * [ ] CNH ou cópia colorida.
        * [ ] Dinheiro.
    * [ ] Preservativos.
    * [ ] Saco de lixo.
    * [ ] Tampões de ouvido.
    * [ ] Capucha.
    * [ ] Chaves (casa e bike).
* [ ] Fantasias.
* [ ] Kit boêmio:
    * [ ] Garrafas penduradas (PET, térmica ou dentro de uma embalagem tetra
          pak) ou camelback nas costas com drinks diversos, como:
        * [ ] Gin ou campari tônico.
        * [ ] Gin ou rum com água de côco.
        * [ ] Chá com gin ou rum.
        * [ ] Porto tônico.
        * [ ] Porto côco.
* [ ] Bônus (opcionalmente em micro-mochila dobrável de 10l):
    * [ ] Mini-mosquetões para prender coisas em shorts/bermuda/mochila.
    * [ ] Protetor solar fator 50.
    * [ ] Refil de goró (garrafinha pequena com destilado).
    * [ ] Cigarrilhas e isqueiro.
    * [ ] Algum instrumento de material reciclável e fácil de tocar.
    * [ ] Xixizator: garrafinha reutilizável com funil e pano de cobertura como
          sonda urinária portátil, discreta e que não viola a lei.
* [ ] Na bike:
    * [ ] Garrafa PET com água de côco e gelo de côco.
    * [ ] Refletores para bike (ao invés de luzes).
