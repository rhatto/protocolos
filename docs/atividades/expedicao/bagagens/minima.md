# Checklist Mínima para Viagens

{{ info.context.pt }}

## Documentos

* [ ] Micro-pasta para panfletos, papéis e documentos.
* [ ] Passaporte(s) (com capa) e xerox.
* [ ] Passagens.
* [ ] Saldo/extrato bancário.
* [ ] Seguro saúde.
* [ ] Cartas de convite e/ou reservas de hospedagem.
* [ ] Carteira de alberguista.
* [ ] Carteira de motorista nacional e internacional.
* [ ] Registro de equipamentos na Receita Federal.
* [ ] Lista de Contatos.
* [ ] Esta checklist.

## Finanças

* [ ] Dinheiro e comprovantes de câmbio.
* [ ] Cartões de crédito e débito internacional.
* [ ] Cartões de transporte do local de destino.

## Equipos

* [ ] Corrente de pescoço para prender chaves.
* [ ] Óculos (de sol e de leitura).
* [ ] Caderno de notas, grafite, lapiseira.
* [ ] Cartões de visita.
* [ ] Smartphone com:.
    * [ ] Mapas offline dos locais de passagem e destino.
    * [ ] Todos os ingressos, tickets etc necessários para a viagem.
    * [ ] Carregador USB, cabo USB e fones de ouvido.
* [ ] Laptop.
* [ ] Tablet.
* [ ] Playlist / músicas!
* [ ] Clipes de papel que funcionem como pregadores de roupas.
* [ ] Carregador USB-C acendedor de cigarro (parece que também tem no carro).
* [ ] App dicionário de espanhol.
* [ ] Garrafa de água reutilizável dobrável 500mL.
* [ ] Roupas dry fit.
* [ ] Gancho prendedor.
* [ ] Kit de costura.
* [ ] Talheres plásticos reutilizáveis.
* [ ] Barra cereais ou chocolate consistente/caramelado/lembas.
* [ ] Caneca.
* [ ] Mochila de ataque com capa de chuva.

## Higiene

* [ ] Remédios.
    * [ ] Analgésico.
    * [ ] Anti-térmico.
    * [ ] Esterilzador de água.
    * [ ] Protetor solar vegano/orgânico.
    * [ ] Pomada medicinal (Tiger Balm, Arnica, etc).
* [ ] Mini-detergente neutro/biodegradável.
* [ ] Mini-shampoo, mini-condicionador e mini-sabonete líquidos OU
      shampoo-sabonete em barra (biodegradáveis) (para corpo e roupas).
* [ ] Pasta de dentes, escova e fio dental.
* [ ] Mini-[pedra de allumbre](https://eswikipedia.org/wiki/Alumbre).
* [ ] Máscaras PFF2.
* [ ] Álcool 70 líquido.
* [ ] Álcool gel.
* [ ] Testes de doenças (como COVID?).
* [ ] Shampoo/sabão biodegradável.
* [ ] Hidrosteril (pegar na UBS).
* [ ] Remédios?
* [ ] Repelente em gel.
* [ ] Pano do tipo "perfex".
* [ ] Lâminas e haste para depilação/barbear.
* [ ] Tesoura sem ponta com lâminas inferiores a 6cm (medidas a partir do
      eixo).

## Roupas (1 semana)

* [ ] Toalha atlética.
* [ ] Capa de chuva.
* [ ] 3 roupas íntimas.
* [ ] 2 pares de meias.
* [ ] Verão:
* [ ] 2 camisetas dry fit.
* [ ] Chapéu?
* [ ] Sandália de trekking compacta ao invés do chinelo?
* [ ] Camisas mais elegantes?
* [ ] Bota para trilha.
* [ ] Inverno:
  * [ ] Palmilhas isolantes.
  * [ ] 2 conjuntos de segunda pele.
  * [ ] Jaco.
  * [ ] [Polar](http://enwikipedia.org/wiki/Polar_fleece Fleece)..
  * [ ] Par de luvas.
  * [ ] Cachecol.
  * [ ] Gorro.
