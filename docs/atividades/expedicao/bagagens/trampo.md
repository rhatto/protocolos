# Checklist do Trampo

{{ info.context.pt }}

Itens pessoais pra deixar na firma ou oficina de trabalho.

* Higiene básica:
  * Escova de dentes
  * Pasta de dentes
  * Fio dental
  * Palitos de dentes não-descartáveis
* Equipos:
  * Protetor auricular
  * Dinheiro reserva
  * Carregadores de bateria
