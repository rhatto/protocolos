# Urbenauta

{{ info.context.pt }}

## Kit verão

* Camiseta dry fit ou sem camisa.
* Calção de banho.
* Sacola zip grande.
* Cycling overshoes.

## Kit inverno

* Kit verão.
* Corta vento.
* Manteiga de cacau.

## Mochila

* Kit básico.
* Capa de chuva para mochila.
* Kit chuva verão ou inverno.
* Sacola dobrável para compras.
