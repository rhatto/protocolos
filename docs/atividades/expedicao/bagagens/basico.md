# Checklist básico

{{ info.context.pt }}

## Micro kit de ferramentas

* [ ] Clipes de papel.
* [ ] Lâmina de barbear.
* [ ] Elásticos.
* [ ] Isqueiro?

## Kit básico

* [ ] EPI.
* [ ] Escova de dentes.
* [ ] Fio dental.
* [ ] Placa de identificação?
