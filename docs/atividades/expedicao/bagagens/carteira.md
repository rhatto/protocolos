# Carteira cypherpunk

{{ info.context.pt }}

* Band-aid.
* Kit reparos de bike.
* Limpador interdental plástico.
* Telefones de emergência.
* Memórias e adaptador, incluindo [Bootless](https://bootless.fluxo.info) e [Tails](https://tails.boum.org).
* Fingerprints digitais.
* Saco zip para coletas.
* Régua de papel (mini fita métrica).
* Palheta.
* [Ficha de saúde](/pessoal/saude).
* Modelo de Habeas corpus.
