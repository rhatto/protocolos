# Checklist para Expedições

{{ info.context.pt }}

Equipamentos pequenos e úteis para expedições científicas:

* Itens da checklist de fuga.
* Placa solar carregadora ou manivela.
* Kit médico.
* Ração sem cozimento.
* Saco zip para amostras.
* Sacos para coleta de rejeitos.
* Saco impermeável.
* Serra de dedo/mão circular de sobrevivência em aço.
* Pederneira, apito, bússola, isqueiro.
* Purificador de água.
* Tela mosquiteira e repelente de insetos.
* Saco de dormir tipo múmia/sarcófago e capa impermeável.
* Isolante térmico.
* Rede leve com proteção mosquiteira.
* Botas.
* Rádio.
* Relógio.
* Espelho.
* Tesoura.
