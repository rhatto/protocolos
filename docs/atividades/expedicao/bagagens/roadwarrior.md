# RoadWarrior

{{ info.context.pt }}

Bill Of Materials - Road Warrior - To be used in the field.

* Laptops e bastante bateria.
* Micro SD Cards e pendrives.
* HDs de 2.5'' slim para diversos sistemas: Qubes, OpenBSD, etc.
* Smartphone de testes.
* Chaves Torx para smartphones.
* 32 and 64 bit ISOs: debian, ubuntu, tails, kali.
* LAN/WLAN router, preferably with OpenWRT.
* Réguas de luz.
* Adaptadores de tomada.
* Hub USB 3.0 com boa fonte.
* Kit de ferramentas.
* Discos externos.
* Pendrives.
* Cartões microSD e adaptadores.
* SIM cards.
* Adaptador VGA para datashow HDMI.
* [Material para oficinas](https://autodefesa.fluxo.info/grupos/oficina.html#material-de-apoio).
