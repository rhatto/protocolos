# Checklists para expedições

{{ info.context.pt }}

{{ warning.needs_work.pt }}

## Pré-viagem

### Geral

* [ ] Se possível, deixe sempre mala(s) sempre prontas e um procedimento padrão
      para viagens ou evacuação.

### Preparação Doméstica

* [ ] Emitir avisos de viagem (com a antecedência necessária), ligar resposta
      automática de férias e fornecer contatos de emergência a partes interessadas,
      por exemplo:
    * [ ] Família.
    * [ ] Amigos.
    * [ ] Clientes.
    * [ ] Contador.
    * [ ] Grupos e projetos.
* [ ] Contabilidade:
    * [ ] Adiantar pagamentos.
    * [ ] Situação de cartão de crédito.
    * [ ] Colocar contas em débito automático.
    * [ ] Fechar operações comerciais desnecessárias (empresas, contas, etc).
* [ ] Combinar cuidadores/as para:
    * [ ] Animais domésticos e públicos.
    * [ ] Casas e escritórios (caseiro ou visitas periódicas).
    * [ ] Tarefas administrativas.
    * [ ] Procurações e demandas legais, administrativas e contábeis.
* [ ] Manutenção preventiva em equipamentos, por exemplo:
    * [ ] Sincronizar arquivos.
    * [ ] Limpar servidores.
    * [ ] Checar nobreaks.
    * [ ] Backups e redundância.
    * [ ] Dosagem de cloro em mini-estação de tratamento de água.
    * [ ] Suplente para administração de datancenters e outras instalações.
* [ ] Pessoal:
    * [ ] Preparação do TPC.
    * [ ] Declaração Canária.
    * [ ] Modo nomail em listas.
    * [ ] Cortar cabelo.
    * [ ] Trancar matrículas em cursos.
    * [ ] Guardar pertences.
  * [ ] Burocracia:
    * [ ] Checar documentação, como por exemplo:
        * [ ] Vistos.
        * [ ] Comprovantes de vacinação.
        * [ ] Seguro de viagem obrigatório.
        * [ ] Considere imprimir em papel tudo o que você necessita para
              completar sua viagem (endereços, reservas, passagens), evitando
              situações em que seus dispositivos de comunicação estão indisponíveis.
    * [ ] Subir arquivos relevantes na pasta compartilhada: https://nc.torproject.net/f/466416
    * [ ] Emitir avisos de férias (Pilates, etc).
    * [ ] Sincronizar arquivos da viagem (reservas, passagens etc) em dispositivos.
    * [ ] Adicionar traslados na agenda.

## Na viagem

### Geral

* [ ] Para não perder coisas durante as viagens, mantê-las sempre agrupadas num
      mesmo local.
* [ ] Proteja seus pertences, não vacile!

### Saúde

* [ ] Protocolos sanitários (uso de máscaras etc) e outras políticas durante
      trechos:
    * [ ] Quarentena preventiva de doenças antes de viagens.
    * [ ] Testes de doenças como COVID-19 antes de trechos nacionais/internacionais.
    * [ ] Testes de doenças como COVID-19 diários antes de contatos com pessoas.

### Bagagem

* [ ] Roupas:
    * [ ] Para lavar a pouca roupa, use:
      * [ ] [Vodca][] ou outra bebida.
      * [ ] Álcool ou 70 ou gel, que pode ser mais barato e fácil de encontrar.
    * [ ] Para minimizar a lavagem de roupas, deixe-as tomando ar após o uso.
    * [ ] Camisetas podem ser penduradas de cabeça pra baixo para que o ar circule
          na região das axilas após passar álcool nelas, evitando a formação de
          colônias de bactérias.
    * [ ] Pendurar camisas e camisetas do avesso em cabides.
* [ ] O "modo avião" (ou trem, ônibus etc):
    * [ ] Mochila de mão para traslados enquanto a mala principal fica no baú acima
          da poltrona:
      * [ ] Óculos de sol, para apreciar a paisagem.
      * [ ] Travesseiro.
      * [ ] Máscara olhos.
      * [ ] Tampão de ouvidos.
      * [ ] Talheres não-descartáveis.
      * [ ] Caneca.
      * [ ] Lenços de papel.

[Vodca]: http://www.tiosolid.com/vodka-as-10-outras-utilidades-alm-de-beber

## Pós-viagem

### Organização

* [ ] Revisar listas de checagens e de bagagens, atualizando novas inclusões.
* [ ] Outros procedimentos de finalização/arquivamento/documentação de viagens.

### Finanças

* [ ] Reembolsos (passagens de trem, traslados, refeiçõoes, materiais etc).

### Bagagem

* [ ] Fazer o procedimento de armazenamento de longo período:
    * [ ] Lavar e secar tudo o que teve contato com suor e gordura (como
          blusas,toucas, lenços e traseira de mochilas), evitando assim o mofo.
* [ ] Um histograma de uso de cada objeto pode ajudar a refinar a bagagem: o que
      manter ou retirar da mala/lista?
* [ ] Laptop:
    * [ ] Retorno do carregamento parcial da baterial, por exemplo com o comando
          `tlp setcharge 75 80`.
