# Laboratório de Hardware

{{ info.context.pt }}

{{ warning.needs_work.pt }}

## Equipamentos

Lista de equipamentos básicos para um laboratório de hardware.

* Avental.
* Lupa com garras.
* Caixa de ferramentas (martelo, alicate, fenda e philips).
* Cabos JTAG.
* Cabo USB to Serial (Raspberry Pi):
  * [Ultimate Serial Port](http://www.mysticengineering.com/debug.buddy/pi.usage.html).
  * [USB to TTL Serial Cable - Debug / Console Cable for Raspberry Pi ID: 954 - $9.95 : Adafruit Industries, Unique & fun DIY electronics and kits](https://www.adafruit.com/products/954).
  * [USB MPSSE Cables](http://www.ftdichip.com/Products/Cables/RPi.htm).
* Câmera infravermelha.
* Leitores de Smartcard/Simcard.
* Leitor de RFID.
* Leitor de senhas da BIOS dos Thinkpads.
* [Bus Pirate - DP](http://dangerousprototypes.com/docs/Bus_Pirate).
* [Bus pirate basic probe set ID: 238 - $7.00 : Adafruit Industries, Unique & fun DIY electronics and kits](https://www.adafruit.com/products/238).
* [Bus Pirate - v3.6a - TOL-12942 - SparkFun Electronics](https://www.sparkfun.com/products/12942).
* [SparkFun FTDI Basic Breakout - 3.3V - DEV-09873 - SparkFun Electronics](https://www.sparkfun.com/products/9873).
* SDR Dongles.
  * Baseados no rtl2832 como FunCubeDongle Pro+, que funciona em OM e OC (vai mais baixo em frequencia).
  * Recomendo dar uma olhada no LimeSDR também.
* Kit lockpicking.
* Multímetro.
* Testador de cabo ethernet.
* Fonte de tensão regulável.
* [OpenPCD Passive RFID Project - OpenPCD](http://www.openpcd.org/).
* Arduíno Leonardo.
* Shields RPI:
  * RFID.
  * Bluetooth 4.0.
* Shields do Arduíno.
* PCB? CNC João de Barro?
* Cortadora Laser.
* RPI 3.
* [LinkIt ONE](https://www.seeedstudio.com/LinkIt-ONE-p-2017.html).
* Beagle Board Green (pra usar de programador standalone pra Gnuk e afins).
* Osciloscópio e gerador de sinais:
  * [Comedi - Control and Measurement Interface](http://www.comedi.org).
  * [xoscope for Linux](http://xoscope.sourceforge.net/) ([pacote](https://packages.debian.org/stable/xoscope)).
  * [BitScope Mini Model 10 | World's Smallest Mixed Signal PC Based USB Oscilloscope!](http://bitscope.com/product/BS10/).
  * [DIY: Turn your GNU/Linux computer into a free oscilloscope | Yann "Bug" Dubois](http://www.yann.com/en/diy-turn-your-gnulinux-computer-into-a-free-oscilloscope-29/09/2010.html).
* Ferramentas e instrumentos para ver melhor:
  * All-in-one:
    * [PSLab.io](https://pslab.io/): small USB powered hardware extension for your Android phone or PC that lets you measure all kinds of things.
    * [Espotek Labrador](https://espotek.com/labrador/) ([código](https://github.com/EspoTek/Labrado)): a USB device that transforms your PC or smartphone into a fully-featured electronics lab.
  * Telescópio.
  * Microscópio USB.
  * Binóculo.
  * Astrolábio.
  * Teodolito.
  * Sismógrafo.
  * Balança.
  * Paquímetro.
  * Micrômetro.
  * Réguas de cálculo.

## Dicas

* Ao desmontar algo, colar as peças numa folha de sulfite, fazendo anotação da
  ordem de desmontagem, posições, etc, como [nesta
  foto](https://geoff.greer.fm/photos/x62/IMG_1158.jpg), usando fita dupla face
  ou uma fita enrolada para colar as coisas; eventualmente tirar fotos durante
  o processo para que a desmontagem e a remontagem sejam mais determinísticas;
  uma dica do tipo se encontra em algum lugar do livro Zen e a Arte de
  Manutenção de Motocicletas.
