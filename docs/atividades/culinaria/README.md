# Culinária

{{ info.context.pt }}

{{ warning.needs_work.pt }}

## Cardápio prático

Conceito:

* Base.
* Guarnição.
* Mistura.
* Salada.
* Sobremesa.

Bases:

* Feijão.
* Lentilha.
* Grão de bico.

Guarnições:

* Arroz branco.
* Arroz integral.
* Couscous.

Misturas:

* Legumes cozidos.
* Omelete.
