# Procedimento padrão de limpeza ou reforma

{{ info.context.pt }}

{{ warning.needs_work.pt }}

* Para o serviço 30 minutos antes do expediente para limpeza do local e das ferramentas.
* Avisar com antecedência a necessidade de mais material.
* Manter o ambiente sempre organizado, isso economiza tempo, evita perdas e deixa o ambiente menos carregado.
* Não sujar.
* Não danificar coisas.
* Trabalhar com cuidado e sem pressa.
* Forrar tudo.

## Ciclo de trabalho

* No caso de reformas, isolar a área a ser trabalhada
* No primeiro dia, juntar o material, forrar e preparar o ambiente
* Tipos de sessão:
  - Curtas: realizar sessões com objetivos pontuais, para que seja possível realizá-las ao longo dos dias entre outras atividades.
  - Contínuas: força-tarefa concentradas num único local.

## Recomendações gerais

* Macacão
* Saco/lata de lixo
* Lona
* Vassoura e pá
* Panos
* Porta-ferramentas e peças
* Procurar manter as mãos limpas
* Antes de quebrar qualquer coisa, entre em contato e pergunte se pode
* Antes de cortar qualquer coisa, entre em contato e pergunte se pode
* Uso e economia de recursos (por exemplo água)
* Não tomar decisões importantes sem consultar os/as responsáveis pela casa

## Como forrar a área de trabalho

* Lonas presas com fita adesiva no chão
* Fazer um "tapete" contínuo com um rolo de saco de livo grande
* Usar uma bandeja para fazer cimento

## Recomendações para jardins

* Não usar o rastelo para varrer piso.
* Tirar folhas velhas?
* Podar plantas? Quais?

## Recomendações sobre o uso de ferramentas

* Boa conservação.
* Não manusear com as mãos sujas.
* Limpar após o uso.
* Não forçá-las.
* Não misturar as ferramentas da casa com as de terceiros.

## Orçamentos

* Nenhum trabalho pode começar sem a aprovação de um orçamento.
* Orçamentos devem ter prazo.
* Acertos podem ser feitos após o término dos serviços para compensar
  excesso de trabalho, mas estes não podem passar muito do orçamento
  acordado.

## Recomendações para louça

* Reusar louça que está secando.

## Contabilidade

Pastas de gastos:

* Já contabilidados.
* Ainda não contabilizados.

## Almoxarifado

* Empilhado.
* Enfileirado.
* Não esconder itens uns atrás dos outros.
* Estoque (pode conter duplicatas) versus armário de coisas em uso.
* Itens do mesmo tipo no mesmo lugar.
