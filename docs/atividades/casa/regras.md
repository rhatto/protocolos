# Regras Domésticas

* Regras genéricas de convívio a serem combinadas caso a caso.
* Elas podem ser orais ou escritas. Caso escritas, podem ser
  afixadas em locais específicos da casa, como por exemplo
  como lembretes em locais especiais (por exemplo sobre lavar
  a louça num local próximo à pia).

{{ info.context.pt }}

{{ warning.needs_work.pt }}

## Básico

* Trabalho é dividido para não haver exploração.
* Deixe as coisas num estado melhor do que as encontrou.
* Se quer limpar menos, suje menos!

## Contabilidade

Vários são os modos de operação possíveis:

### Esquema clássico

Contabilidade usual, com planilha de gastos: muito útil durante o período de
montagem do ambiente doméstico, onde tivemos vários gastos de material.

### Esquema prático

Esquema atual, mais prático uma vez que o ambiente doméstico já esteja montado:

- Só custo fixo entrando no racha, podendo ser definido um valor médio.

- Provisões e mantimentos a gente deixa rolar, cada um compra o que achar
  necessário e come o que quiser, mas sem rachar os custos.

Assim, uma das pessoas paga as contas e a(s) outr(o/as) só faria(m) um único depósito.
E só é preciso refazer essa conta se algum dos valores mudar.

Assim fica mais fácil, apesar de não ser mais exato.

### Esquema colaborativo

Usando uma aplicação como o [misery](https://packages.debian.org/stable/misery) ou o
[ihatemoney](https://ihatemoney.org/) ([código](https://ihatemoney.org/)), permitindo
que todos/as possam adicionar e editar gastos:

- Gastos fixos podem ser compartilhados normalmente: a pessoa que pagou o gasto indica
  para quem este gasto foi feito.

- Por padrão, gastos que não foram acordados previamente são arcados apenas pela pessoa
  que decidiu e gastou, porém quem quiser compartilhar/consumir o gasto pode se incluir
  no racha.

## Definições

* Sobre frituras.
* Andar de sapatos dentro de casa.
* Bagunça dentro e fora do quarto.
* Compartilhamento de material de higiene.
* Padrão de lavagem de panelas para aumentar suas conservação.
* Evitar abrir produtos que já possuem outras embalagens abertas.
* Manter ambiente limpo e organizado para evitar trabalho excessivo.
* Locais para ativos pessoais e coletivos (geladeira, armários, despensas, etc).

## Limpeza

Modos de limpeza:

* Mutirão coletivo periódico.
* Revezamento semanal periódico.
* Coisas largadas por aí estão sujeitas a serem repostas no lugar!

Louça:

* Lave o que sujou + 1.
* Secar bem a louça e guardá-la quando possível, evitando acúmulo.
* Deixar esponja seca e sem sabão.
* Deixar pia e ralo limpos para não entupir.

## Mantimentos

* Comprar periódicas em atacadões.
* Compras semanais de frutas, verduras e legumes.

## Social

* Aluguel temporário.
* Hospedagem solidária.
* [Couchsurfing](https://www.couchsurfing.com)?
