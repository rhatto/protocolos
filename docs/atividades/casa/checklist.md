# Checklist Doméstico Básico

{{ info.context.pt }}

{{ warning.needs_work.pt }}

## Pagamentos mensais

* Água.
* Luz.
* Gás.
* Internet.

## Organização

* Fósforos.
* Esterilizador (potabilizador) de água e filtro.
* Água potável e comida para um mês.
* Reserva de comida de último recurso com validade longa (enlatados, proteínas, etc).
* Rádio portável a pilha ou manivela.
* Kit médico.
* Lista de contatos impressa.
* Caixa de ferramentas.
* OpenHouse.
* Comissão aperiódica de reforma, renovação/rotação de estoques e descarte.
* Bota-fora.

## Básico pessoal

* Kit de higiene completo.
* Roupas de cama, toalhas de corpo e rosto.
* Rede de dormir.
* Tatames tradicionais japoneses (empilháveis) (para dormir/sentar) e travesseiros.
* Bancada (caveletes, tábua, mesa, tomadas, carregadores, luminárias).
* Sacola de roupa suja.
* Arara de roupas.
* Caixa pra festinhas.
* Caixa de roupas.
* Caixa de utensílios/acessórios, por exemplo:
  * Bomba de ar.
  * Kit de reparos de pneus.
  * Jogo de ferramentas.
  * Baterias para luz de bicicleta.
  * Miscelânea de informática (cabos, adaptadores, pendrives).
  * Sacolas, fita silver tape / black tape, papelaria.
  * Protetores auriculares.
  * Trocados.
* Caixa de equipamentos.
* Suporte para bicicleta.

## Referências

* [What are the essential items to stockpile in the face of impending nuclear disaster?](http://www.newstatesman.com/politics/uk/2017/08/what-are-essential-items-stockpile-face-impending-nuclear-disaster).
