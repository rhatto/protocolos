# Orfanato de Projetos

{{ info.context.pt }}

{{ warning.needs_work.pt }}

* Trata-se de um esquema em que alguém possui um portfolio de projetos e
  procura pessoas e times que queiram adotá-los.

* Uma rotina periódica faz com que o orfanato entre em contato com as pessoas
  para checar se os projetos que elas adotaram não foram abandonados.

## Requisitos

Para um projeto possa ser adotado, a pessoa ou grupo responsável terá de estar
de acordo com algumas coisas:

* O desenvolvimento precisa ser em software livre.
* O software precisa ser instanciável, ou seja, grupos e pessoas podem criar
  instalações deles onde quiserem.
* Gestão da instância principal coletiva e com abertura.
* Respeito à privacidade, preocupação com segurança e que estejam na linha dos
  [Princípios Éticos](https://templates.fluxo.info/etica/coletiva/).
* [Responsabilização](/coletivo/responsabilizacao/).

Além disso, o orfanato tenta, na medida do possível:

* Prestar consultoria sobre os projetos.
* Realizar checagens periódicas (pings semestrais, por exemplos) para saber se
  as pessoas ainda estão mantendo os projetos ou se é necessário procurar
  outras pessoas para adotá-los.
