# Administração do $servidor

{{ info.context.pt }}

{{ warning.needs_work.pt }}

Este processo estabelece os critérios de administração do `$servidor`,
doravante mencionado como `$servidor`, canalizador de fluxos ou simplesmente
como servidor.

# Classes RSP

O servidor deve estar configurado de acordo com as seguintes classes do
[Resource Sharing Protocol](https://rsp.fluxo.info):

* `$classe` - `$classe_versao`.

# Política de Administração

0. Criação de Usuários:
   a. Qualquer integrante do Coletivo pode ter uma conta no servidor, mas caso
      tenha deve zelar pela segurança da mesma e concordar com a presente política.
   b. A criação de usuários no servidor deve ser comunicada ao Coletivo e
      seguir eventuais procedimentos existentes.
   c. A senha da conta no servidor não pode ser compartilhada com outras contas
      e deve ser razoavelmente forte.
   d. Em caso de perda ou roubo de senha, o Grupo de Trabalho do servidor deve
      ser contatado o quanto antes.

1. Configuração: ao instalar ou efetuar qualquer tipo de configuração na máquina, procure:
   a. Adicionar, se possível e/ou necessário, a configuração em sistema de
      gestão servidores que o Coletivo utiliza.
   b. Documentar os procedimentos utilizados ou informe ao Grupo de Trabalho do servidor.

3. Comunicação: na medida do possível, comunique o Grupo de Trabalho do
   servidor sobre alterações feitas na sua configuração configuração.

# Quota

O Coletivo alocará para si, em princípio, um limite garantido de `$disco` de
espaço em disco no servidor. Toda a quantidade adicional de disco existente no
servidor pode ser disponibilizada para outros grupos afins, mediante processo
formal e sem garantias de backup.

Quotas de uso de banda, processamento e memória ficam a cargo do Grupo de
Trabalho do servidor ou de todo o Coletivo conforme necessidade ou mediante
requisição.

# Responsabilização

Cabe ao Grupo de Trabalho formado pelas pessoas responsáveis por este processo
seguir a política de administração e ainda:

* Zelar para que o servidor possua o nível de segurança, privacidade e
  estabilidade escolhidas.
* Cuidar para que o downtime do servidor não passe de `$downtime`.
* Efetuar as atualizações de softwares necessárias para o funcionamento básico
  do servidor.

A responsabilização neste processo não implica o compromisso com a
administração de todas as camadas, aplicações, configurações e dados que
existam ou possam existir no servidor, mas apenas com o funcionamento básico do
servidor.

# Dependências

A realização deste processo depende da realização dos seguintes processos:

* `$dependencia`.

# Sobre este texto

O texto deste processo foi redigido utilizando o [Template para Administração
de Servidor](/provedor/servidor). No caso de alterações que não dizem respeito
apenas ao Grupo e que possam enriquecer tal template, favor submetê-las também
upstream, isto é, ao texto do template.
