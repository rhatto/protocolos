# Entrega de backups solicitados

{{ info.context.pt }}

{{ warning.needs_work.pt }}

Processo que consiste na entrega de backups solicitados por grupos e pessoas
hospedadas na infra-estrutura do Coletivo. As tarefas envolvidas consistem em:

1. Obter as solicitações a backups e organizá-las numa tabela, mantendo assim o
   Coletivo informado sobre o andamento deste processo. Por possivelmente
   existirem backups de qualidades distintas, é preciso perguntar à parte
   solicitante, caso necessário, de qual backup os dados precisam ser entegues.
2. Obter, caso existente, o backup solicitado, realizando uma auditoria caso
   necessário.
3. Disponibilizar os backups apenas às pessoas responsáveis pelos ou donas dos
   dados ou informá-las caso o backup não exista. Informá-las também se o eventual
   backup foi ou não auditado e:
  a. Caso tenha sido auditado, disponibilizar, em linhas gerais, o procedimento utilizado.
  b. Caso não tenha sido auditado, informal qual risco isso representa.

## Auditoria do backup

Quando necessária, a auditoria básica consiste em:

1. Retirar qualquer código executável que possa ser substituído.
2. Marcar todo o código executável insubstituível como vulnerável, cuja
   auditoria é deve ser repassada para o grupo dono do código.
3. Destruição ou modificação de qualquer senha encontrada, mesmo que a mesma se
   encontre armazenada de modo cifrado.
4. Auditagem básica de conteúdo: verificação de datas de acesso e escrita a
   arquivos, passar anti-virus, etc.
5. Auditagem avançada de conteúdo, se possível.

Procedimentos mais refinados ficam à cargo da situação e do grupo de trabalho
de entrega de backups solicitados (composto pelas pessoas responsabilizadas
pelas tarefas acima mencionadas).

## Prazos

O prazo de entrega de backups é proporcional ao tamanho do mesmo e à
necessidade de auditoria:

* Backups de até 100MB: entrega em até 30 dias.
* Backups de até 1GB: entrega em até 60 dias.
* Backups maiores que 1GB: entrega em até 90 dias.

No caso de necessidade de auditoria, o prazo de entrega é duplicado.

## Template

    Conforme solicitado, o último backup disponível de $descricao, datado de $data
    e obtido $origem, já está disponível.

    Seguem os dados:

      - URL: https://backups.$dominio/$sitio
      - Conta: $sitio
      - Senha: $senha

    O https://backups.$dominio atualmente usa um certificado SSL
    auto-assinado, cuja impressão digital é

      $fingerprint

    Hashes dos arquivos disponibilizados:

      md5sum:  $hashes
      sha1sum: $hashes

    Auditoria realizada:

      - Busca e eventual remoção de código executável.
      - Mudança de senhas em banco de dados (senhas truncadas).
      - Busca por vírus.

    Tarefas não-realizadas porém recomendadas:

      - Checagem do conteúdo do banco de dados (para evitar calúnia ou
        desinformação)
      - Checagem do conteúdo dos arquivos.
      - IMPORTANTE: checagem de usuários para evitar inscrições de
        elementos estranhos.
      - Checagem da configurações da instância, caso ela seja reinstalada
        noutro local.
      - Limpeza do spam e desativação de todas as contas desconhecidas
        (principalmente relacionadas a spam).

    Observações:

      - Em princípio, não há prazo para a permanência de tal backup
        no local disponibilizado. No entanto, pode ser que ele precise
        ser apagado para liberar espaço ou nalgum procedimento de
        limpeza. Por isso, recomenda-se que sejam baixados o quanto antes.
