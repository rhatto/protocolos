# Hospedagem

{{ info.context.pt }}

{{ warning.needs_work.pt }}

Este processo define os procedimentos de hospedagem do Coletivo. A hospedagem
de conteúdo e/ou serviços constitui processo formal e é dividida em dois
níveis:

1. Grupo de trabalho de uma dada plataforma.
2. Grupo responsável por uma dada hospedagem.

## Necessidade de formalização

Como a hospedagem consiste numa atividade sensível, onde é importante dar
alguma garantia a quem se hospeda e da mesma maneira ter garantias contra
possíveis problemas que cada hospedagem pode causar, ambos os níveis devem ser
estabelecidos como processos formais. Além disso, como parte da implementação
deste processo, inclusive sítios já hospedados e plataformas existentes deverão
passar pela formalização.

A hospedagem está sujeita a comprometimentos (do Coletivo e da parte hospedada)
que satisfaçam a uma Política de Hospedagem do Coletivo.

## Grupo de uma plataforma

Ao grupo de trabalho de uma dada plataforma cabe cuidar do funcionamento,
manutenção e segurança de uma dada plataforma de hospedagem. Para cada
plataforma a ser oferecida hospedagem é preciso um grupo de trabalho
responsável.

A não existência de um grupo de trabalho de uma plataforma não proíbe a
existência de instalações da plataforma para uso interno do Coletivo, mas
impede que a hospedagem de terceiros (isto é, de grupos e indivíduos de fora do
Coletivo) seja realizada.

## Grupo de uma hospedagem

Ao grupo de trabalho responsável por uma dada hospedagem cabe cuidar da
hospedagem de um dado grupo/indivíduo. Para que seja possível hospedar um grupo
ou indivíduo numa dada plataforma, é necessário que haja um grupo de trabalho
em funcionamento para essa plataforma.

O processo formal para cada hospedagem deve conter as seguintes ações:

1. Apresentação do Coletivo (realizada durante a etapa "discussão" do processo)
   através do envio de sua Carta de Hospedagem.
2. Apresentação do grupo ou indivíduo a ser hospedado realizada durante a etapa
   "discussão" do processo).
3. Decisão:
   * No caso de aprovação da hospedagem pelo Coletivo e após o processo sido
     responsabilizado:
       1. A(s) pessoas(s) responsável(is) pela hospedagem deve(m) enviar o
          Termo de Comprometimento de Hospedagem e a Política de Hospedagem do Coletivo.
       2. O coletivo ou indivíduo a ser hospedado deve concordar com o Termo de
          Comprometimento de Hospedagem, caso contrário a hospedagem não pode ser
          realizada.
   * No caso de não-aprovação pelo Coletivo ou falta de responsabilização, o
     grupo ou indivíduo que seria hospedado deve ser informado da decisão
     juntamente com o motivo, se possível.

## Responsabilização

Cabe ao grupo de trabalho de uma hospedagem:

1. Aplicar a Política de Hospedagem do Coletivo junto à parte hospedada.
2. Manter a comunicação entre o Coletivo e a parte hospedada.
