# Configuração de sistemas padronizada e centralizada

{{ info.context.pt }}

{{ warning.needs_work.pt }}

O presente processo estabelece o funcionamento de uma configuração de sistemas
padronizada e centralizada. Levando em conta que o Coletivo pode lidar com
muitas camadas de canalização informacional, cada um com diversos serviços
configurados, backups locais e remotos e outras especificidades, torna-se
interessante desenvolver um esquema de configuração centralizada, de forma a
tornar fácil a manutenção, replicação e substituição dos ambientes assim como o
compartilhamento de configurações com outros grupos.

## Divisão de configuração e compartilhamento

A configuração dos sistemas é dividida da seguinte forma:

1. Especificação de camadas via [Resource Sharing Protocol
   (RSP)](https://rsp.fluxo.info) de acordo com as necessidades e possibilidades
   do Coletivo.
2. Configuração efetiva dos sistemas através de aplicação especializada.

## Compartilhamento de configurações

No que concerne ao compartilhamento das configurações, a seguinte divisão é
utilizada:

1. Repositório privado, de acesso restrito ao Coletivo e contendo informações e
   configurações cuja publicização é prejudicial ou desnecessária do Coletivo.
2. Repositório público de configurações, denominado de
   [http://padrao.sarava.org Padrão Saravá], contendo as configurações cuja
   publicização auxilia no intercâmbio com outros grupos.

Ambos os repositórios devem utilizar controle de versão e o Coletivo ainda é
encorajado a utilizar configurações disponibilizadas por outros grupos, unindo
assim esforços para a economizar trabalho.

## Implementação

A configuração efetiva deve ser obtida através do uso de um sistema como o
[Puppet](http://puppetlabs.com), por ter diversos módulos disponíveis, uma
linguagem de configuração bastante flexível e uma comunidade próxima que já o
utiliza.

As seguintes características de implementação devem ser satisfeitas:

1. O repositório e o servidor `puppetmaster` devem rodar a partir de uma
   instância cuja configuração de camadas é a mais segura do Coletivo e os dados
   devem estar disponíveis somente via conexão segura.
2. O repositório '''deve''' ter backups em diversos locais.
3. O controle de versão utilizado para os módulos e demais configurações do `puppet` é o `git`.
4. O `puppet` deve estar rodando no maior número possível de camadas do
   Coletivo e obtendo suas configurações do `puppetmaster`.

## Responsabilização

É tarefa do Grupo de Trabalho de Configurações, composto pelas pessoas
responsáveis pelo presente processo:

1. Manter o máximo possível de configurações de sistemas do Coletivo segundo os
   critérios estabelecidos no presente processo.
2. Realizar auditoriais periódicas (com base anual) seguida relatório e
   atualização da configuração dos sistemas conforme necessário.
3. Manter um [padrão de configuração](https://padrao.fluxo.info) atualizado e
   em funcionamento.
4. Alterar, na medida do possível, a configuração dos sistemas e documentações
   relacionadas conforme solicitações do Coletivo.
