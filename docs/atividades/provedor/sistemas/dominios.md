# Gerenciamento de domínios

{{ info.context.pt }}

{{ warning.needs_work.pt }}

Os domínios utilizados pelo Coletivo constituem seu ''namespace'' no qual podem
definir endereços de acesso público e privado. Sendo indispensáveis para a
autonomia básica do Coletivo e para a possibilidade de hospedagem de outros
grupos, o gerenciamento de domínios constitui um processo de garantia dessa
autonomia, especialmente se levado em conta que o DNS é um sistema
centralizado, pouco transparente, anti-democrático e de tendência
centralizante.

O presente processo estabelece um grupo de trabalho para o gerenciamento de
domínios, cujas tarefas envolvem:

1. Renovação dos domínios com '''antecedência''' ao prazo de vencimento da
   mesma, solicitando ao grupo de contabilidade os gastos necessários para cumprir
   tal tarefa.
2. Aplicação de política de privacidade e segurança à configuração dos
   domínios, incluindo auditoria periódica para a verificação dessas
   configurações.
3. Registro de novos domínios conforme a necessidade e formalização pelo Coletivo.
4. Manter o Coletivo informado sobre essas tarefas.

## Responsabilização

As pessoas responsabilizadas por esse grupo de trabalho precisam ter condições
de realizar operações financeiras internacionais com cartão de crédito.

## Domínios do Coletivo

Os domínios do Coletivo são:

* domínio1
* domínio2

Vale observar que:

1. Outros domínios podem ser adicionados na lista mediante procedimento formal
   para a alteração da mesma.
2. Por esse processo fica automaticamente aprovado o gasto de recursos
   financeiros do Coletivo para o pagamento da renovação/registro dos domínios do
   Coletivo.
