# Administração de configurações de DNS dos domínios do Coletivo

{{ info.context.pt }}

{{ warning.needs_work.pt }}

Este processo estabelece as linhas gerais para a administração de configurações
DNS dos domínios do Coletivo.

## Tarefas

Cabe ao Grupo de Trabalho formado pelas pessoas responsáveis pelo presente
processo:

1. Manter a configuração de DNS dos domínios do observando os critérios de
   segurança cabíveis.
2. Atender as requisições do Coletivo de mudanças de configuração de DNS.

## Dependências

A realização deste processo depende da realização dos seguintes processos:

* [Gerenciamento de domínios](/provedor/sistemas/dominios).
