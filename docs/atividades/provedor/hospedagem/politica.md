# Política de Hospedagem do Coletivo

{{ info.context.pt }}

{{ warning.needs_work.pt }}

    Política de Hospedagem do Grupo
    -------------------------------

    1. O Coletivo reserva para si o poder de hospedar ou deixar de hospedar
       qualquer grupo ou indivíduo a partir dos principios e critérios éticos,
       politicos e práticos do Coletivo.

    2. No caso de deixar de hospedar um grupo ou indivíduo, o Coletivo se
       compromete a avisar a parte hospedada com antecedência e disponibilizar os
       arquivos envolvidos na hospedagem.

    3. Grupos e indivíduos só são hospedados pelo Coletivo se mostrarem-se
       dispostos a uma relação recíproca de troca de conhecimento e atividades.

    4. Criada a parceria o Coletivo deve ser informado das ações e rumos que cada
       projeto toma caso afetem o Coletivo, assim como o Coletivo se compromete a
       informar a parte hospedada de qualquer venha a atingi-la.

    5. Os termos da parceria devem estar claros no sentido de um comprometimento de
       ambos os grupos no suporte, manutenção, e desenvolvimento dos sitios e afins
       que venham a ser criados por conta da hospedagem.

    6. A parte hospedada se responsabiliza pelo conteúdo do sitio, não cabendo ao
       Coletivo sofrer as consequências jurídicas de conteúdo impróprio ou ilegal. No
       entanto, o Coletivo fará o máximo possível para proteger a identidade e a
       privacidade da parte hospedada.

    7. As relações devem se dar de maneira mais transparente possível, não
       existindo informação encoberta ou deturpada por ambas as partes.

    8. As relações tem como finalidade a solidariedade no conhecimento, a
       complementariedade nas ações e nas trocas entre os grupos, devendo ser
       imprescíndivel maneiras de ensino-aprendizagem mútuos e de políticas que unam
       os grupos em prol de uma mudança sócio-política.

    9. Coletivo se compromete a informar a parte hospedada ao menos em linhas
       gerais sobre os critérios e políticas de privacidade e segurança das
       plataformas de hospedagem utilizadas pela parte hospedada.

    10. Hospedagens de movimentos sociais com atividades sensíveis no país são
       encaminhadas, quando possível, a plataformas hospedadas no estrangeiro.

    11. A hospedagem consiste em cooperação e não prestação de serviços.

[Versão inglesa](/english/hosting/policy).
