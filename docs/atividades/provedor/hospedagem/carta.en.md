# Hosting Letter

{{ info.context.en }}

{{ warning.needs_work.en }}

    $collective Hosting Letter
    --------------------------
    
    $collective is part of an intersection of various groups that discuss politics and
    technology in different ways. We work with internet servers turned to various
    cooperative goals. Then, our ideia is to colaborate with groups/projects that
    are part of mutual and multiple aid and support.
    
    $collective is an autonomous project kept by a collective of volunteers. One of our
    main goals is the collective creation of public spaces, common areas with projects and
    groups that intend to enforce and straighten coexistence.
    
    Our intention is not just offer a "hosting service" and then we're not available
    for groups that just want such thing. We want that groups we host colaborate with
    the construction of a neighborhood, a rhizome, meaning that technology doesn't have to be
    a barrier, but exactly the opposite. As technology is also a social construction,
    its purposes, its configurations and the processes it interferes with can't
    impose themselves independently of the choices made by the social groups where it's used.
    
    The internet is not only an environment of cooperation, but also of apropriation and
    exploitation of common goods. We understand that it just turns essentially into a
    public space when people can control their production and access means, things that
    don't happen on corporate and governmental spaces. Because of this we try to
    create public spaces, non-corporative and non-governmental and we hope that
    groups we host colaborate to the construction of such spaces.
    
    During the construction of these kind of spaces, we make discussions where we try to
    discover themes such as culture, society, technology, activism, social change
    and many others. Such a research, when possible, is shared pubicly.
    
    We research the political implications of technology and develop systems and
    instruments using different political values. We also keep a political dialogue
    in the logics of theory/practice.
    
    So, one of our proposals is to collectivelly establish a network of mutual
    support among groups and individuals interested in sharing the same physical
    structure to share their knowledges, activities and researches.
    
    We attempt then to break the relation between service provider/clients,
    because we're not service providers and the groups/individuals we host are not
    our clients.  We both make part of the same collaboration network where diverse
    interests converge towards mutual empowerment. And who knows if such way of
    organizing things won't spread to other parts of society.
    
    Thinking about knowledge distribution and with an incentive to new groups
    interested in keeping their own server infrastructure, we try to share the most of
    our organization scheme.
    
    And remember: for us, $collective is not just a hosting system or a service provider.
    
    Interesting links:
    
     - Researches available: http://wiki.$domain
     - Configurations and operational procedures: http://padrao.$domain
    
    In solidarity,
    $collective Group
