# Termo de Comprometimento de Hospedagem

{{ info.context.pt }}

{{ warning.needs_work.pt }}

    Olá :)

    Termo de Comprometimento de Hospedagem
    --------------------------------------

    Discutimos e estamos de acordo em oferecer hospedagem conforme sua requisição.
    Agora, para que possamos efetivamente manter a hospedagem, é preciso:

      1. Apresentarmos qual é o nosso comprometimento com relação a essa hospedagem.
      2. Que você e/ou seu grupo concordem com o presente termo de compromisso e com
         a nossa Política de Hospedagem.

    Estes são os termos de compromisso mútuo, onde explicitamos qual será nosso
    comprometimento com a hospedagem em questão pelo qual a parte hospedada precisa
    concordar para que seja possível manter tal relação.

    Caso você concorde com os termos desta carta e aceite nossa Política de
    Hospedagem, basta responder afirmativamente que sua hospedagem será iniciada
    logo que possível. :)

    Nosso comprometimento
    ---------------------

    Por essa hospedagem, nos comprometemos a manter a hospedagem em funcionamento.
    A manutenção da hospedagem ocorre através de trabalho voluntário e responsável.

    A infra-estrutura utilizada para hospedagem é estável, porém sujeita
    a eventuais intempéries da rede ou mesmo a problemas mais graves.

    Por isso, a hospedagem pode ficar fora do ar de vez em quando. Estamos sempre
    atentos e prezamos pela segurança e integridade dos dados hospedados. Porém,
    por diversos motivos, não podemos garantir a total disponibilidade ou mesmo a
    eternidade destes dados.

    Nos comprometemos, na medida do possível, a manter cópias de segurança dos
    dados contidos em nossa infra-estrutura. No entanto, pedimos a você que
    mantenha, também na medida do possível, cópias de seus arquivos.

    Nosso grupo é formado por pessoas que desempenham uma série de tarefas.  As
    atividades mais importantes e cruciais, como a hospedagem, dependem de várias
    tarefas e cada uma delas está associada a um grupo de trabalho de pessoas
    responsáveis pela sua realização.

    Mesmo assim, pessoas podem deixar de trabalhar num dado grupo de trabalho e
    eventualmente alguma tarefa não possa mais ser realizada por falta de um mínimo
    de pessoas responsáveis por ela.

    É nesse sentido que garantimos o funcionamento e a realização da hospedagem: de
    acordo com a força de trabalho disponível no nosso grupo. Por isso, é possível
    que, no futuro, aconteça de termos que encerrar a hospedagem numa dada
    plataforma.  Mas, se o fizermos, garantimos que não será da noite para o dia e
    daremos tempo suficiente para que você e seu projeto possam migrar seus dados
    para outro local.

    Nos comprometemos também a informar, mediante solicitação, nossos procedimentos
    de acordo com nossa política de transparência. Tais procedimentos variam desde
    características de privacidade e segurança das plataformas utilizadas como
    também da situação do nosso coletivo.

    Como nossos procedimentos podem variar ao longo do tempo, convém a você nos
    solicitar as descrições de procedimentos conforme julgar necessário.

    Temos nossas limitações mas na medida do possível manteremos as coisas
    funcionando. :)

    Seu comprometimento
    -------------------

    Esperamos de você e do seu projeto que use o recurso oferecido com sabedoria.
    Não peça sítios e ferramentas que você deixará abandonadas e, caso você instale
    seu próprio programa, tenha a responsabilidade de deixá-lo atualizado, uma vez
    que brechas de segurança no seu espaço podem comprometer outros projetos
    hospedados.

    Não se esqueça que a manutenção de um sistema de múltiplos projetos é bastante
    trabalhosa e dificil de manter segura e por isso pedimos a colaboração de todo
    mundo. Se puder nos comunicar quando for instalar algum software no seu espaço,
    ficaríamos muito agradecidos/as :)

    Interfaces de comunicação e compartilhamento
    --------------------------------------------

    Um possível espaço de interação entre os projetos é a Lista da Vizinhança[1]
    (inscrição apenas para emails seguros, entre em contato para detalhes).
    Cultive-a! Ao hospedarmos grupos e indivíduos, torcemos para que estes também
    tornem-se responsáveis por zelar por tais espaços de convivência.

    Nossa vizinhança é também um local de articulação de rede, onde todos os grupos
    e indivíduos que compartilham da estrutura por nós mantida se comunicam.

    Se os temas e estudos com os quais nos preocupamos também os/as inspiram, nós
    os/as convidamos a participar dessas discussões. Para evitar a centralização
    das discussões, propomos que vocês os discutam coletivamente, em seus projetos
    e/ou grupos, tornando público os processos e resultados de tais discussões.
    Para nós é fundamental tentar construir uma metodologia que possibilite a
    disseminação pública de tais discussões.

    [1] $endereco_da_lista_da_vizinhanca

    Em solidariedade,
    Coletivo $grupo

[Versão inglesa](/english/hosting/terms).
