# Checklist para Bases de Dados

{{ info.context.pt }}

{{ warning.needs_work.pt }}

* Fundamental:
  * Backups remotos automatizados.
  * Acesso web à plataforma apenas via conexão mais segura (https).
  * Adequação à legislação (LGPD, Marco Civil etc).
  * Minimizar quantidade de dados pessoais coletados e exibidos ao estritamente necessário.
  * Acesso restrito para informações pessoais.
* Recomendado:
  * Não utilizar "nuvem" corporativa: usar servidor próprio, pois evita o acesso por terceirizados.
  * Protocolo (acordo comum) sobre o uso da plataforma pelos pesquisadores(as), incluindo aspectos sobre privacidade.
  * Treinamento básico de segurança e privacidade para administradores(as) e alimentadores(as) do sistema.
  * Definição sobre licenciamento de dados (direitos autorais) e política de dados abertos.
* Opcional:
  * Armazenamento criptografado.
  * Backups offline.
