# Hosting Terms

{{ info.context.en }}

{{ warning.needs_work.en }}

    Hi :)

    Agreement on Hosting
    --------------------

    We discussed and we agree to offer hosting as you requested. Now, in order for we to
    effectivelly maintain this hosting, we need to:

      1. State what is our commitment  in relation to this hosting.
      2. That you and/or your group agree with the present term of agreement and
         with our Hosting Policy.

    These are mutual commitment agreements, where we explain what is our commitment
    with the hosting and that the hosted part needs to agree so we can keep such
    relation.

    In case you agree with the terms of this letter and accept our Hosting Policy,
    just reply saying so and the hosting will start as soon as possible. :)

    Our commitment
    --------------

    We commit ourselves to keep the hosting working. The hosting maintenance
    happens through volunteer and responsible work.

    The used infra-structure is stable, but subject to eventual power and network
    outages or even more sensitive problems.

    Then, sometimes hosting can go offline. We will always be allert and we care
    for security and integrity of the hosted data. But, for a lot of reasons, we
    cannot garantee the total availability or even the eternity of such data.

    We commit ourselves as much as possible to keep security copies of the data we
    host. But we ask for you to also arrange, as much as possible, backup copies of
    your data.

    Our group is composed by people that do a lot of tasks. The most important and
    sensitive tasks, such as hosting, are dependent to other taks and each one is
    associated to a workgroup of responsible persons.

    It may happen that someone leaves a workgroup and eventually some tasks will lack
    commited people to get them accomplished. It's on this sense that we garantee
    hosting: according to the available workforce at our group. It's possible that,
    in the future, something happens and we stop hosting in a given platform.
    But, if we do so, we garantee that it won't happen from night to day and we'll
    give enough time for you and your project to migrate the data somewhere else.

    We also have the commitment to inform, when you ask, our procedures according to
    our accounting policy. Such procedures vary from privacy and security policies
    to choices of platforms and our group's situation.

    As our procedures can change with time, we recommend you to ask for procedures
    descriptions when you feel necessary.

    We have our limitations but we'll keep things working as much as we can. :)

    Your commitment
    ---------------

    We hope that you and/or your project use the offered resource with wisdom.
    Don't ask for websites and tools if you will abandon them. If you install your own
    programs, please have the responsibility to keep it up-to-date as security holes at
    your place can compromise other hosted projects.

    Please don't forget that the maintenance of multiple projects needs a lot of work
    and is difficult to keep secure. Then we ask for everyone's collaboration. We would be
    very pleased if you informed us about your plans to install tools in your space. :)

    Sharing interfaces
    ------------------

    A possible space for interactions among projects is the Neighborhood Mailing
    List[1] (subscription just for sufficiently secure mail accounts, get in touch for
    more information). When we host groups and individuals, we hope that they will also
    be responsible in what comes to taking care of such spaces.

    Our neighborhood is also a place for articulations, where all groups and
    individuals sharing the same infra-structure can communicate between
    themselves.

    If the subjects we research are inspiring for you too, we invite you to
    participate in such discussions. To avoid centralization, we propose you to
    collectivelly discuss with your group and publish the conclusions you
    get. For us it is fundamental to try to build a metodology that makes possible the
    dissemination of such discussions.

    [1] $neighborhood_mailing_list_address

    In solidarity,
    $grupo Collective
