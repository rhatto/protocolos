# Grupo de Trabalho de Hospedagem em $plataforma

{{ info.context.pt }}

{{ warning.needs_work.pt }}

O presente processo estabelece o funcionamento do Grupo de Trabalho de
Hospedagem em `$plataforma` (doravante mencionado apenas como
'''plataforma'''), consistindo em:

1. Manter instalações atualizadas e em funcionamento da plataforma.
2. Acompanhar avisos de segurança e atualizações dos aplicativos necessários
   para o funcionamento da plataforma.
3. Caso não seja realizado automaticamente, efetuar atualizações de segurança
   em no máximo '''uma semana''' (incluindo finais de semana e feriados) após as
   mesmas serem disponibilizadas.
4. Observar e aplicar os critérios de segurança e privacidade existentes para
   os/as usuários da plataforma.
5. Caso possível, atender a pedidos do Coletivo pela instalação adicional da
   plataforma em locais distintos em virtude de aspectos legais e políticos.
6. Disponibilizar ao Coletivo informações relacionadas aos procedimentos
   utilizados pelo grupo de trabalho, manutenções e atualizações que foram ou
   serão efetuadas.

# Responsabilização

É de responsabilização do grupo de trabalho a realização das tarefas
anteriormente mencionadas. Não é de responsabilidade do presente grupo de
trabalho:

1. Manter ou entrar em contato com grupos hospedados.
2. Prestar suporte aos grupos hospedados.

Não é de responsabilidade do presente grupo de trabalho:

1. Pelo funcionamento de cada instância da plataforma.

Em outras palavras, o presente grupo de trabalho não se responsabiliza pelo uso
de cada instância da plataforma, mas sim pelo funcionamento, atualização e
auditoria das instalações globais da plataforma, isto é, o grupo de trabalho
não lida com instâncias específicas da plataforma mas sim com a infra-estrutura
da mesma.

# Sobre este texto

O texto deste processo foi redigido utilizando o [Template para Grupo de
Trabalho de Hospedagem](/organizacao/misc/plataforma). No caso de alterações
que não dizem respeito apenas ao Grupo e que possam enriquecer tal template,
favor submetê-las também upstream, isto é, ao texto do template.
