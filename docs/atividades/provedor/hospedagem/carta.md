# Carta de Hospedagem

{{ info.context.pt }}

{{ warning.needs_work.pt }}

A Carta de Hospedagem não apenas estabelece as intenções e princípios que o
Coletivo adota para a hospedagem quanto pode ser utilizada como apresentação
aos grupos e indivíduos canditatos a hospedagem.

    Carta de Hospedagem do $coletivo
    --------------------------------

    O $coletivo é parte de uma intersecção de vários grupos que discutem política e
    tecnologia de diferentes formas. Partindo disso, trabalhamos com servidores de
    internet, voltados para distintas finalidades de cooperação. Sendo assim, nossa
    idéia é colaborar com grupos/projetos que participem de experiências de apoio
    mútuo e múltiplo.

    O $coletivo é um projeto autônomo, mantido por um coletivo de voluntários e
    voluntárias. Um dos nossos principais objetivos é a construção coletiva de
    espaços públicos, comuns entre diversos projetos e grupos que tenham a intenção
    de fortalecer e estreitar sua convivência.

    Nossa intenção não é oferecer um "serviço de hospedagem", por isso não estamos
    dispostos/as a nos aproximar de grupos que busquem este tipo de serviço.
    Queremos que os grupos  por nós hospedados colaborem com a construção de uma
    vizinhança, um rizoma. De tal maneira que a técnica e a tecnologia não sejam
    impedimento para isso, muito pelo contrário.  Sendo a tecnologia também uma
    construção social, seus propósitos, sua configuração e os processos nos quais
    ela interfere não podem prescindir dos desígnios dos grupos sociais onde ela é
    manipulada.

    A internet é um ambiente de cooperação, mas também de apropriação e exploração
    de bens públicos.  Nós entendemos que ela só se torna essencialmente um espaço
    público na medida em que as pessoas possam controlar seus meios de produção e
    de acesso, o que não ocorre em espaços corporativos ou governamentais. Por isso
    buscamos a criação de espaços públicos, não corporativos e não estatais, e
    esperamos que os grupos por nós hospedados colaborem com a construção desses
    espaços.

    Durante a construção de tais espaços, realizamos discussões nas quais tentamos
    desvendar temas como cultura, sociedade, tecnologia, ativismo, mudanças sociais
    entre outros. Tais estudos, quando possível, são disponibilizados publicamente.

    Estudamos as implicações políticas da técnica, desenvolvemos sistemas e
    instrumentos a partir de outros valores políticos, além de dialogarmos
    politicamente dentro da lógica cíclica da teoria/prática.

    Por isso, uma de nossas propostas é o estabelecimento coletivo de uma rede de
    apoio mútuo entre os grupos e indivíduos interessados em partilhar de uma mesma
    estrutura para compartilhar seus conhecimentos, atividades desenvolvidas e
    estudos realizados, de forma a integrar ativamente esta rede.

    Buscamos, portanto, quebrar com a relação prestador de serviço/cliente, pois
    não somos prestadores/as de serviço e nem os grupos/indivíduos por nós
    hospedados são nossos clientes, ambos fazemos parte de uma mesma rede de
    colaboração onde interesses diversos convergem para o fortalecimento dessa
    rede, e quem sabe para que esta forma de organização extrapole a própria rede e
    contamine assim as demais esferas que compõe a sociedade.

    Pensando na distribuição do conhecimento, e como incentivo a novos grupos
    interessados em manter sua própria plataforma de servidores, buscamos divulgar
    o máximo da sistematização de organização.

    E lembre-se: para nós, @ $coletivo não é apenas um sistema de hospedagem ou um
    mero provedor de serviços.

    Links de interesse:

     - Estudos disponibilizados: http://wiki.$dominio
     - Configurações e procedimentos operacionais: http://padrao.$dominio

    Atenciosamente,
    Coletivo $coletivo

[Versão inglesa](/english/hosting/letter).
