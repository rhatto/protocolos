# Refusal hosting letter

{{ info.context.en }}

{{ warning.needs_work.en }}

    Hi $requisitante,

    Unfortunatelly we can't host the project you requested because:

      - We think it doesn't match the etical principles we adopt[1].

      - And/or we consider it matches the kind of projects we have a critical
        position with[2].

    We believe that there's not just one way to fight. We also don't believe that
    our etical principles and our criticisms represent the "right" viewpoint.
    Our viewpoint just reflect the conclusions we got after a lot of discussions.
    We try just to be coherent with what we believe and that's the reason why we
    can't accept your request.

    We don't want for this statement to mean that we want to give no value for
    the thing your project do or about what do you believe.

    [1] See http://encontro.fluxo.info/Principal/ConjuntoDePrincipiosEticos
    [2] See http://wiki.$dominio
