# Hosting Policy

{{ info.context.en }}

{{ warning.needs_work.en }}

    $collective Group Hosting Policy
    --------------------------------
    
    1. The Collective reserves to istelf the power to host or discontinue the
       hosting of any group or individual according to the ethical, political and
       practical principles of the Collective.
    
    2. In the case of hosting discontinuation for a given group, the Collective
       commits itself to inform the hosted part with sufficient time and to make
       available the hosted data.
    
    3. Groups and individuals are just hosted by the Collective if they want to
       have a mutual relation of resources and activity sharing.
    
    4. Once the hosting partnership is created, the Collective must be informed of
       actions and ways the hosted part is taking if those affects the Collective. By
       the other hand, the Collective commits to inform the hosted part of anything
       that can affect it.
    
    5. The terms of partnership must be clear in the sense of the commitment
       between both sides on support, maintenance and development of the host and
       sites.
    
    6. The hosted part is responsible for the hosted content and the Collective
       cannot suffer legal consequences of improper or illegal content. Anyway the
       Collective will make everything possible to protect the identity and privacy of
       the hosted part.
    
    7. The relations should be established as transparently as possible, avoiding
       hidden or manipulated information by both parts.
    
    8. The relations have the purpose of solidarity on knowledge, complementarity
       on actions and exchange among groups, being vital to achieve ways of mutual
       teaching-learning and develop policies that unite groups towards social and
       political changes.
    
    9. The Collective commits itself to inform at least in general ways its
       security and privacy policies on the hosting platforms.
    
    10. Hosting of social movements with sensitive content are kept, when possible,
        hosted on platforms abroad.
    
    11. The hosting consists in cooperation and not in service providing.
