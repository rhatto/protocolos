# Modelo de carta de recusa de hospedagem

{{ info.context.pt }}

{{ warning.needs_work.pt }}

Este é um modelo de carta para recusa de hospedagem, no caso de projetos que
não concordamos. Para evitar mal entendidos, é importante termos um carta
ponderada e bem esclarecedora.

    Olá $requisitante,

    Infelizmente não poderemos hospedar o projeto que você requisitou porque:

      - Consideramos que ele não se enquadra nos princípios éticos que adotamos[1].

      - E/ou então consideramos que ele se enquadra nos tipos de projetos com os
        quais mantemos uma postura crítica[2].

    Acreditamos que não exista uma única forma de luta e muito menos que a nossos
    princípios éticos e as nossas críticas representem o ponto de vista "correto".
    Nosso ponto de vista apenas representa as conclusões que chegamos após muita
    discussão. Tentamos apenas nos manter coerentes com o que acreditamos e é por
    isso que não podemos efetuar a sua requisição.

    Não queremos de modo algum que isso signifique que estamos desmerecendo a
    atuação do seu projeto ou as coisas que você acredita.

    [1] Vide http://encontro.sarava.org/Principal/ConjuntoDePrincipiosEticos
    [2] Vide http://wiki.$dominio

[Versão inglesa](/english/hosting/refusal).
