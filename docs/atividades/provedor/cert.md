# Gestão de chave e certificado SSL

{{ info.context.pt }}

{{ warning.needs_work.pt }}

O presente processo trata da gestão de chave e certificado SSL para conexões
ditas seguras entre servidores do Coletivo.

## Considerações

O Coletivo reconhece que

1. O protocolo HTTPS possui sérios problemas de design, impedindo por exemplo
   que diferentes certificados possam ser utilizados num mesmo IP.

2. A indústria da certificação digital representa um sério risco de segurança e
   uma [imposição tecnocrata](http://lair.fifthhorseman.net/~dkg/tls-centralization/) à
   utilização prática do HTTPS. Ela recria um domínio cartorial no cyberespaço e
   pode a qualquer momento [ser utilizada por governos ou corporações para forjar
   certificados autenticados](http://web.monkeysphere.info/news/internet_secret_backdoor/) e
   com isso [grampear](https://secure.wikimedia.org/wikipedia/en/wiki/Man-in-the-middle_attack)
   a conexão entre usuários/as e computadores.

3. Idealmente, a atitude a ser tomada por um coletivo técnico radical deveria
de não utilizar a indústria da certificação como meio de assegurar a
identificação de seus serviços e máquinas. Deveria, ao invés disso, utilizar
apenas esquemas abertos como
    a. Certificadores Comunitários como o [CACert](http://cacert.org).
    b. [Monkeysphere](http://web.monkeysphere.info).
    c. A simples verificação da impressão digital dos certificados mediante
       algum vínculo direto (por exemplo, troca de fingerprints ou validação via
       OpenPGP) com os/as administradores das máquinas.

4. No entanto
    a. Nem todos os navegadores acompanham, por padrão, certificados de
       entidades comunitárias. É o
       [caso do CACert](https://secure.wikimedia.org/wikipedia/en/wiki/Cacert#Inclusion_status).
    b. A adoção de ferramentas livres para HTTPS ainda está muito longe de se
       tornar comum. No presente, haveria pouca possibilidade de que uma metodologia
       livre seja eficiente. Ao invés disso, haveria um involuntário incentivo aos/às
       usuários para que aceitem certificados considerados como inválidos pelos
       navegadores, o que pode facilitar ainda mais o grampo: se os usuário aceitam
       qualquer certificado, então não haveria diferença entre eles aceitarem o
       certificado verdadeiro do falso.

## Conclusões

Portanto, o Coletivo conclui que

1. No momento, ainda importante ter um certificado assinado pela indústria da
   certificação, isto é, por uma entidade autorizada pelo cartel do SSL.
3. A autoridade certificadora a ser utilizada deve obedecer alguns critérios básicos:
  a. Não é diretamente conectada a uma agência governamental.
  b. Não possui problemas políticos óbvios.
  c. Cujo certificado raíz não está encadeado com certificados que não satisfazem os critérios acima.
2. Dada a sua insuficiência, tal certificação corporativa não deve ser o único
   meio para a validação dos certificados SSL e assim esquemas alternativos como o
   [Monkeysphere](http://web.monkeysphere.info) devem ser utilizados e
   encorajados, de modo que haja um aumento da massa crítica necessária para
   tornar tais métodos mais populares. O mesmo se aplica para a verificação da
   impressão digital dos certificados.

## Utilização de HTTPS

Conforme o documento [Best Practices for Online Service
Providers](https://www.eff.org/wp/osp), o Coletivo concorda que conexões HTTPS
devem ser utilizadas o máximo possível.

1. O Coletivo utilizará HTTPS apenas em servidores confiáveis que onde seja
   possível armazenar as chaves SSL de forma criptografada.
2. Utilizar, quando possível, cabeçalhos
   [HSTS](http://www.debian-administration.org/article/Enabling_HTTP_Strict_Transport_Security_on_debian_servers).
2. Considerando que o certificado vale apenas para um único domínio e seus
   subdomínios (isto é, o domínio principal do Coletivo), o Coletivo utilizará
   HTTPS por padrão apenas:
     a. Para serviços, sistemas e sítios que utilizem o domínio principal do
        Coletivo, quando não houver impedimento técnico para tal.
     b. Para outros domínios desde que solicitado pela parte hospedada.
3. Considerar a utilização de [entradas CAA no DNS](https://links.fluxo.info/tags/caa).

## Implementação

A implementação de HTTPS também deve obedecer a uma suite bem estabelecida.

Tal escolha deve desabilitar uma série de cifras ruins e habilitar aquelas que
provém [Perfect Forward Secrecy
(PFS)](https://secure.wikimedia.org/wikipedia/en/wiki/Perfect_forward_secrecy).

## Outros protocolos

A utilização do SSL também deve se estender a outras plataformas, como por
exemplo email (via TLS) desde que possível e que atenda critérios análogos aos
anteriores.

## Escolha de autoridade certificadora

Dentre as autoridades certificadoras, o Coletivo escolhe a [Gandi](https://gandi.net) por:

1. Atender os critérios acima estabelecidos.
2. Diferentemente de outras empresas, possui uma preocupação com privacidade e
   comprometimento com serviços de internet alternativos e independentes.
2. [Contribuir com projetos de código aberto](https://secure.wikimedia.org/wikipedia/en/wiki/Gandi#Gandi_supports ),
   vide [lista](http://en.gandi.net/supports/).

No entanto, é importante notar as limitações envolvidas na escolha do Gandi:

    Q: Por que muitos grupos usam GANDI como registrar?
    R: Porque naqueles tempos (2000), ter seu próprio domínio era tranquilamente
       de 5 a 10 vezes mais caro do que hoje. GANDI acabou com esses preços
       introduzindo ofertas muito baratas no mercado. E como em geral temos
       pouco ou nenhum dinheiro para colocar em projetos, simplemente compramos
       nossos domínios lá.

    Q: O que GANDI significa?
    R: Isso é uma curiosidade, mas GANDI significa "Gestion et Attribution
       des Noms de Domaines Internet", "Atribuição e gestão de nomes de
       domínio de Internet". Como empresa, o modelo de negócios era simples:
       praticamente tudo automatizado, praticamente sem suporte e preços
       baixos.

    Q: Por que GANDI é vista como uma organização política?
    R: Os fundadores originais do GANDI tinham idéias políticas sobre bens
       comuns e como todo o negócio de nomes de domínio da Internet era
       baseado em escassez virtual. Um deles até escreveu um livro,
       "Confessions d'un voleur" ("Confissões de um ladrão") no qual ele
       descreve extensamente sobre como fez um monte de dinheiro exatamente
       vendendo algo que não existia e que não havia sentido em vender.

    Q: Por que GANDI é visto como "descolado"?
    R: Alguns dos fundadores originais também eram envolvidos na cena
       alternativa da Internet na França. Parte do dinheiro obtida pelo
       GANDI ajudou outros projetos, um dos quais o operador sem fins
       lucrativos Gitoyen.

    Q: Onde estão esses fundadores agora?
    R: Em algum outro lugar! Como não foram capazes de concordar em onde
       o dinheiro deveria ir, os quatro fundadores resolveram vender o
       GANDI. Eles fizeram isso até por um preço justo. Tentaram vender
       para pessoas que respeitariam o espírito original, e em certo
       sentido o fizeram. Parece que o GANDI ainda contribui para
       projetos de software livre e relacionados, tanto em tempo de
       trabalho quanto em dinheiro.

    Q: Devo confiar no GANDI?
    R: Da mesma forma que você confia em qualquer companhia capitalista.
       Eles preferirão preservar seu negócio ao invés de ajudar você.

## Responsabilização

O Grupo de Trabalho formado pelas pessoas responsáveis pelo presente processo deve:

1. Caso o Coletivo disponha de recursos financeiros, manter certificado SSL
   assinado para `*.dominio` via [Gandi](https://gandi.net). O Coletivo arcará com
   os custos. Caso contrário, adotar a certificação
   [CACert](http://www.cacert.org/) como padrão.
2. Manter formas alternativas de certificação via:
     a. [Monkeysphere](http://web.monkeysphere.info).
     b. A simples verificação da impressão digital dos certificados mediante
        algum vínculo direto (por exemplo, troca de fingerprints ou validação via
        OpenPGP) com os/as administradores das máquinas.
     c. Manter, na medida do possível, o público informado das mundanças nas
        haves de acesso, utilizando para isso OpenPGP. Como exemplo, manter uma página
        ública e atualizada sobre os atuais certificados utilizados e também informar
        rupos e pessoas próximas sobre mudanças em certificados.
3. Opcionalmente, manter também a assinatura via [http://www.cacert.org/ CaCert].
4. Evitar o vencimento da validade dos certificados, utilizando para isso
   métodos como o [http://prefetch.net/articles/checkcertificate.html
   ssl-cert-check], implementado por exemplo no
   [http://git.fluxo.info/puppet-ssl puppet-ssl].
5. Observar a aplicação dos critérios e determinações do presente processo,
   operando conjuntamente com o GT de [wiki:Camadas Configuração de sistemas
   padronizada e centralizada].
