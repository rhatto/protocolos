# Grupo de Trabalho de Backups

{{ info.context.pt }}

{{ warning.needs_work.pt }}

O presente processo estabelece as linhas gerais de funcionamento de um grupo de
trabalho responsável pela realização de backups para o Coletivo, cujos
objetivos são delineados nos critérios que a seguir.

## Preservação

O grupo de trabalho deve manter backups do máximo número possível de
camadas/instâncias do Coletivo, preservando os critérios de segurança e
privacidade assim como a Política de Segurança da Informação do Coletivo e
especialmente o seguinte critério de persistência da informação:

`
Informações armazenados num determinado nível de acesso ou segurança (exemplo,
disco criptografado) devem, por padrão, permanecer nesse mesmo nível ou serem
transferidas para um nível mais seguro, exceto quando constitui informação
desclassificada e permitida para descer de nível.
`

Do ponto de vista do [RSP](http://rsp.fluxo.info), o GT de Backups deve
proporcionar replicação de camadas que preservem (ou que aumentem o nível, se
possível) suas propriedades de segurança e privacidade no acesso à informação.

## Otimização de parâmetros

O grupo de trabalho deve ainda otimizar os seguintes parâmetros ao propiciar a
realização de backups:

1. Periodicidade.
2. Incrementos.
3. Largura de banda.
4. Segurança e integridade.
5. Espaço em disco.

## Auditagem

O grupo de trabalho deve também realizar auditagens periódicas nos backups para
se certificar de sua realização e, se possível, possuir um sistema automático
de relatórios de backups.

## Dependências

A realização deste processo depende da realização dos seguintes processos:

* [Política de segurança da informação](/coletivo/comunicacao/acl).
