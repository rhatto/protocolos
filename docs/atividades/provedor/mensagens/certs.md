# Informe: mudança de certificado

{{ info.context.pt }}

{{ warning.needs_work.pt }}

De acordo com a política[1] de gestão de certificados X.509 usados nas
comunicações TLS/HTTPS e dada a proximidade do vencimento do certificado atual,
o `$coletivo` gerou um novo certificado SSL validado por
`$certificate_authority`.

A assinatura SHA1 do novo certificado é

    SHA1 Fingerprint=$fingerprint

Detalhes sobre o certificado estão em https://www.sarava.org/certs

Tal informação pode ser verificada nas informações de segurança do seu
navegador ao acessar um sítio com conexão segura. Detalhes a respeito podem ser
encontrados no Manual de Criptografia[2].

O certificado é válido para o domínio sarava.org e todos os seus subdomínios.
Assim, está sendo utilizada conexão segura por padrão na maioria dos
subdomínios de `$dominio` (por exemplo `www.$dominio`).

No entanto, alguns sítios ainda podem estar sem conexão criptografada ou
oferecerem links internos utilizando http.

Em caso de dúvidas, basta entrar em contato :)

[1] https://protocolos.fluxo.info/organizacao/comunicacao/cert/
[2] https://manual.fluxo.info/criptografia/internet
