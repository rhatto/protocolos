# Modelo de informe de downtime

{{ info.context.pt }}

{{ warning.needs_work.pt }}

## Português

Estamos com uma queda inesperada de um de nossos servidores.

Assim, alguns serviços ficarão inacessíveis nas próximas horas.

    Servidor          : servidor.example.org
    Problema          : não identificado
    Tempo estimado    : 72 horas
    Serviços afetados :
      - Todas as listas de discussão.
      - Contas de email.
      - Todos os vservers hospedados para terceiros/as.
      - Todos os sites hospedados nesse servidor.

Outros serviços e servidores não estão afetados.

## English

We're having an unexpected downtime in one of our servers.

Then, some services will be unreachable in the next hours.

    Server            : servidor.example.org
    Problem           : N/A
    Expected downtime : 72 hours
    Services affected :
      - All mailing lists.
      - Email accounts.
      - All third-party hosted vservers.
      - All websites hosted in the server.

Other servers and services are unaffected.
