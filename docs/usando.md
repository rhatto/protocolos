# Como usar este projeto

## Introdução

A modularidade de tais protocolos permite que eles sejam adotados tanto como um
todo quanto em pequenos conjuntos (ou mesmo unitariamente) sem perda de
consistência desde que seja observada a dependência entre protocolos/processos.

## Variáveis

Muitos dos protocolos utilizam variáveis, isto é, porções de texto prontas para
a subsituição por um valor correspondente. Como exemplo, ocorrências da porção
de texto `$coletivo` podem ser substituídas pelo nome do seu grupo.

## Clonando o código

O código pode ser obtido diretamente de um dos seus espelhos:

* Espelho #1: [https://git.fluxo.info/templates](https://git.fluxo.info/templates).
* Espelho #2: [https://0xacab.org/rhatto/protocolos.git](https://0xacab.org/rhatto/protocolos.git).

## Metadados

* {{ metadata.version.pt }}: `ver:2024-09-14`.
* {{ metadata.identifier.pt }}: `pid:usando`.
