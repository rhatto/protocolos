# Tarefas

Lista de tarefas deste projeto.

{{ info.context.pt }}

## Estruturação

* [ ] Renomear branch `master` para `main`.
* [ ] Re-estruturação inspirada em projetos como o Copy Far "AI":
    * [x] Usar plugins de metainformação (`macros`).
    * [x] Nome, ID e versão para cada protocolo.
    * [x] Versão de cada documento original.
    * [ ] Re-estruturar pastas.
    * [ ] Cada tema/tópico numa pasta, com um texto introdutório independente de
          protocolo específico.
    * [ ] Cada tema/tópico pode ter inúmeros protocolos.

## Apresentação

* [ ] Introdução: "protocolos" não podem ser "procotolos"!
* [ ] Definir o entendimento de "protocolos" e "sociotécnica" a partir
      da pesquisa de Nahema Falleiros:
      * @nahema2017 sobre "protocolos atrofiados", a abrindo um diálogo com
        Cortázar e seus manuais de instruções.
      * @nahema2024 pág. 107 sobre sistemas sócio-técnicos (Grupo de Tavistock).
      * "Protocol: How Control Exists after Decentralization",
        @galloway2004.
* [ ] Sobre a abordagem contratualista: suas propriedades e suas muitas
      limitações e problemas.

## Site

* [ ] Modos diurno e noturno.
* [ ] Melhorias de design para que fique mais inclusivo.

## Template

* [ ] Create a documentation template including:
    * Version.
    * Original document (if translation, summary etc).
    * Original document version.

## Protocol Suite

Um desenvolvimento posterior deste projeto poderia inclusive permitir a
especificação de uma dada escolha protocolar através de:

* Uma string de Protocol Suite.
* Identificadores de cada protocolo ou parte de protocolo.
* Indicação de dependências entre protocolos.

Um esboço deste tipo foi feito no [RSP](https://rsp.fluxo.info).

## Nomenclaturas

* [ ] Padronização de nomes de arquivos:
    * [ ] Relativos a transações financeiras (boletos, comprovantes de pagamento
          e transferências), que funcione para todos os projetos. Campos: data,
          credor, valor, ID da transação etc.
* [ ] Linguagem inclusiva e neutra:
    * [ ] Análise sobre a possibilidade de uma linguagem totalmente "neutra":
          tratam-se de escolhas políticas, e "neutro" nesse contexto diria respeito
          à não distinguir pessoas de acordo com critérios discriminatórios.
    * [ ] Sobre como as pessoas querem ser tratadas e referidas.
    * [ ] Sobre como mencionar pessoas no geral e no específico.
    * [ ] Sobre siglas das diversidades, como por exemplo:
        * [ ] LGBTQIAPN+.
        * [ ] LGBTQIA2S+.
        * [ ] LGBTQQIAAP.
    * [ ] Gramáticas existentes, incluindo:
        * Uso de _e_ ao invés de _a_ e _o_: nem sempre dá certo, por exemplo no
          caso de "trabalhador", "trabalhadora", "trabalhadores". Isto é, o _e_
          conflita com o uso existente.
        * Cacildis: O Método Mussum: resolve o problema do _e_, pois não há uso
          existente equivalente para o _i_.
        * Busca pelo uso de frases com gênero neutro: "Boas vindas, pessoal" ao
          invés de "Olá a todos, sejam bem vindos"; "cada qual" ao invés de "cada um"; etc.
* [ ] Sobre terminologias, incluindo:
    * [ ] "Minoria", "maioria", representatividade etc.
    * [ ] "Racismo linguístico: os subterrâneos da linguagem e do racismo",
          @nascimento2020b.

## Organização

* [ ] Introduzir o conceito de arquivo `WONTDO` ou `ICEBOX`? Ou apenas usar
      uma seção "Geladeira" ou "Congelado" nos arquivos `TODO` e/ou entradas num
      arquivo do tipo `ChangeLog`?
* [ ] Adicionar nota de CUIDADO: organização pode ser opressiva, pode inibir,
      pode barrar a criatividade. Pode ser tornar uma ordem, um ordenamento que
      inibe ao invés de incentivar. Organização não é algo que se impõe, mas
      algo que se constrói coletivamente a partir das particularidades
      e características.

## Comunicação

* [ ] Sobre mensagens sucintas: qual recomendação?
    * Mensagens compactas podem soar ríspidas e secas.
    * Ao mesmo tempo em que mensagens compactas tornam&se a norma, cada vez mais
      diretas e retas.

## Conduta

* [ ] Introdução:
    * Erich Fromm: amor socialista, 2R 2C: base para qualquer relação.
    * Códigos de conduta como contratos e acordos coletivos, mas que
      também podem ser adotados no nível pessoal, como um contrato
      consigo sobre como se portar.
* [ ] Linguagens inclusivas, de gênero neutro, não-binárias etc.
    * Criar diferentes protocolos de linguagem inclusiva, representando
      diferentes propostas e práticas.
    * Juntar referências sobre o assunto.
    * "De" ao invés de "do" e "da", pois "de" é de gênero neutro:
        * Exemplo: "Livro de Pessoa de Tal" ao invés de artigo usando o gênero
          gramatical "oficial" portugês no masculino ou feminino.
* [ ] Política Anti-Assédio:
    * Sobre assediadores saberem como e quem assediar. O assédio não
      ocorre somente contra uma pessoa, como é parte de toda uma rede
      de relações sociais onde assediadores estabelecem níveis de
      relação, compartimentalização, divisionismo etc de modo que
      seja estabelecida um modus operandi de perpetuação e
      acobertamento.
* [ ] Lugares de falas:
  * [ ] Definições:
    * [ ] Lugar de fala, por exemplo a partir de Djamila Ribeiro[@ribeiro2019].
    * [ ] Lugar de multi-fala:
      * Ponto de emissão, no espaço-tempo das diversas origens, caminhadas,
        privilégios etc, de enunciações diversas, inclusive menções à enunciações
        de pessoas de outros lugares de (multi-)fala.
      * É a prática de (re-)incorporar muitas vozes numa mesma voz,
        (re-)disponibizando-a(s) para outras incorporações de (multi-)fala.
    * [ ] Lugares de (multi-)fala:
      * Consideração de que uma mesma pessoa enunciante pode ter origens,
        caminhadas e heranças múltiplas, algumas mais ou menos potencializadoras,
        mais ou menos favoráveis, podendo até alguma delas serem
        despotencializadoras e desfavoráveis.
      * As hipersuperfícies dos espaços-tempos de (multi-)fala(s) não são
        necessariamente contínuas.
    * [ ] O momento da fala pode ser de exercimento (e até de (re-)produção) de
          privilégios, e assim é importante ter a noção de quando, como e quanto
          falar.
* [ ] Conduta estrita:
    * Se te consideram, ou se você se considera, uma pessoa de privilégios.
    * Avisar as pessoas quando você não estiver em condições de convívio,
      para que não pensem que o problema é com elas.
    * Não encostar nas pessoas.
    * Não encarar ninguém.
    * Relações poluídas, pré-carregadas de traumas e más experiências
      anteriores. Cada pessoa pode trazer bagagens boas e ruins numa
      relação. E as relações tendem a já começar dentro de uma sociedade
      poluída com violências.
    * Primeiro encontro, tensão, assumir as piores possibilidades, precaução.
      Voto de boas intenções, compreensão das tensões pré-existentes e
      voto de confiança para que eventuais encontros posteriores sejam
      melhores; encontros subsequentes confiança.
    * Reconhecer o quanto sua mera existência ou presença já pode ser uma
      violência para outros seres. Tentar ao máximo reduzir essa violência.
    * Estimar se a sua presença e atitudes serão mais benéficas do que os
      prejuízos que ela pode trazer a outros seres e ao ambiente.
    * Indicar que:
      * Tentativa de ser consciente das relações de poder que podem emergir do
        fato de estar se comunicando, ou da mera presença.
      * Há intenção de diálogo, não de impor um discurso.
      * Pode haver diferença nos sentidos das palavras usadas e outros
        entendimentos, e o vocabulário que usarei talvez não seja o mais
        apropriado.

## Expedições

* [ ] Reorganizar e limpar sessão.
* [ ] Diagrama de bagagens (como cada kit pode ser combinado).
* [ ] "Modo Carnaval": telefone celular sobressalente:
    * Em alguns aplicativos de mensageria instantânea (como Signal):
        * Usar como "dispositivo conectado":
            * Prós:
                * Mais simples e fácil.
            * Contras:
                * Isolamento de contatos? Bom, é só não importar contatos no telefone do
                  carnaval.

## Desconexão

* [ ] Disconnection HOWTO / Sumiço Computacional / "Forget me for a while", incluindo dicas:
      * [ ] Avisar de antemão a galera sobre o período o off
      * [ ] É como tirar férias ou ficar doente, mas sem tirar férias ou ficar doente.
      * [ ] Se respeitam gala e nojo, pq não respeitar também a concentração? os
            três implicam numa indisponibilidade pro balcão mundial de atendimento.

## Transition procedures

### Transition file

* Inclusão de modelo de "arquivo de transição" e "testamentos" (digitais e
  "analógicos"): [transition files](https://jacobian.org/2022/nov/9/transition-files/).

### Sucessão

* [GitHub Deceased User Policy - GitHub Docs](https://docs.github.com/en/site-policy/other-site-policies/github-deceased-user-policy)
    * [Maintaining ownership continuity of your personal account's repositories - GitHub Docs](https://docs.github.com/en/account-and-profile/setting-up-and-managing-your-personal-account-on-github/managing-access-to-your-personal-repositories/maintaining-ownership-continuity-of-your-personal-accounts-repositories)

## Referências

\bibliography

## Metadados

* {{ metadata.version.pt }}: `ver:latest`.
* {{ metadata.identifier.pt }}: `pid:todo`.
