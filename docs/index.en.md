# Sociotechnic Protocols

## About

**Blueprints, boilerplates, models, lists, templates! Ready to use and to
organize yourself and your team!**

This is a repository with protocols for personal and collective management.

It has procedures, behaviors and simple rules that can be freely adopted.
When combined, they may contribute to the sprouting of complex collective
modes of organization.

They are small machines that increase the flow and organization of social
groups.

{{ info.context.en }}

## Translations

These protocols are mainly maintained in [Portuguese](/), but versions
in other languages may be available.

## Metadata

* {{ metadata.version.pt }}: `ver:2024-09-14`.
* {{ metadata.identifier.pt }}: `pid:index_en`.
* {{ metadata.original_identifier.pt }}: `pid:index`.
* {{ metadata.original_version.en }} `ver:2024-09-14`.
