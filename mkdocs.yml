#
# Mkdocs configuration
#

# Basic info
site_name: Protocolos Sociotécnicos

# Folders
docs_dir : docs
site_dir : site

# URLs
#repo_url: https://0xacab.org/rhatto/protocolos
repo_url: https://git.fluxo.info/templates
edit_uri: ''

# Development
# Site address is bind to 0.0.0.0 so it works find inside a Docker container.
# A better config would be desirable.
#dev_addr: '0.0.0.0:8040'

# Theme configuration
# See https://www.mkdocs.org/user-guide/choosing-your-theme/
#     https://squidfunk.github.io/mkdocs-material/
theme:
  name               : material
  collapse_navigation: true
  titles_only        : false
  logo               : https://fluxo.info/images/fluxo.png
  palette:
    primary: blue grey

  # Do not use external fonts
  # Usefult for offline operation
  # See https://squidfunk.github.io/mkdocs-material/setup/changing-the-fonts/#autoloading
  #font: false

# Extra stylesheets
extra_css:
  - site.css

# Plugins
# https://www.mkdocs.org/dev-guide/plugins/
plugins:
  # Search
  # See https://www.mkdocs.org/user-guide/configuration/#search
  #     https://squidfunk.github.io/mkdocs-material/setup/setting-up-site-search/
  search: {}

  # Awesome Pages Plugin
  # https://github.com/lukasgeiter/mkdocs-awesome-pages-plugin
  awesome-pages:
    collapse_single_pages: true

  # The built-in privacy plugin automatically identifies external assets as
  # part of the build process and downloads all assets for very simple
  # self-hosting
  #
  # See https://squidfunk.github.io/mkdocs-material/plugins/privacy/
  #     https://squidfunk.github.io/mkdocs-material/setup/ensuring-data-privacy/#built-in-privacy-plugin
  privacy: {}

  # BibTeX support using mkdocs-bibtex
  # https://pypi.org/project/mkdocs-bibtex/
  # https://github.com/shyamd/mkdocs-bibtex
  bibtex:
    bib_dir   : "biblio"
    csl_file  : "apa.csl"
    #cite_inline: true

  # Internationalization
  i18n:
    docs_structure: suffix
    languages:
      - locale: en
        name: English
        build: true

      - locale: pt
        name: Português
        build: true
        default: true
        nav_translations:
          Provedor: Provider

  # MkDocs-Macros Plugin
  # https://mkdocs-macros-plugin.readthedocs.io
  macros: {}

  # Redirects
  # https://github.com/mkdocs/mkdocs-redirects
  # In case site re-structure is needed, this plugin will ensure permalink functionality
  #- redirects:
  #    redirect_maps:
  #      'somepage.md' : 'otherpage.md'

# Markdown extensions
# See https://www.mkdocs.org/user-guide/configuration/#markdown_extensions
#     https://squidfunk.github.io/mkdocs-material/setup/extensions/
markdown_extensions:
  # Footnotes
  # https://squidfunk.github.io/mkdocs-material/reference/footnotes/
  footnotes: {}

  # Tasklist handling
  # https://squidfunk.github.io/mkdocs-material/setup/extensions/python-markdown-extensions/#tasklist
  pymdownx.tasklist:
    custom_checkbox: true

  # Admonitions
  # https://squidfunk.github.io/mkdocs-material/reference/admonitions/
  admonition: {}

  # Details
  # https://squidfunk.github.io/mkdocs-material/setup/extensions/python-markdown-extensions/#details
  pymdownx.details: {}

# Copyright notice
copyright: Copyleft &copy; 2024

# Extra template parameters
# https://www.mkdocs.org/user-guide/configuration/#extra
extra:
  generator: false

  metadata:
    version:
      pt: >
        Versão deste documento

      en: >
        Document version

    identifier:
      pt: >
        Identificador

      en: >
        Identifier

    original_identifier:
      pt: >
        Documento original

      en: >
        Original document

    original_version:
      pt: >
        Traduzido e adaptado da versão original em Português

      en: >
        Translated and adapted from the original Portuguese version

  warning:
    trigger:
      pt: >
        !!! warning "Alerta de conteúdo"

            Esta seção trata de temas sensíveis e que podem causar desconforto.

            Por outro lado, ela contém modelos de proceder cujo objetivo é
            acabar com violências e desconfortos sociais.

            [Sair deste site](https://pt.wikipedia.org)

      en: >
        !!! warning "Content warning"

            This section deals with sensitive topics that may cause discomfort.

            On the other hand, it contains behavior models that can help
            end violences and social discomfort.

            [Exit this site](https://en.wikipedia.org)

    needs_work:
      pt: >
        !!! warning "Documentação incompleta"

            A documentação a seguir encontra-se num estágio ainda inicial,
            muito incompleto e pode não fazer sentido fora do conteúdo para o
            qual ela foi criada.

            Trabalho adicional ainda é necessário para que ela esteja num
            estado minimamente útil e compreensível.

      en: >
        !!! warning "Incomplete document"

            The documentation that follows is in an initial stage,
            very incomplete and may not make sense outside it's original
            context.

            Additional work is still needed to bring it to a minimally
            useful and comprehensible state.

  info:
    context:
      pt: >
        !!! info "Contexto, pertinência e relevância"

            Esta documentação contém sugestões oriundas de pesquisas e
            vivências em contextos sociais específicos.

            Elas de modo algum são sugestões universais ou imposições de
            uma forma de agir e se comportar.

            Não são modelos no sentido de que _devem_ ser seguidos, mas modelos
            no sentido de que _podem_ ser adotados, adaptados ou até mesmo
            criticados e melhorados, desde que ajudem, sejam úteis a façam
            sentido.

      en: >
        !!! info "Context, pertinence and relevance"

            This documentation has suggestions from research and experiences
            from specific social contexts.

            They are not, on any level, universal suggestions or impositions
            about specific ways to act and behave.

            They're not models that _must_ be followed, but models that
            _can_ be adopted, adapted or even criticized and improved, as
            long as they can be useful, helpful and make sense.
